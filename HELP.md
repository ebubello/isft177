

Hola!

👍( ❛ ͜ʖ ❛ 👍)



#GITLAB 
##1- Inicializar un directorio local para el control de versiones de Git

git init

##2- Dentro del Folder que quiero hacer commit

git config --global user.name "your_username"
git config --global user.email "ebubello@gmail.com"

##3- Para verificar la configuración, ejecute:

git config --global --list

##4- Descargue los últimos cambios en el proyecto

git pull <remote> <name-of-branch>

<remote> es típicamente origin

##5 - Agregar y confirmar cambios locales

git add <file-name or="" folder-name="">
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"

##6 - Agregar todos los cambios para confirmar

git add .
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"

##7 - Enviar cambios a GitLab.com

git push origin master

##8 - Eliminar todos los cambios en la rama

git checkout .

##7 - Ver los cambios que has realizado

git status

##8 - Ver diferencias

git diff

##Para solucionar problema con SSH

$ git config --global http.sslVerify false
