<?php

class Dashboard extends Controller
{
    public function __construct()
    {
        $this->dash = $this->model('dashboardModel');
        $this->go = new url_helper();
        $this->us = new UserSession();

        //Inicio sesion manulamente, debido a que necesito crear un objeto de la clase UserSession en dashboard y si el metodo esta en el constructor
        //me putea, que ya existe una sesion creada.
        $this->us->sessionStart();
    }


    public function dashboard()
    {

        $user_ = $this->us->getCurrentUser();
        //var_dump($user_);

        if (is_null($user_)) {

            $this->go->redirection('/Usuario/login');
        } else {

            $this->view('pages/dashboard');
        }
    }


    public function dashPreInView()
    {
        $user_ = $this->us->getCurrentUser();
        //var_dump($user_);

        if (is_null($user_)) {

            $this->go->redirection('/Usuario/login');
        } else {

            $this->view('pages/dashPreInsView');
        }
    }


    public function viewPreInsc()
    {

        echo json_encode($this->dash->getPreInscription(), true);
    }



    public function dashPreInsDoc($dni, $tec)
    {
        //Recibe el dni y pinta en pantalla los documentos cargados por el usuario.
        $user_ = $this->us->getCurrentUser();

        if (is_null($user_)) {

            $this->go->redirection('/Usuario/login');
        }

        if ($_SERVER["REQUEST_METHOD"] == "GET") {

            $this->view('pages/dashPreInsDoc');

            foreach ($this->dash->getPathDocs($dni, $tec) as $key => $value) {

                echo '<tr>
                       <th class="table-light" scope="row">' . $value['id'] . '</th>
                       <td class="table-light">' . $value['doc_description'] . '</td>
                       <td class="table-light">' . $value['Description'] . '</td>
                       <td class="table-light"><a target="_blank" href="' . '../../' . UPLOAD_FILES . $value['dni'] . '/' . $value['doc_description'] . '">Ver</a></td>
                       <td class="table-light">' . $value['description'] . '</td>
                       <td class="table-light">
                    
                    <a class="btn btn-info" title="Ver Documentos" href="../../../Dashboard/dashPreInsUpdateStatus/' . $value['id'] . '/' . $value['dni'] . '/' . 1 . '/' . $value['idTecCareer'] . '" role="button"><i class="fas fa-thumbs-up"></i></a>
                    <a class="btn btn-danger" title="Eliminar" href="../../../Dashboard/dashPreInsUpdateStatus/'  . $value['id'] . '/' . $value['dni'] . '/' . 2 . '/' . $value['idTecCareer'] . '" role="button"><i class="fas fa-thumbs-down"></i></a>
                       </td>
                   </tr>';
            }

            echo '    </tbody>';
            echo '  </table>';
            echo '</div>';

            echo '</div>';
            echo '

          <!-- DataTable JavaScript -->
          <script src="https://code.jquery.com/jquery-3.5.1.js"></script> 
          <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
          <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

         
         
          <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
          <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
          <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
          
         
          <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.3/js/dataTables.buttons.min.js"></script>
          <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.bootstrap4.min.js"></script>
          <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.flash.min.js"></script>
          <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.html5.min.js"></script>
          <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.3/js/buttons.print.min.js"></script>

          ';
        }
    }

    public function dashPreInsUpdateStatus($id, $dni, $status, $idTec)
    {
        //Actualizo el estado de los documentos.
        $array = [
            'id' => $id,
            'dni' => $dni,
            'status' => $status,
            'idTec' => $idTec
        ];

        if ($this->dash->updateDocStatus($array)) {
            if ($this->dash->updatePreInscriptionStatus($array)) {
                $this->go->redirection('/Dashboard/dashPreInsDoc/' . $array['dni'] . '/' . $array['idTec']);
            }
        } else {
            echo 'No se guardo el cambio';
        }
    }


    public function deletePreIns($dni, $idTec)
    {

        //delete de la preInscripcion dependiendo del id de rol elimina o hace un borrado logico.
        $user_ = $this->us->getCurrentUser();

        if (is_null($user_)) {

            $this->go->redirection('/Usuario/login');
        }

        if ($_SERVER["REQUEST_METHOD"] == "GET") {

            if ($this->dash->deletePreIns($dni, $idTec)) {
                if ($this->dash->deleteFiles($dni)) { //Si existe al menos un registro para el DNI, no borro la folder.

                    $this->go->redirection('/Dashboard/dashPreInView');
                }
            }
        }
    }


    public function activePreIns($value)
    {
        $user_ = $this->us->getCurrentUser();

        if (is_null($user_)) {

            $this->go->redirection('/Usuario/login');
        }

        if ($_SERVER["REQUEST_METHOD"] == "GET") {

            if ($this->dash->actPreIns($value)) {

                $this->go->redirection('/Dashboard/dashPreInView');
            } else {
                //$this->go->redirection('/Dashboard/dashPreInView');
                echo '<script type="text/javascript">
                        alert("Registro no activado, verifique el DNI ingresado!!!);
                    </script>';
            }
        }
    }


    public function enableConfig($beginDate, $endDate)
    {
        if ($_SERVER["REQUEST_METHOD"] == "GET") {

            $fechas = [
                'FechaInicio' => $beginDate,
                'FechaFin' => $endDate,
                'Status' => 'A',
                'Id' => 1,
            ];

            //Actualiza los datos de configuracion para activar el periodo de preInscripción
            if ($this->dash->updateConfig($fechas)) {
                $this->go->redirection('/Dashboard/dashboard'); //Redirecciono al Dashboard
            } else {
                echo 'Algo salio mal';
            }
        } else {
            $this->go->redirection('/Dashboard/dashboard'); //si el metodo no es GET, Redirecciono al Dashboard
        }
    }


    public function disableConfig(){

        if($this->dash->disableConfig()){
            $this->go->redirection('/Dashboard/dashboard'); //Redirecciono al Dashboard
        }else {
            echo 'Algo salio mal';
        }
    }

}
