<?php

class home extends Controller
{
    public function __construct()
    {
        //Se instancian los "modelos" que sean necesarios 
       
        $this->modelo_usario = $this->model('usuarioModel');
        $this->Cont = $this->controller('PreInscripcion');
        $this->us = new UserSession();

        $this->modelo_crud_novedades = $this->model('modelo_crud_novedades');
        $this->modelo_crud_anuncios = $this->model('modelo_crud_anuncios');
        $this->modelo_home = $this->model('homeModel');
       
        $this->go = new url_helper(); //Instancio un objeto de la clase URL_HELPER

        //$this->us = new UserSession();
        $this->us->sessionStart();
    }
    


     // Por defecto (o si se escribe una URL incorrecta) se accede al metodo index de la clase home
     public function index()
     {
             $novedades = $this->modelo_crud_novedades->novedadeshome();
             $anuncios = $this->modelo_crud_anuncios->anuncioshome();
             $_SESSION['Novedadeshome'] = $novedades;
             $_SESSION['anuncioshome'] = $anuncios;
             $this->view('pages/home');
     }
 
     

    public function Resoluciones()
    {
        $this->view('pages/Resoluciones');
    }

    public function Reglamento()
    {
        $this->view('pages/Reglamento');
    }

    public function Constancias()
    {
        $this->view('pages/Constancias');
    }

    public function CAI()
    {
        $this->view('pages/CAI');
    }

    public function contacto()
    {
        $this->view('pages/contacto');
    }

    public function tSistemas()
    {
        $this->view('pages/tSistemas');
    }

    public function tContable()
    {
        $this->view('pages/tContable');
    }

    public function tGestion()
    {
        $this->view('pages/tGestion');
    }

    public function tClinicos()
    {
        $this->view('pages/tClinicos');
    }

    public function institucion()
    {
        $this->view('pages/institucion');
    }

    public function crud_novedades()
    {
        if(isset($_SESSION['message'])){
        unset($_SESSION['message']);}
        $novedades = $this->modelo_crud_novedades->novedades();
        $_SESSION['Novedades'] = $novedades;
        $this->view('pages/crud_novedades');
    }

    public function cargar_novedad()
    {
        if (isset($_POST['save_task'])) {

            $carpeta = UPLOAD_IMG .'novedades/';
            opendir($carpeta);
            $imagen = $_FILES['imagen']['name'];
            $ruta = $carpeta . $_FILES['imagen']['name'];
            
            if (!file_exists($carpeta)) {mkdir($carpeta, 0777, true);}
            // copy($_FILES['imagen']['tmp_name'], $ruta);
            move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
           
            
            $titulo = trim($_POST['titulo']);
            $descripcion = trim($_POST['descripcion']);
            $prioridad = trim($_POST['prioridad']);
            $fecha_inicio = $_POST['fecha_inicio'];
            $fecha_fin = $_POST['fecha_fin'];
            $link = trim($_POST['link']);
            $nombrelink = trim($_POST['nombre_link']);
            $this->modelo_crud_novedades->cargar_novedad($titulo, $descripcion,$prioridad,$fecha_inicio,$fecha_fin,$link,$nombrelink,$imagen);
            if(isset($_SESSION['Novedades'])){
            unset($_SESSION['Novedades']);}
            $novedades = $this->modelo_crud_novedades->novedades();
            $_SESSION['message'] = 'Novedad Cargada Correctamente';
            $_SESSION['message_type'] = 'success';
            $_SESSION['Novedades'] = $novedades;
            $this->view('pages/crud_novedades');
        }
    }

    public function borrar_novedad()
    {

        if (isset($_GET['ID'])) {
            $ID = $_GET['ID'];
            $this->modelo_crud_novedades->borrar_novedad($ID);
            if(isset($_SESSION['Novedades'])){
            unset($_SESSION['Novedades']);}
            $novedades = $this->modelo_crud_novedades->novedades();
            $_SESSION['message'] = 'Novedad Eliminada';
            $_SESSION['message_type'] = 'danger';
            $_SESSION['Novedades'] = $novedades;
            $this->view('pages/crud_novedades');
        }
    }

    public function editar_novedad()
    {

        if (isset($_POST['actualizar'])) {

            $carpeta = UPLOAD_IMG .'novedades/';
            opendir($carpeta);
            $imagen = $_FILES['imagen']['name'];
            $ruta = $carpeta . $_FILES['imagen']['name'];
            
            if (!file_exists($carpeta)) {mkdir($carpeta, 0777, true);}
            // copy($_FILES['imagen']['tmp_name'], $ruta);
            move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);

            if($imagen==null){
            $imagen = $_POST['imagentext'];}

            $idnovedad = $_POST['idnovedad'];
            $titulo = trim($_POST['titulo']);
            $descripcion = trim($_POST['descripcion']);
            $prioridad = trim($_POST['prioridad']);
            $fecha_inicio = $_POST['fecha_inicio'];
            $fecha_fin = $_POST['fecha_fin'];
            $link = trim($_POST['link']);
            $nombrelink = trim($_POST['nombre_link']);


            $this->modelo_crud_novedades->actualizar_novedad($idnovedad,$titulo,$descripcion,$prioridad,$fecha_inicio,$fecha_fin,$link,$nombrelink,$imagen);
            if(isset($_SESSION['Novedades'])){
            unset($_SESSION['Novedades']);}
            $novedades = $this->modelo_crud_novedades->novedades();
            $_SESSION['message'] = 'Novedad Actualizada';
            $_SESSION['message_type'] = 'warning';
            $_SESSION['Novedades'] = $novedades;
            $this->view('pages/crud_novedades');
        }

        if (isset($_POST['cancelar'])) {
            if(isset($_SESSION['Novedades'])){
                unset($_SESSION['Novedades']);}
            $novedades = $this->modelo_crud_novedades->novedades();
            $_SESSION['Novedades'] = $novedades;
            $this->view('pages/crud_novedades');
        }


        if (isset($_GET['ID'])) {
            $id = $_GET['ID'];
            $novedad = $this->modelo_crud_novedades->consultar_novedad($id);
            $_SESSION['Novedad'] = $novedad;
            $this->view('pages/editar_novedad');
        }
    }

    // -----------

    public function crud_anuncios()
    {
        if(isset($_SESSION['message'])){
        unset($_SESSION['message']);}
        $anuncios = $this->modelo_crud_anuncios->anuncios();
        $_SESSION['anuncios'] = $anuncios;
        $this->view('pages/crud_anuncios');
    }


    public function cargar_anuncio()
    {
        if (isset($_POST['save_task'])) {

            $carpeta = UPLOAD_IMG .'anuncios/';
            opendir($carpeta);
            $imagen = $_FILES['imagen']['name'];
            $ruta = $carpeta . $_FILES['imagen']['name'];
            
            if (!file_exists($carpeta)) {mkdir($carpeta, 0777, true);}
            // copy($_FILES['imagen']['tmp_name'], $ruta);
            move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
           
            
            $titulo = trim($_POST['titulo']);
            $descripcion = trim($_POST['descripcion']);
            $prioridad = trim($_POST['prioridad']);
            $fecha_inicio = $_POST['fecha_inicio'];
            $fecha_fin = $_POST['fecha_fin'];
            $link = trim($_POST['link']);
            $nombrelink = trim($_POST['nombre_link']);
            $this->modelo_crud_anuncios->cargar_anuncio($titulo, $descripcion,$prioridad,$fecha_inicio,$fecha_fin,$link,$nombrelink,$imagen);
            if(isset($_SESSION['anuncios'])){
            unset( $_SESSION['anuncios']);}
            $anuncios = $this->modelo_crud_anuncios->anuncios();
            $_SESSION['message'] = 'Anuncio Cargado Correctamente';
            $_SESSION['message_type'] = 'success';
            $_SESSION['anuncios'] = $anuncios;
            $this->view('pages/crud_anuncios');
        }
    }

    public function borrar_anuncio()
    {

        if (isset($_GET['ID'])) {
            $ID = $_GET['ID'];
            $this->modelo_crud_anuncios->borrar_anuncio($ID);
            if(isset($_SESSION['anuncios'])){
            unset( $_SESSION['anuncios']);}
            $anuncios = $this->modelo_crud_anuncios->anuncios();
            $_SESSION['message'] = 'Anuncio Eliminado';
            $_SESSION['message_type'] = 'danger';
            $_SESSION['anuncios'] = $anuncios;
            $this->view('pages/crud_anuncios');
        }
    }

    public function editar_anuncio()
    {

        if (isset($_POST['actualizar'])) {

            $carpeta = UPLOAD_IMG .'anuncios/';
            opendir($carpeta);
            $imagen = $_FILES['imagen']['name'];
            $ruta = $carpeta . $_FILES['imagen']['name'];
            
            if (!file_exists($carpeta)) {mkdir($carpeta, 0777, true);}
            // copy($_FILES['imagen']['tmp_name'], $ruta);
            move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);

            if($imagen==null){
            $imagen = $_POST['imagentext'];}

            $idanuncio = $_POST['idanuncio'];
            $titulo = trim($_POST['titulo']);
            $descripcion = trim($_POST['descripcion']);
            $prioridad = trim($_POST['prioridad']);
            $fecha_inicio = $_POST['fecha_inicio'];
            $fecha_fin = $_POST['fecha_fin'];
            $link = trim($_POST['link']);
            $nombrelink = trim($_POST['nombre_link']);


            $this->modelo_crud_anuncios->actualizar_anuncio($idanuncio,$titulo,$descripcion,$prioridad,$fecha_inicio,$fecha_fin,$link,$nombrelink,$imagen);
            if(isset($_SESSION['anuncios'])){
            unset( $_SESSION['anuncios']);}
            $anuncios = $this->modelo_crud_anuncios->anuncios();
            $_SESSION['message'] = 'Anuncio Actualizado';
            $_SESSION['message_type'] = 'warning';
            $_SESSION['anuncios'] = $anuncios;
            $this->view('pages/crud_anuncios');
        }

        if (isset($_POST['cancelar'])) {
            if(isset($_SESSION['anuncios'])){
            unset( $_SESSION['anuncios']);}
            $anuncios = $this->modelo_crud_anuncios->anuncios();
            $_SESSION['anuncios'] = $anuncios;
            $this->view('pages/crud_anuncios');
        }


        if (isset($_GET['ID'])) {
            $id = $_GET['ID'];
            $anuncio = $this->modelo_crud_anuncios->consultar_anuncio($id);
            $_SESSION['anuncio'] = $anuncio;
            $this->view('pages/editar_anuncio');
        }
    }


    public function Go()
    {
        
        $this->Cont->preInscripcion();
    }

    public function GoHome()
    {
        $this->view('pages/home');
    }

    public function goIntro()
    {
        $this->Cont->Intro();
    }


    public function enviarContacto(){

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (!empty($_POST)) {

                $formContacto = [
                    'nombre'    => trim($_POST['fname']),
                    'correo'    => trim($_POST['email']),
                    'asunto'    => trim($_POST['subject']),
                    'mensage'   => trim($_POST['message']),
                ];

                //Si el metodo devuelve TRUE, es que se envio el  EMAIL correctamente.
                if( $this->modelo_home->enviarMailContacto($formContacto) ){

                    //Mejorar esto.
                   //Se ejecuta el Alert desde JS.
                    //$this->GoHome(); //Voy al HOME si esta OK
                    $this->go->redirection('/home'); //Redirecciono a HOME
                }else{
                    //Mejorar esto.
                    echo "Message could not be sent. Mailer Error: Ni Idea de cual fue el error";
                    //$this->GoHome();
                    $this->go->redirection('/home'); //Redirecciono a HOME
                }

               

            }
        }else {
            //$this->GoHome();
            $this->go->redirection('/home'); //Redirecciono a HOME
        }
    }

    public function help(){
        $this->view('pages/ayuda');
    }

}
