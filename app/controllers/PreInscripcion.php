<?php


// include_once('../helpers/url_helper.php');

class preInscripcion extends Controller
{

    public function __construct()
    {
        $this->email = $this->model('preInscripcionModel');
        $this->go = new url_helper(); //Instancio un objeto de la clase URL_HELPER

    }



    //Valido si existe una Pre Inscripcion para el DNI.
    public function dni()
    {

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (!empty($_POST)) {

                $dni = $_POST['dni'];

                //Valido si existe el DNI
                $result = $this->email->validarPreInscripcion($dni);
                // Y de paso consulto las tecnicaturas que no estan asignadas al DNI.
                $result2 = $this->email->technicalCareerUsed($dni);

                //Obtengo los dos resultados de las funciones anteriores y guardo todo en un array para poder devolver dos resultados en uno.

                $final = [
                    'result' => $result,
                    'result2' => $result2,
                ];

                if ($final['result'] <= 3) {
                    echo json_encode($final);
                } else {
                    echo json_encode('false');
                }
            }
        }
    }


    //Va a la vista Intro.
    public function Intro()
    {
        $this->view('pages/intro');
    }




    //Va a la vista de PreInscripcion.
    public function preInscripcion()
    {

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
        } else {
            $this->view('pages/preInscripcion');
        }
    }


    //Envio Mail
    public function SavePreInscription()
    {

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!empty($_POST)) {

                $datosRegistro = [
                    'techCareer' => trim($_POST['tecnicatura']),
                    'LastName'   => trim($_POST['apellido']),
                    'Name'       => trim($_POST['nombre']),
                    'Address'    => trim($_POST['direccion']),
                    'DNI'        => trim($_POST['dni']),
                    'Email'      => trim($_POST['email']),
                    'Phone'      => trim($_POST['telefono']),
                    'ConsFoto'   => 'Foto-DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFilefoto']['name']),
                    'ConsABC'    => 'ABC-DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFileABC']['name']),
                    'ConsDNI'    => 'DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFileDNI']['name']),
                    'ConsLS'     => 'LS-DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFileLS']['name']),
                    'ConsPNac'   => 'PNac-DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFilePNac']['name']),
                    'ConsTitulo' => 'Titulo-DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFileTitulo']['name']),
                    'ConsInsc'   => 'Inscripcion-DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFileinscripcion']['name']),
                    'ConsRegla'  => 'Reglamento-DNI-' . $_POST['dni'] . '-' . basename($_FILES['InputFileReglamento']['name']),

                ];

                //Si el mail se eviado OK    
                if ($this->email->enviarEmail($datosRegistro)) {
                    //Los archivos se subieron correctamente;
                    if ($this->email->saveFiles($datosRegistro)) {
                        //Los archivos se guardaron correctamente;
                        if ($this->email->saveInfo($datosRegistro)) {
                            //Guardo registro de documentos para poder saber el estado en el que se encuentran.
                            if ($this->email->saveDocument($datosRegistro)) {
                             
                        //        echo (" 
                        //        <script>
                        //        function prueba(){
                        //        $(document).ready(function(){
                        //        Swal.fire({
                        //                position: 'center',
                        //                icon: 'success',
                        //                title: 'PreInscripción Recibida',
                        //                text: 'En breve nos pondremos en contacto.',
                        //                showConfirmButton: false,
                        //                timer: 1500
                        //            })
                        //        });
                        //    };
                        //    prueba();");

                                $this->go->redirection('/home'); //Redirecciono a HOME
                            }
                        } else {
                            echo "Message could not be sent.";
                            $this->go->redirection('/home'); //Redirecciono a HOME
                        }
                    } else {
                        echo "Erro al subir los archivos";

                        $this->go->redirection('/home'); //Redirecciono a HOME
                    }
                } else {
                    echo 'Error al guardar los datos';
                    $this->go->redirection('/home'); //Redirecciono a HOME

                }
            }
        } else {
            echo 'Error todo los campos son requeridos';
        }
    }


    public function SavePreInscriptionShort()
    {

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!empty($_POST)) {

                $datosRegistros = [
                    'techCareer' => trim($_POST['sel1']),
                    'DNI'        => trim($_POST['dni2']),
                    'ConsABC'    => 'ABC-DNI-' . $_POST['dni2'] . '-' . basename($_FILES['InputFileABC']['name']),
                    'ConsInsc'   => 'Inscripcion-DNI-' . $_POST['dni2'] . '-' . basename($_FILES['InputFileinscripcion']['name']),
                ];

                if ($this->email->saveFiles($datosRegistros)) { 
                    if ($this->email->saveInfo($datosRegistros)) { 
                        if ($this->email->saveDocumentShort($datosRegistros)) {
                            if ($this->email->enviarEmailShort($datosRegistros)) { 
                        $this->go->redirection('/home');
                            }
                        }
                    }
                }
            }
        }

        $this->go->redirection('/home'); //Redirecciono a HOME
    }
}
