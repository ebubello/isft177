<?php

class Usuario extends Controller
{
    public function __construct()
    {
        $this->user = $this->model('usuarioModel');
        $this->go = new url_helper();
        $this->us = new UserSession();
       


        //Inicio sesion manulamente, debido a que necesito crear un objeto de la clase UserSession en dashboard y si el metodo esta en el constructor
        //me putea, que ya existe una sesion creada.
        $this->us->sessionStart();
    }


    public function Home(){
        $this->go->redirection('/home/GoHome');
    }


    public function logOut()
    {

        $this->us->closeSession(); //Cierro sesión.
        $this->go->redirection('/Usuario/login');
    }

    public function login()
    {
        // 2) La vista Usuario (LOGIN) tiene un file JS asociado (Ver final del HTML en la vista),  
        //    que se encarga de enviar los datos por Post a ESTE metodo. El mismo ejecuta todas las validaciones 
        //    Correspondientes y devuleve una respuesta (JSON) a la vista . SI todo esta bien, el file JS redirecciona al Dashboard, 
        //    caso contrario la respuesta a la vista es "FALSE" y nos permite notificar al usuario que algo salio mal sin recargar
        //    la pagina.

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (!empty($_POST)) {
                $datosUsuario = [
                    'dni' => trim($_POST['dni']),
                    'pass' => trim($_POST['password']),
                ];



                if (isset($_SESSION['user'])) {

                    $this->us->closeSession(); //Si existe Sesion alguna, la cierro y pido que se vuelva a loguear.
                    $this->go->redirection('/home/login');
                } else  if ($this->user->verificarDNI($datosUsuario)) {

                    if ($this->user->verificarPassword($datosUsuario)) {
                        //Si el user y pass son correctos, seteo la sesión con el username.

                        $dni = $datosUsuario['dni'];

                        $this->us->setCurrentUser($dni);

                        echo json_encode('true'); //envio la respuesta en formato JSON a /public/js/usuario/usuario.js

                    } else {

                        echo json_encode('false'); //Si no exite el PASSWORD envio la respuesta en formato JSON a /public/js/usuario/usuario.js
                    }
                } else {

                    echo json_encode('false'); //Si no exite el USER envio la respuesta en formato JSON a /public/js/usuario/usuario.js
                }
            }
        } else {
            // 1) La primera vez detecta que no hay peticion POST, entra por el else y va a la vista de Login de usuario.
            $this->view('pages/usuario');
        }
    }

    public function register()
    {
        // 2) Una vez que se cargaron los datos en el formulario de registrar y presionamos "REGISTRAR", 
        //    los datos son enviados por POST y guardados en la DB. Por eso la vista register no cuenta 
        //    con informacion en el TAG action. Ejemplo: En el HTML action="#".

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $array = [];
            //Recibo desde "register.JS", por POST y antes de guardar los datos, valido que el email y dni no existan.
            if (!empty($_POST)) {
                $datosRegister = [
                    'username' => trim($_POST['username']),
                    'dni'      => trim($_POST['dni']),
                    'email'    => trim($_POST['email']),
                    'password' => password_hash($_POST['password'], PASSWORD_DEFAULT, ['cost' => 10]),
                    'idrol'    => trim($_POST['perfil']),
                ];


                //En la tabla User solo podemos tener un registro por Email y DNI.

                // SI el DNI "NO EXISTE", valido el email.
                if (!$this->user->verificarDNI($datosRegister)) {

                    //SI el EMAIL no existe, guardo los datos enviados por POST.
                    if (!$this->user->verificarEmail($datosRegister)) {

                        if ($this->user->register($datosRegister)) {

                            //Si el metodo register devuelve true, redirecciona a la vista de login
                            //por medio de registerData.js

                            $array = ['false', 'false'];
                            echo json_encode($array);
                        }
                    } else {
                        $array = ['false', 'true'];
                        echo json_encode($array);
                    }
                } else {
                    $array = ['true', 'false'];
                    echo json_encode($array);
                }
            }
        } else {
            // 1) La primera vez, detecta que no hay peticion POST, entra por el else y va a la vista de registrar usuario.
            $this->view('pages/register');
            //$this->go->redirection('pages/register');
        }
    }


    public function forgot()
    {

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (!empty($_POST)) {

                $email = [
                    'email' => trim($_POST['email']),
                ];

                //Si el email existe, envio un mail con el link para poder resetear la password.
                if ($this->user->verificarEmail($email)) {
                    if ($this->user->sendEmailForgotPass($email)) {
                        echo json_encode('true');
                    }
                } else {
                    echo json_encode('false', JSON_FORCE_OBJECT);
                }
            }
        } else {
            $this->view('pages/forgot');
        }
    }


    public function reset($email)
    {
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            //Si el parametro enviado desde el link de restablecer contraseña es IGUAL a la Variable de sesion, permito cambiar contraseña.
            if ($_SESSION['emailForgot'] == $email) {

                $this->view('pages/reset');

            } else {
                $this->go->redirection('/home/login');
            }
        }
        
    }

    public function resetPassword(){

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            
            $pass1 = $_POST['newPass'];
            $pass2 = $_POST['newPass2'];

            if($pass1 === $pass2){ // Si es TRUE, encripto y actualizo la nueva Pass

                $passhash = password_hash( $pass1, PASSWORD_DEFAULT, ['cost' => 10]);

                if( $this->user->updatepassword($_SESSION['emailForgot'], $passhash) ){

                    unset($_SESSION['emailForgot']); //Elimino la sesion creada al enviar el EMAIL.

                    echo json_encode('true'); 
                }

            }else{
                echo json_encode('false');
            }
        } 
    }
}
