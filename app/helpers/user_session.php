<?php

class UserSession {

    public function __construct(){
        //session_start();

        //Agregue UserModel a Initializer.php para poder acceder a los metodos del mismo.
        $this->p = new usuarioModel();
        $this->u = new dashboardModel();
    }

    //recibo el DNI desde la sesion creada y obtengo el userName.
   public function getUser(){
    return  $this->p->getUsuario($_SESSION['user']);
  }



    public function sessionStart(){
        session_start();
    }

    public function setCurrentUser($user){
    
        $_SESSION['user'] = $user;
    }


    public function getCurrentUser(){
    
        return $_SESSION['user'];
    }

    public function closeSession(){
        session_start();
        session_unset();
        session_destroy(); 
    }


    public function getRolbyUserSession(){
        return  $this->u->getRol($this->getCurrentUser());
    }

  
}

?>