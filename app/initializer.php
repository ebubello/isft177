<?php

// llamando a config
require_once 'config/config.php';

// llamando a url helperl
require_once 'helpers/url_helper.php';

// llamando a user session
require_once 'helpers/user_session.php';

require_once 'models/usuarioModel.php';

require_once 'models/dashboardModel.php';

require_once 'models/preInscripcionModel.php';

// require_once 'controllers/Dashboard.php';


// llamando a libs
spl_autoload_register(function($files){
    require_once 'libs/' . $files . '.php';
});
