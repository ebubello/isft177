<?php

class dashboardModel
{

  private $db;

  public function __construct()
  {
    // Se instancia la clase "Base" para facilitar ejecucion de comandos SQL
    $this->db = new Base;
  }


  public function getPreInscription()
  {
    $User = $_SESSION['user'];

    $this->db->query('call sp_pre_inscription(:User)');
    $this->db->bind(':User', $User);
    $result = $this->db->registers();



    return $result;
  }

  public function getPathDocs($dni, $idTec)
  {

    $this->db->query('call sp_get_pre_inscription_document(:dni, :idTecCareer)');

    $this->db->bind(':dni', $dni);
    $this->db->bind(':idTecCareer', $idTec);
    $result = $this->db->registers();


    return $result;
  }

  public function updateDocStatus($datos)
  {

    if (!empty($datos)) {

      $status = '--';

      $this->db->query('UPDATE pre_inscription_document SET id_status = :id_status WHERE id = :id and dni = :dni and idTecCareer = :idTec');

      $this->db->bind(':id', $datos['id']);
      $this->db->bind(':dni', $datos['dni']);
      $this->db->bind(':id_status', $datos['status']);
      $this->db->bind(':idTec', $datos['idTec']);


      if ($this->db->execute()) {

        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  public function updatePreInscriptionStatus($datos)
  {

    $this->db->query('call PreIns_UpdateStatus (:dni , :idTec)');
    $this->db->bind(':dni', $datos['dni']);
    $this->db->bind(':idTec', $datos['idTec']);

    if ($this->db->execute()) {

      return true;
    } else {

      return false;
    }
  }


  public function deletePreIns($dni, $idTec)
  {
    //Si el user es del tipo admin entonces borro el registro, caso contrario
    //REalizo un borrado logico (en la tabla ACTION = D)

    $dniUser = $_SESSION['user'];

    $this->db->query('call sp_delete_pre_inscription(:dni, :dniUser, :idTec)');
    $this->db->bind(':dni', $dni);
    $this->db->bind(':dniUser', $dniUser);
    $this->db->bind(':idTec', $idTec);

    if ($this->db->execute()) {

      return true;
    } else {

      return false;
    }
  }


  public function deleteFiles($dni)
  {
    //Borro todo el contenido del folder asociada al dni, siempre y cuando el usuario tenga permisos de ADMIN.
    $result = $this->getRol($_SESSION['user']);


    if ($result == 1) {

      //Consulto si exite al menos un DNI
      $this->db->query('SELECT count(dni) AS DNI FROM pre_inscription WHERE dni = :dni');
      $this->db->bind(':dni', $dni);
      $result = $this->db->register();

      if ($result['DNI'] == 0) { //Si no exite registro alguno en preInscripcion para el DNI, borro la carpeta y sus archivos.

        $files = glob(UPLOAD_FILES . $dni . '/*');

        foreach ($files as $file) 
        {
          is_dir($file) ? rmdir($file) : unlink($file);
        }
        
        rmdir(UPLOAD_FILES . $dni);
      
          return true;         
      } 
        
    } 
    //Si exite al menos un registro en preInscripcion para el DNI, no borro la carpeta y sus archivos.
    return true;
  }


  public function actPreIns($dni)
  {
    //Si el user es del tipo admin entonces activo el registro.

    $dniUser = $_SESSION['user'];

    $this->db->query('call sp_active_pre_inscription(:dni, :dniUser)');
    $this->db->bind(':dni', $dni);
    $this->db->bind(':dniUser', $dniUser);

    if ($this->db->execute()) {

      return true;
    } else {

      return false;
    }
  }


  public function getRol($dniUserSession)
  {

    //devuelve el Id de ROl dependiendo del usuario de session.

    $this->db->query('SELECT idrol FROM user WHERE dni = :dni');
    $this->db->bind(':dni', $dniUserSession);
    $result = $this->db->register();

    return $result['idrol'];
  }

  public function getPreInsSuccess($tecnicatura)
  {

    $this->db->query("SELECT count(status) AS CantStatus FROM pre_inscription WHERE status = 'OK' AND technical_career = :tecnicatura ");
    $this->db->bind(':tecnicatura', $tecnicatura);
    $result = $this->db->register();

    return $result['CantStatus'];
  }

  public function getPreInsWarning($tecnicatura)
  {

    $this->db->query("SELECT count(status) AS CantStatus FROM pre_inscription WHERE status = '--' AND technical_career = :tecnicatura ");
    $this->db->bind(':tecnicatura', $tecnicatura);
    $result = $this->db->register();

    return $result['CantStatus'];
  }

  public function getPreInsDanger($tecnicatura)
  {

    $this->db->query("SELECT count(status) AS CantStatus FROM pre_inscription WHERE status = 'NOT OK' AND technical_career = :tecnicatura ");
    $this->db->bind(':tecnicatura', $tecnicatura);
    $result = $this->db->register();

    return $result['CantStatus'];
  }

  public function updateConfig($datos)
  {
    if (!empty($datos)) {

      $this->db->query('UPDATE preinscrition_config SET begin_date = :beginDate, end_date = :endDate, status = :_status  WHERE id = :id');

      $this->db->bind(':id', $datos['Id']);
      $this->db->bind(':beginDate', $datos['FechaInicio']);
      $this->db->bind(':endDate', $datos['FechaFin']);
      $this->db->bind(':_status', $datos['Status']);


      if ($this->db->execute()) {

        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public function selectConfig()
  {

    //Permito que el link de inscripcion este disponible, si la fecha actual esta entre el rango configurado en la tabla PreInscrition_Config.

    $this->db->query('SELECT status, begin_date, end_date FROM preinscrition_config WHERE id = 1');

    $result = $this->db->register();

    if ($result['status'] == 'A' && $result['begin_date'] >= date("Y-m-d") && date("Y-m-d") <= $result['end_date']) {

      return "<a id='pi' class='dropdown-item text-white bg-secondary' href='" . URL_PROJECT . "/preInscripcion/Intro'>Preinscripción</a>";
    }
  }

  public function disableConfig()
  {

    $this->db->query('UPDATE preinscrition_config SET  status = :_status  WHERE id = :id');

    $this->db->bind(':id', 1);
    $this->db->bind(':_status', 'D');


    if ($this->db->execute()) {

      return true;
    } else {
      return false;
    }
  }
}
