<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class homeModel
{

  private $db;

  public function __construct()
  {
    // Se instancia la clase "Base" para facilitar ejecucion de comandos SQL
    $this->db = new Base;
  }

  public function enviarMailContacto($formContacto){

    $varMail  = EMAIL;

    $mail = new PHPMailer(true);

    try {
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                              // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                         // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                     // Enable SMTP authentication
        $mail->Username   = EMAIL;                                    // SMTP username
        $mail->Password   = EMAIL_PASSWORD;                           // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;           // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                      // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom($varMail, 'noreply');
        $mail->addAddress(EMAIL);                                     // Add a recipient
        $mail->addAddress($varMail);                                  // Name is optional
        $mail->addReplyTo($varMail);


        // Attachments
        //$mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Christmas_in_Prague-Joyce_Hannam.pdf');  
        // Content
        $mail->isHTML(true);                                          // Set email format to HTML
        $mail->Subject = 'Enviado por: ' . $mail->Username;
        $mail->Body    = '<h1 align=center><br>Email: ' . $formContacto['correo'] . '<br>Nombre: '.$formContacto['nombre'].'<br> Asunto: '.$formContacto['asunto'].'<br>Mensage: '.$formContacto['mensage'].'</h1>';


        $mail->send();
        //echo 'Message has been sent';
        return true;
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        return false;
    }


  }
  

}
