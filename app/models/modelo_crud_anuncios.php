<?php

class modelo_crud_anuncios
{

    private $db;

    public function __construct()
    {
        // Se instancia la clase "Base" para facilitar ejecucion de comandos SQL
        $this->db = new Base;
    }

    public function anuncioshome()
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fecha = date("Y-m-d H:i:s");
        $this->db->query('SELECT * FROM anuncios WHERE (Fecha_Inicio<=:fecha AND Fecha_Fin>=:fecha) ORDER BY prioridad' );
        $this->db->bind(':fecha', $fecha);
        return $this->db->registers2();
    }


    public function anuncios()
    {
        $this->db->query('SELECT * FROM anuncios ORDER BY prioridad');
        return $this->db->registers2();
    }

    public function cargar_anuncio($titulo,$descripcion,$prioridad,$fecha_inicio,$fecha_fin,$link,$nombrelink,$imagen)
    {
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fecha_alta = date("Y-m-d H:i:s");
        $this->db->query('INSERT INTO anuncios (titulo, descripcion, prioridad, fecha_inicio, fecha_fin , link , nombre_link , imagen , fecha_alta) VALUES (:titulo , :descripcion, :prioridad, :fecha_inicio, :fecha_fin, :link, :nombre_link, :imagen, :fecha_alta )');
        $this->db->bind(':titulo', $titulo);
        $this->db->bind(':descripcion', $descripcion);
        $this->db->bind(':prioridad', $prioridad);
        $this->db->bind(':fecha_inicio', $fecha_inicio);
        $this->db->bind(':fecha_fin', $fecha_fin);
        $this->db->bind(':link', $link);
        $this->db->bind(':nombre_link', $nombrelink);
        $this->db->bind(':imagen', $imagen);
        $this->db->bind(':fecha_alta', $fecha_alta);
        $this->db->execute();
    }

    public function borrar_anuncio($ID)
    {
        $this->db->query('DELETE FROM anuncios WHERE Id_Anuncio=:ID');
        $this->db->bind(':ID', $ID);
        $this->db->execute();
    }

    public function consultar_anuncio($ID)
    {
        $this->db->query('SELECT * FROM anuncios WHERE Id_Anuncio=:ID');
        $this->db->bind(':ID', $ID);
        return $this->db->registers2();
    }

    public function actualizar_anuncio($ID,$titulo,$descripcion,$prioridad,$fecha_inicio,$fecha_fin,$link,$nombrelink,$imagen)
    {
        $this->db->query('UPDATE anuncios SET titulo=:titulo,  descripcion=:descripcion, prioridad=:prioridad, fecha_inicio=:fecha_inicio, fecha_fin=:fecha_fin, link=:link,nombre_link=:nombre_link, imagen=:imagen WHERE Id_Anuncio=:ID');
        $this->db->bind(':ID', $ID);
        $this->db->bind(':titulo', $titulo);
        $this->db->bind(':descripcion', $descripcion);
        $this->db->bind(':prioridad', $prioridad);
        $this->db->bind(':fecha_inicio', $fecha_inicio);
        $this->db->bind(':fecha_fin', $fecha_fin);
        $this->db->bind(':link', $link);
        $this->db->bind(':nombre_link', $nombrelink);
        $this->db->bind(':imagen', $imagen);
        $this->db->execute();
    }


}