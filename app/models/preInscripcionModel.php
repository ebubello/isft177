<?php


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


require URL_APP . '/libs/PHPMailer/src/Exception.php';
require URL_APP . '/libs/PHPMailer/src/SMTP.php';
require URL_APP . '/libs/PHPMailer/src/PHPMailer.php';



class preInscripcionModel
{

  private $db;

  public function __construct()
  {

    $this->db = new Base;
    // $this->volver = $this->controller('home');
  }



  public function enviarEmail($datos)
  {


    $lastName = $datos['LastName'];
    $name     = $datos['Name'];
    $varMail  = $datos['Email'];

    $mail = new PHPMailer(true);

    try {
      //Server settings
      //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
      $mail->isSMTP();                                              // Send using SMTP
      $mail->Host       = 'smtp.gmail.com';                         // Set the SMTP server to send through
      $mail->SMTPAuth   = true;                                     // Enable SMTP authentication
      $mail->Username   = EMAIL;                                    // SMTP username
      $mail->Password   = EMAIL_PASSWORD;                           // SMTP password
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;           // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
      $mail->Port       = 587;                                      // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

      //Recipients
      $mail->setFrom($varMail, 'noreply');
      $mail->addAddress(EMAIL);                                     // Add a recipient
      $mail->addAddress($varMail);                                  // Name is optional
      $mail->addReplyTo($varMail, $lastName);
      //$mail->addCC($varMail);
      // $mail->addBCC('bcc@example.com');

      // Attachments
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Christmas_in_Prague-Joyce_Hannam.pdf');  
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Christmas_in_Prague-Joyce_Hannam.pdf');         // Add attachments
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Grammar Revision File 7 and 8.pdf');  
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Vocabulary Revision File 8.pdf');  
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/logo.jpg', 'logo.jpg');    // Optional name
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Christmas_in_Prague-Joyce_Hannam - copia.pdf');         // Add attachments
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Grammar Revision File 7 and 8 - copia.pdf');  
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Vocabulary Revision File 8 - copia.pdf');  
      //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/logo - copia.jpg', 'logo2.jpg'); 

      // Content
      $mail->isHTML(true);                                          // Set email format to HTML
      $mail->Subject = 'Enviado por: ' . $mail->Username;
      $mail->Body    = '<h1 align=center>Nombre: ' . $lastName . ' ' . $name . '<br>Email: ' . $varMail . '<br>Tu pre inscripción está siendo analizada, en breve nos pondremos en contacto para darte más indicaciones.</h1>';
      // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      $mail->send();
      //echo 'Message has been sent';
      return true;
    } catch (Exception $e) {
      echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
      return false;
    }
  }

  public function saveFiles($datosAdjuntos)
  {
    $size = count($datosAdjuntos); //Sabiendo el tamaño del array, se de que metodo viene el mismo.

    //Entra por acá, si el metodo que llamo a saveFiles es SavePreInscription() y el tamaño del array es mayor a 4.
    if (!empty($datosAdjuntos) && $size > 4 ) {
      $folderName = $datosAdjuntos['DNI'];
      $foto       = $datosAdjuntos['ConsFoto'];
      $ABC        = $datosAdjuntos['ConsABC'];
      $DNI        = $datosAdjuntos['ConsDNI'];
      $LS         = $datosAdjuntos['ConsLS'];
      $PNac       = $datosAdjuntos['ConsPNac'];
      $Titulo     = $datosAdjuntos['ConsTitulo'];
      $Insc       = $datosAdjuntos['ConsInsc'];
      $Regla      = $datosAdjuntos['ConsRegla'];

      //$tipoArchivo = strtolower(pathinfo($foto, PATHINFO_EXTENSION));

      $carpeta = UPLOAD_FILES . $folderName . '/';

      if (!file_exists($carpeta)) {
        mkdir($carpeta, 0777, true);
      }


      move_uploaded_file($_FILES["InputFilefoto"]["tmp_name"], $carpeta . $foto);
      move_uploaded_file($_FILES["InputFileABC"]["tmp_name"], $carpeta . $ABC);
      move_uploaded_file($_FILES["InputFileDNI"]["tmp_name"], $carpeta . $DNI);
      move_uploaded_file($_FILES["InputFileLS"]["tmp_name"], $carpeta . $LS);
      move_uploaded_file($_FILES["InputFilePNac"]["tmp_name"], $carpeta . $PNac);
      move_uploaded_file($_FILES["InputFileTitulo"]["tmp_name"], $carpeta . $Titulo);
      move_uploaded_file($_FILES["InputFileinscripcion"]["tmp_name"], $carpeta . $Insc);
      move_uploaded_file($_FILES["InputFileReglamento"]["tmp_name"], $carpeta . $Regla);

      return true;

    } else if (!empty($datosAdjuntos) && $size == 4 ) {

      $folderName = $datosAdjuntos['DNI'];
      $ABC        = $datosAdjuntos['ConsABC'];
      $Insc       = $datosAdjuntos['ConsInsc'];

      $carpeta = UPLOAD_FILES . $folderName . '/';

      if (!file_exists($carpeta)) {
        mkdir($carpeta, 0777, true);
      }

      move_uploaded_file($_FILES["InputFileABC"]["tmp_name"], $carpeta . $ABC);
      move_uploaded_file($_FILES["InputFileinscripcion"]["tmp_name"], $carpeta . $Insc);

      return true;

    }else {

      return false;
    }

  }

  public function saveInfo($datos)
  {
    $size = count($datos); //Sabiendo el tamaño del array, se de que metodo viene el mismo.

    $status = "--";

    if (!empty($datos) && $size > 4) {


      $this->db->query('call sp_save_preinscription (:LastName , :Name , :Address , :DNI , :Email , :Phone, :status, :technical_career, :path, :action)');

      $this->db->bind(':LastName', $datos['LastName']);
      $this->db->bind(':Name', $datos['Name']);
      $this->db->bind(':Address', $datos['Address']);
      $this->db->bind(':DNI', $datos['DNI']);
      $this->db->bind(':Email', $datos['Email']);
      $this->db->bind(':Phone', $datos['Phone']);
      $this->db->bind(':status', $status);
      $this->db->bind(':technical_career', $datos['techCareer']);
      $this->db->bind(':path', UPLOAD_FILES . $datos['DNI'] . '/');
      $this->db->bind(':action', 'A');

      if ($this->db->execute()) {

        return true;
      }

    } else if(!empty($datos) && $size == 4) {

      $this->db->query('call sp_save_pre_inscription_2 (:DNI, :technical_career)');

      $this->db->bind(':DNI', $datos['DNI']);
      $this->db->bind(':technical_career', $datos['techCareer']);
     

     if ($this->db->execute()) {

        return true;
      }
    }else {
      return false;
    }
  }


  public function saveDocument($datos)
  {
    //Guarda 4 veces y deberia guardar 2---Ver que onda
    /* ver de meter el nombre del file directamete que viene desde el formulario */
    if (!empty($datos)) {

      $carpeta = UPLOAD_FILES . $datos['DNI'];

      if (is_dir($carpeta)) {
        if ($dir = opendir($carpeta)) {
          while (($archivo = readdir($dir)) !== false) {
            if ($archivo != '.' && $archivo != '..' && $archivo != '.htaccess') {

              $this->db->query('call sp_save_pre_inscription_document (:dni , :doc_description, :id_status, :idTecCareer, :doctype)');

              $this->db->bind(':dni', $datos['DNI']);
              $this->db->bind(':doc_description', $archivo);
              $this->db->bind(':id_status', 3);
              $this->db->bind(':idTecCareer', $datos['techCareer']);
              $this->db->bind(':doctype', 'C'); //Por default le asigno C a todos los registros de la PreInscripcion y luego lo cambio en la DB.
              $this->db->execute();
            }
          }
          closedir($dir);
        }
      }

      if ($this->updateDocument($datos['DNI'], $datos['techCareer'])) {
        return true;
      } else {

        return false;
      }
    }
  }

  public function updateDocument($dni, $idTec)
  {

    $this->db->query('call sp_update_pre_inscription_document (:dni , :idTecCareer )');
    $this->db->bind(':dni', $dni);
    $this->db->bind(':idTecCareer', $idTec);
    // $this->db->execute();

    if ($this->db->execute()) {

      return true;
    } else {
      return false;
    }
  }


  public function validarPreInscripcion($dni)
  {

    $this->db->query('call sp_validate_dni(:dni)');
    $this->db->bind(':dni', $dni);
    $this->db->register();

    $result = $this->db->rowCount();
    
    return $result;
    
  }


  public function technicalCareerList()
  {

    $this->db->query('SELECT distinct id, description FROM technical_career');

    $result = $this->db->registers();

    return $result;
  }

  public function technicalCareerUsed($dni)
  {

    $this->db->query('SELECT distinct id, description FROM technical_career where  id not  in (select technical_career from pre_inscription where dni = :dni)');
     $this->db->bind(':dni', $dni);
    $result = $this->db->registers();

    return $result;
  }


  public function saveDocumentShort($datos){
    if (!empty($datos)) {

      
      $this->db->query('call sp_save_pre_inscription_document2 (:dni , :doc_description, :doc_description2, :id_status, :idTecCareer, :doctype)');

      $this->db->bind(':dni', $datos['DNI']);
      $this->db->bind(':doc_description', $datos['ConsABC']);
      $this->db->bind(':doc_description2', $datos['ConsInsc']);
      $this->db->bind(':id_status', 3);
      $this->db->bind(':idTecCareer', $datos['techCareer']);
      $this->db->bind(':doctype', 'U'); //Por default le asigno C a todos los registros de la PreInscripcion y luego lo cambio en la DB.
     // $this->db->execute();

      if ($this->db->execute()) {
          return true;
      }else {
          return false;
      }
      

    }
  }

  public function enviarEmailShort($datos)
  {
    $this->db->query('SELECT DISTINCT last_name as LastName , name as Name , email as Email FROM pre_inscription where dni = :dni');
    $this->db->bind(':dni', $datos['DNI']);
   $result = $this->db->registers();

   $datos = [
    'LastName'=> $result[0]['LastName'],
    'Name'=> $result[0]['Name'],
    'Email'=> $result[0]['Email'], 
   ];


   if($this->enviarEmail($datos)){
     return true;
   }else {
     return false;
   }

  }


}
