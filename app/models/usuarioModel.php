<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


// require URL_APP . '/libs/PHPMailer/src/Exception.php';
// require URL_APP . '/libs/PHPMailer/src/SMTP.php';
// require URL_APP . '/libs/PHPMailer/src/PHPMailer.php';

class usuarioModel
{


    private $db;

    public function __construct()
    {
        // Se instancia la clase "Base" para facilitar ejecucion de comandos SQL
        $this->db = new Base;
    }

    //recibo el DNI desde la sesion creada y obtengo el userName.
    public function getUsuario($datosSession)
    {
        $this->db->query('SELECT username FROM user WHERE dni = :dni');
        $this->db->bind(':dni', $datosSession);
        $result = $this->db->register();

        return $result['username'];
    }

    public function verificarPassword($datosUsuario)
    {

        $this->db->query('SELECT password FROM user where dni = :dni');
        $this->db->bind(':dni', $datosUsuario['dni']);

        $rows   = $this->db->registers();
        foreach ($rows as $row) {
            $result =  $row['password'];
        }

        if (password_verify($datosUsuario['pass'], $result)) {
            return true;
        } else {
            return false;
        }
    }

    public function verificarDNI($datosRegister)
    {
        $this->db->query('SELECT * FROM user WHERE dni = :dni');
        $this->db->bind(':dni', $datosRegister['dni']);
        $this->db->execute();
        if ($this->db->rowCount()) { //En caso de que el email exista devuelve false , si no existe true
            return true;
        } else {
            return false;
        }
    }

    public function verificarEmail($datosRegister)
    {

        //$email = ;

        $this->db->query('SELECT * FROM user WHERE email = :email');
        $this->db->bind(':email', $datosRegister['email']);
        $this->db->execute();
        if ($this->db->rowCount()) { //En caso de que el email exista devuelve false , si no existe true
            return true;
        } else {
            return false;
        }
    }


    public function register($datosRegister)
    {
        $this->db->query('INSERT INTO user (username , dni , email , password, idrol) VALUES (:username , :dni , :email , :password, :idrol)');
        $this->db->bind(':username', $datosRegister['username']);
        $this->db->bind(':dni', $datosRegister['dni']);
        $this->db->bind(':email', $datosRegister['email']);
        $this->db->bind(':password', $datosRegister['password']);
        $this->db->bind(':idrol', $datosRegister['idrol']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updatepassword($email, $passhash) //Actualizo la pass.
    {
        $this->db->query('UPDATE user SET password=:contrasena WHERE email=:correo');
        $this->db->bind(':contrasena', $passhash);
        $this->db->bind(':correo', $email);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function sendEmailForgotPass($datos)
    {

        $varMail  = $datos['email'];

        $_SESSION['emailForgot'] = $varMail; //Creo una variable de sesion para poder validar que el email es el correcto.

        $link = URL_PROJECT . '/Usuario/reset/' . $varMail;

        $mail = new PHPMailer(true);

        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                              // Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                         // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                     // Enable SMTP authentication
            $mail->Username   = EMAIL;                                    // SMTP username
            $mail->Password   = EMAIL_PASSWORD;                           // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;           // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = 587;                                      // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom($varMail, 'noreply');
            $mail->addAddress(EMAIL);                                     // Add a recipient
            $mail->addAddress($varMail);                                  // Name is optional
            $mail->addReplyTo($varMail);


            // Attachments
            //   $mail->addAttachment(URL_APP . '/libs/PHPMailer/files/Christmas_in_Prague-Joyce_Hannam.pdf');  
            // Content
            $mail->isHTML(true);                                          // Set email format to HTML
            $mail->Subject = 'Enviado por: ' . $mail->Username;
            $mail->Body    = '<h1 align=center><br>Email: ' . $varMail . '<br>Usted cuenta con 10 minutos para restablecer contraseña, pasado el tiempo, debera volver a realizar el proceso. </h1><br><h2><a href=' . $link . '>Restablecer Contraseña</a></h2>';


            $mail->send();
            //echo 'Message has been sent';
            return true;
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            return false;
        }
    }
}
