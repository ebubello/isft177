<!-- Footer -->
<footer class="page-footer font-small bg-dark pt-2 mt-4">

  <!-- Footer Links -->
  <div class="container text-center text-md-left">

    <!-- Footer links -->
    <div class="row text-center text-md-left mt-1 pb-3">

      <!-- Grid column -->
      <div class="col-md-3 col-lg-3 col-xl-4 mx-auto mt-3">
        <h6 class="text-uppercase text-white mb-4 font-weight-bold">Instituto Superior de Formación Técnica N° 177</h6>
        <p class="text-white ">
          <!--  <a href="<?php echo URL_PROJECT ?>/home/institucion">Institución</a> -->
          <section class="container-fluid d-flex justify-content-center align-items-left">
            <a href=""><img src="<?php echo URL_PROJECT ?>/img/home/logoisft177.png" width="121" height="121" class="d-inline-block align-top rounded " alt="Logo ISFT 177" title="Instituto de Formación Tecnica Superior N°177"></a>
          </section>
        </p>
      </div>
      <!-- Grid column -->

      <hr class="w-100 clearfix d-md-none">

      <!-- Grid column -->
      <!-- <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
       <h6 class="text-uppercase text-white mb-4 font-weight-bold">Carreras</h6>
          <p>
            <a class="item text-white" href="<?php echo URL_PROJECT ?>/home/tSistemas">Tec. Análisis de Sistemas</a>
        </p>
        <p>
        <a class="text-white" href="<?php echo URL_PROJECT ?>/home/tClinicos">Tec. Laboratorio de Análisis Clínico</a>
        </p>
        <p>
        <a class="text-white" href="<?php echo URL_PROJECT ?>/home/tContable">Tec. Administración Contable</a>
        </p>
        <p>
        <a class="text-white" href="<?php echo URL_PROJECT ?>/home/tGestion">Tec. Gestión Ambiental y Salud</a>
        </p>
      </div> -->
      <!-- Grid column -->

      <!--       <hr class="w-100 clearfix d-md-none"> -->

      <!-- Grid column -->
      <div class="col-md-4 col-lg-2 col-xl-4 mx-auto mt-3">
        <h6 class="text-uppercase text-white mb-4 font-weight-bold">Carreras</h6>
        <p>
          <a class="text-white mr-3" href="<?php echo URL_PROJECT ?>/home/tSistemas" target="_blank">Tec. Análisis de Sistemas</a>
        </p>
        <p>
          <a class="text-white mr-3" href="<?php echo URL_PROJECT ?>/home/tClinicos" target="_blank">Tec. Laboratorio de Análisis Clínicos</a>
        </p>
        <p>
          <a class="text-white mr-3" href="<?php echo URL_PROJECT ?>/home/tContable" target="_blank">Tec. Administración Contable</a>
        </p>
        <p>
          <a class="text-white mr-3" href="<?php echo URL_PROJECT ?>/home/tGestion" target="_blank">Tec. Gestión Ambiental y Salud</a>
        </p>
      </div>

      <!-- Grid column -->
      <hr class="w-100 clearfix d-md-none">

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="text-uppercase text-white mb-4 font-weight-bold">Contacto</h6>
        <p class="text-white">
          <i class="fas fa-home text-white mr-3"></i> Zapiola 1420, Libertad, Merlo, Buenos Aires.</p>
        <p class="text-white">
          <i class="fas fa-envelope text-white mr-3"></i>isft177.libertad@gmail.com</p>
        <p class="text-white">
          <i class="fas fa-phone text-white mr-3"></i> (0220)-4942076 </p>



      </div>
      <!-- Grid column -->

    </div>
    <!-- Footer links -->

    <hr>

    <!-- Grid row -->
    <div class="row d-flex align-items-center">

      <!-- Grid column -->
      <div class="col-md-7 col-lg-8">

        <!--Copyright-->
        <p class="text-center text-white-50 text-md-left">© 2020 Copyright:
          <!--    <a href="https://mdbootstrap.com/"> -->
          <strong>ISFT N° 177</strong>
          </a>
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-5 col-lg-4 ml-lg-0">

        <!-- Social buttons -->
        <!--  <div class="text-center text-md-right">
          <ul class="list-unstyled list-inline">
            <li class="list-inline-item text-white">
              <a class="btn-floating btn-sm rgba-white-slight mx-1" href="https://www.facebook.com/ISFT177.Libertad/">
                <i class="btn-facebook">
                </i>
              </a>
            </li> -->
        <!-- <li class="list-inline-item text-white">
              <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-twitter"></i>
              </a>
            </li> -->
        <!-- <li class="list-inline-item text-white">
              <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-google-plus-g"></i>
              </a>
            </li> -->
        <!-- <li class="list-inline-item text-white">
              <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li> -->
        </ul>
      </div>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  </div>
  <!-- Footer Links -->
  <script>
    $(document).ready(function() {
      $(window).scroll(function() {
        if ($(this).scrollTop() > 50) {
          $('#back-to-top').fadeIn();
        } else {
          $('#back-to-top').fadeOut();
        }
      });
      // scroll body to 0px on click
      $('#back-to-top').click(function() {
        $('body,html').animate({
          scrollTop: 0
        }, 400);
        return false;
      });
    });
  </script>
  <a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button"><i class="fas fa-angle-double-up"></i></i></a>
</footer>
<!-- Footer -->

</body>

</html>