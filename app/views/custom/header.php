<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    
    <!-- Estilos Bootstrap Local -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

    
    
    <!-- JS Bootstrap Local -->
    <script src="<?php echo URL_PROJECT ?>/js/jquery/jquery-3.5.1.min.js"></script>
    <script src="<?php echo URL_PROJECT ?>/js/popper/popper1.16.0.min.js"></script>
    <script src="<?php echo URL_PROJECT ?>/js/bootstrap/bootstrap.min.js"></script>

    <!-- Etiqueta global del sitio (gtag.js) - Google Analytics -->
      <script async src = "https://www.googletagmanager.com/gtag/js?id=G-5X9QLDGQ94"> </script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag () {dataLayer.push (argumentos);}
        gtag ('js', nueva fecha ());

        gtag ('configuración', 'G-5X9QLDGQ94');
    </script>

    
    <!-- GoogleFonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@700&display=swap" rel="stylesheet">
    <!-- Css -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_home.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_contacto.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_inscripcion.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/login/my-login.css">
    
    <style>
        .back-to-top {
    position: fixed;
    bottom: 88px;
    right: 25px;
    background-color: #f2ca52;
    display: none;
}
    </style>

    
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/sweetAlert2/sweetalert2.min.css">
        <!-- Animated -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/sweetAlert2/animate.min.css">

     <!-- DashBoard Css -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/dashboard/simple-sidebar.css">
   
    <!-- JavaScript -->
    <script src="<?php echo URL_PROJECT ?>/js/preInscripcionJS/script.js"></script>

    <!-- Font Awesome -->
    <script defer src="<?php echo URL_PROJECT ?>/js/dashboard/all.js"></script> <!--load all styles -->


    <!-- Iconos Feather -->
    <script src="<?php echo URL_PROJECT ?>/js/dashboard/feather.min.js"></script>


    <!-- Start of  Zendesk Widget script -->
      <!-- <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=8850c382-4abb-46bc-8e63-97098825cd9f"> </script> -->
    <!-- End of  Zendesk Widget script -->

        <!--Start of Tawk.to Script-->
          <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5f483c51cc6a6a5947af721e/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
          </script>
        <!--End of Tawk.to Script-->

    <!-- Icono Pesataña -->
    <link rel="icon" href="<?php echo URL_PROJECT ?>/img/home/logoisft177.png">

    <!-- Titulo -->
    <title><?php echo NAME_PROJECT ?></title>

</head>

<body>

