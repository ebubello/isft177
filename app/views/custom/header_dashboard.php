<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    
    <!-- Estilos Bootstrap Local -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

    
    
    <!-- JS Bootstrap Local -->
    <script src="<?php echo URL_PROJECT ?>/js/jquery/jquery-3.5.1.min.js"></script>
    <script src="<?php echo URL_PROJECT ?>/js/popper/popper1.16.0.min.js"></script>
    <script src="<?php echo URL_PROJECT ?>/js/bootstrap/bootstrap.min.js"></script>

    
    <!-- GoogleFonts -->
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">

    <!-- Css -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_home.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_contacto.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_inscripcion.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/login/my-login.css">

     <!-- DashBoard Css -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/dashboard/simple-sidebar.css">

     <!-- SweetAlert2 -->
     <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/sweetAlert2/sweetalert2.min.css">
        <!-- Animated -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/sweetAlert2/animate.min.css">

   
    <!-- JavaScript -->
    <script src="<?php echo URL_PROJECT ?>/js/preInscripcionJS/script.js"></script>

    <!-- Font Awesome -->
    <script defer src="<?php echo URL_PROJECT ?>/js/dashboard/all.js"></script> <!--load all styles -->


    <!-- Iconos Feather -->
    <script src="<?php echo URL_PROJECT ?>/js/dashboard/feather.min.js"></script>


    <!-- Icono Pesataña -->
    <link rel="icon" href="<?php echo URL_PROJECT ?>/img/home/logoisft177.png">

    <!-- Titulo -->
    <title><?php echo NAME_PROJECT ?></title>

</head>

<body>

