<div class="d-flex" id="wrapper">

  <!-- Sidebar -->
  <div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading"><i class="fas fa-user-secret"></i>&nbsp;&nbsp; <strong> <?php echo $sesion->getUser(); ?> </strong></div>
    <div class="list-group list-group-flush">
      <a href="<?php echo URL_PROJECT ?>/Dashboard/dashboard" class="list-group-item list-group-item-action bg-light"><i data-feather="command"></i>&nbsp;&nbsp;Dashboard</a>
      <a href="<?php echo URL_PROJECT ?>/Dashboard/dashPreInView" class="list-group-item list-group-item-action bg-light"><i data-feather="file-text"></i>&nbsp;&nbsp;Preinscripciones</a>
      <a href="<?php echo URL_PROJECT ?>/home/crud_novedades" class="list-group-item list-group-item-action bg-light"><i data-feather="bookmark"></i></i></i>&nbsp;&nbsp;Novedades</a>
      <a href="<?php echo URL_PROJECT ?>/home/crud_anuncios" class="list-group-item list-group-item-action bg-light"><i data-feather="message-square"></i></i></i>&nbsp;&nbsp;Anuncios</a>
      <a href="<?php echo URL_PROJECT ?>/home/help" class="list-group-item list-group-item-action bg-light"><i data-feather="help-circle"></i></i></i>&nbsp;&nbsp;Ayuda</a>
      
      
    </div>

  </div>



  <!-- /#sidebar-wrapper -->

  <!-- Page Content -->
  <div id="page-content-wrapper">

    <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
      <button class="btn btn-light " id="menu-toggle"><i data-feather="menu"></i></button>


      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i data-feather="sidebar"></i>&nbsp;Opciones
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              
              <!-- Ver porque no se genera la ruta -->
              <?php  
                $UserSession= $sesion->getRolbyUserSession();
                if( $UserSession == 1){
                echo ('<a class="dropdown-item" id="cRegister" href="../Usuario/register"><i data-feather="user-plus"></i>&nbsp;&nbsp;Crear Usuario</a>
                 <a class="dropdown-item" data-toggle="modal" href="#myModal"><i data-feather="unlock"></i>&nbsp;&nbsp;Activar PreInscripción</a>
                 <a class="dropdown-item" data-toggle="modal" href="#myModalConf"><i data-feather="unlock"></i>&nbsp;&nbsp;Habilitar PreInscripción</a>
                <div class="dropdown-divider"></div>');
                }
                ?>
             


            
            <a class="dropdown-item" href="<?php echo URL_PROJECT ?>/Usuario/logOut"><i data-feather="log-out"></i>&nbsp;&nbsp;Cerrar Sesión</a>
      </div>
      </li>
      </ul>
  </div>
  </nav>


