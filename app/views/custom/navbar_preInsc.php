

    <!-- Comienzo de barra de navegación -->
    <nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark ">
        <a class="navbar-brand" href="<?php echo URL_PROJECT ?>/home" title="Instituto de Formación Tecnica Superior N°177">
            <img src="<?php echo URL_PROJECT ?>/img/home/logoisft177.png" width="33" height="33" class="d-inline-block align-top rounded" alt="Logo ISFT 177"> ISFT 177
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo URL_PROJECT ?>/home"> Inicio</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo URL_PROJECT ?>/home/institucion">Institución</a>
                </li>
                <li class="nav-item dropdown active ">
                    <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Carreras
                    </a>
                    <div class="dropdown-menu bg-secondary" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/tSistemas">Tec. Análisis de Sistemas</a>
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/tClinicos">Tec. Laboratorio de Análisis Clínicos</a>
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/tContable">Tec. Administración Contable</a>
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/tGestion">Tec. Gestión Ambiental y Salud</a>
                    </div>
                </li>
                <li class="nav-item dropdown active ">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Alumnos
                    </a>
                    <div class="dropdown-menu bg-secondary" aria-labelledby="navbarDropdownMenuLink">
                        <?php
                       
                       $d = new dashboardModel();
                       
                       echo ($d->selectConfig());
                       
                        ?>
                        <!-- <a class="dropdown-item text-white bg-secondary" href="#">Documentos</a> -->
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/Resoluciones">Resoluciones</a>
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/Reglamento">Reglamento</a>
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/Constancias">Constancias</a>
                        <a class="dropdown-item text-white bg-secondary" href="<?php echo URL_PROJECT ?>/home/CAI">CAI</a>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo URL_PROJECT ?>/home/contacto">Contacto</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo URL_PROJECT?>/Usuario/login">Intranet <img src="<?php echo URL_PROJECT ?>/public/img/home/intranet.png" width="15" height="15" class="fondoiconintranet rounded d-inline-block align-center" alt=""></a>
                </li>
            </ul>

            <div class="ml-auto">
            <div id="fecha">
                <script type="text/javascript">
                    var fecha = new Date()
                    var anio = fecha.getYear()
                    if (anio < 1000)
                        anio += 1900
                    var dia = fecha.getDay()
                    var mes = fecha.getMonth()
                    var diam = fecha.getDate()
                    if (diam < 10)
                        diam = "0" + diam
                    var diavec = new Array("Domingo", "Lunes", "Martes", "Mi&eacute;rcoles", "Jueves", "Viernes", "S&aacute;bado")
                    var mesvec = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
                    document.write("<div><small><font class='mr-2' color='white' face='Arial'>" + diavec[dia] + " " + diam + " de " + mesvec[mes] +
                        " de " + anio + "</font></div></small>")
                </script>

            </div>

               
            </div>
            <a href="https://www.facebook.com/ISFT177.Libertad/" target="_blank" class="btn btn-outline-light p-0 border-0">
                    <img src="<?php echo URL_PROJECT ?>/public/img/home/iconface.png" width="35" height="35" class="d-inline-block align-top" alt="">
                </a>
                <a  class="btn btn-outline-light p-0 border-0">
                    <img src="<?php echo URL_PROJECT ?>/public/img/home/iconinsta.png" width="35" height="35" class="d-inline-block align-top" alt="">
                </a>
        </div>
    </nav>



    <!--Final de barra de navegación -->