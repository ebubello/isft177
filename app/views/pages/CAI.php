<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>
<!-- Container principal -->
<div class="col-lg-16 container coloryellow mt-1">
    <div class="col-lg-20">
        <div class="tab-content">

       <!-- Contenido de CAI-->
        <div class="tab-pane fade show active" id="list-perfil" role="tabpanel" aria-labelledby="list-perfil-list">
              <div class="d-flex coloryellow ml-0 rounded-0">
                    <div class="container">
                         <h3 class="font-weight-bold mt-4 ml-0 display-5 letragoogletitulo">Consejo Académico Institucional</h3>
                         <hr class="my-4">
                         <h4 class="mt-0 ml-0 display-5 letragoogletitulo">Naturaleza y Fuciones del Consejo Académico Institucional</h4><br>
           
                         <p class="text-justify">
                              Los Consejos Académicos Institucionales serán los órganos colegiados de asesoramiento, propuesta, debate y decisión, para una gestión participativa, en el marco de la autonomía institucional en la unidad del sistema del nivel superior, dentro de las regulaciones de la política educativa jurisdiccional y federal.
                          </p>
                          <p class="text-justify">
                              Los Consejos Académicos Institucionales deberán organizarse y se desarrollarán conforme a los reglamentos internos que los mismos otorguen, en el marco de lo establecido en la presente reglamentación.
                          </p>
                          <p class="text-justify">
                              Las decisiones del CAI tendrán carácter vinculante para la Institución en aquellos tópicos detallados en la presente y carácter de asesoramiento en toda otra cuestión que trate a requerimiento de los equipos de conducción.
                          </p>
                          <p>
                              <a href="<?php echo URL_PROJECT ?>/documents/CAI/CAI 2009-res4044.pdf" target="_blank">
                              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
                                <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
                                <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/> 
                              </svg> Descargar el Marco Normativo del Consejo Académico Institucional</a>
                          </p>                             
                    </div>                        
              </div>
        </div>
       <!-- fin CAI -->                    
        </div>
    </div>
</div> 
<!--Fin del container principal  -->
<br>
<?php
// Footer
include_once URL_APP . '/views/custom/footer.php';

?>