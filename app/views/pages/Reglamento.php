<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<!-- Container principal -->
<div class="col-lg-16 container coloryellow mt-1">
  <div class="col-lg-20">
    <div class="tab-content">

      <!-- Contenido de REGLAMENTO-->
      <div class="tab-pane fade show active" id="list-perfil" role="tabpanel" aria-labelledby="list-perfil-list">
        <div class="d-flex coloryellow ml-0 rounded-0">
          <div class="container">
            <h3 class="font-weight-bold mt-4 ml-0 display-5 letragoogletitulo">Reglamento para el alumno</h3>
            <hr class="my-4">
            <h4 class="mt-0 ml-0 display-5 letragoogletitulo">Orientaciones y deberes de los alumnos</h4><br>

            <p class="text-justify">
              Para el logro de los objetivos planteados cada alumno deberá participar, manifestando respeto y adhesión a las distintas actividades propuestas, tales como: Fiesta de Bienvenida, Fiesta de Colación de Grado, Actos Escolares, Espacios de Reflexión, Jornadas Institucionales, etc.
            </p>
            <p class="text-justify">
              Es deber de todos colaborar para que en el Instituto reine un ambiente de trabajo guardando una conducta acorde:
            </p>
            <p>
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
              </svg>
              Dejando el aula ordenada al retirarse.
            </p>
            <p>
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
              </svg>
              Manteniendo la higiene de los baños.
            </p>
            <p>
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
              </svg>
              Arrojando los papeles y desperdicios en los cestos.
            </p>
            <p>
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
              </svg>
              Dando muestras de modestia y buen gusto en el vestir (no se podrá asistir con gorra, bermudas, musculosas, ojotas, shorts, etc).
            </p>
            <p>
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
              </svg>
              No ingresando al Instituto con armas ni bebidas alcohólicas de ningún tipo.
            </p>
            <p>
              <a href="<?php echo URL_PROJECT ?>/documents/Reglamento/Reglamento alumnos.pdf" target="_blank">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z" />
                  <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z" />
                  <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z" />
                </svg>
                Descargar el Reglamento del Alumno Completo.</a>
            </p>
          </div>
        </div>
      </div>
      <!-- fin REGLAMENTO-->
    </div>
  </div>
</div>
<!--Fin del container principal  -->
<br>

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>