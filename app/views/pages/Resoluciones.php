<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<!-- Container principal -->
<div class="col-lg-16 container coloryellow mt-1">
    <div class="col-lg-20">
        <div class="tab-content">

       <!-- Contenido de RESOLUCIONES-->
        <div class="tab-pane fade show active" id="list-perfil" role="tabpanel" aria-labelledby="list-perfil-list">
              <div class="d-flex coloryellow ml-0 rounded-0">
                    <div class="container">
                         <h3 class="font-weight-bold mt-4 ml-0 display-5 letragoogletitulo">Resoluciones</h3>
                         <hr class="my-4">
                         <h4 class="mt-0 ml-0 display-5 letragoogletitulo">Se dejan los archivos PDF de las resoluciones de cada carrera:</h4><br>
                          <p>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg>
                            Tecnicatura Superior en Análisis de Sistemas - <a href="<?php echo URL_PROJECT ?>/documents/Resoluciones/SISTEMAS-RSC-6790-2019-ANEXO -IF-2019-36021554-GDEBA-CPEYTDGCYE.pdf" target="_blank">Resolución 6790/19</a>
                         </p>
                         <p>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg>
                            Tecnicatura Superior en Laboratorio de Análisis Clínicos - <a href="<?php echo URL_PROJECT ?>/documents/Resoluciones/205-18-ResClinicos.pdf" target="_blank">Resolución 205/18</a> 
                         </p>
                         <p>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg>
                            Tecnicatura Superior en Administración Contable - <a href="<?php echo URL_PROJECT ?>/documents/Resoluciones/admcontable_res273_03.pdf" target="_blank">Resolución 273/03</a>
                         </p>
                         <p>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg>
                            Tecnicatura Superior en Gestión Ambiental y Salud - <a href="<?php echo URL_PROJECT ?>/documents/Resoluciones/Ambientalysalud-res442-08.pdf" target="_blank">Resolución 442/08</a>
                         </p>
                         <p>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg>
                            Régimen Académico - <a href="<?php echo URL_PROJECT ?>/documents/Resoluciones/Res_4043_09_regimen-academico-final-Resolucion.pdf" target="_blank">Resolución 4043/09</a>
                         </p>
                         <p>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg>
                            Consejo Federal de Educación - <a href="<?php echo URL_PROJECT ?>/documents/Resoluciones/Res 72-08 CFE.pdf" target="_blank">Resolución 72/08</a>
                         </p>
                         <p>
                          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
                          </svg>
                            Ayudantes de Cátedra - <a href="<?php echo URL_PROJECT ?>/documents/Resoluciones/Ayudantedecatedra-res3121.pdf" target="_blank">Resolución 3121/04</a>
                         </p>                       
                    </div>                        
              </div>
        </div>
       <!-- fin RESOLUCIONES-->                    
        </div>
    </div>
</div> 
<!--Fin del container principal  -->
<br>

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>