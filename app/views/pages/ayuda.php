<?php
$sesion = new UserSession();

include_once URL_APP . '/views/custom/header_dashboard.php';

include_once URL_APP . '/views/custom/navbar_dashboard.php'; //Acá hay que ver de hacer que funcione esta navBar que es la del Dashboard.

?>
<div class="container">

    <div class="list-group">
        <h1 class="search-bar__subtitle mb-2 mt-4">Ayuda</h1>
        <a href="<?php echo URL_PROJECT ?>/documentacion/Manual_del_Usuario_sitio_web_177-PreInscripciones.pdf" target="_blank" class="list-group-item list-group-item-action mt-4"> <img src="<?php echo URL_PROJECT ?>/img/icons/pdf.svg" height="35" width="35" alt="pdf">   Preinscripciones</a>
        <a href="<?php echo URL_PROJECT ?>/documentacion/Instructivo de carga y administración de novedades y anuncios - Sitio web ISFT 177.pdf" target="_blank" class="list-group-item list-group-item-action"> <img src="<?php echo URL_PROJECT ?>/img/icons/pdf.svg" height="35" width="35" alt="pdf"> Novedades y Anuncios</a>
        <a href="<?php echo URL_PROJECT ?>/documentacion/Instructivo de administración y uso del chat tawk.to  - Sitio web ISFT 177.pdf" target="_blank" class="list-group-item list-group-item-action"> <img src="<?php echo URL_PROJECT ?>/img/icons/pdf.svg" height="35" width="35" alt="pdf"> Chat Tawk.to</a>
        <a href="<?php echo URL_PROJECT ?>/" class="list-group-item list-group-item-action disabled">Otros...</a>

        
    </div>

</div>







<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Activar PreInscripción</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="usr">DNI:</label>
                    <input type="text" class="form-control" id="usr" value="0">
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="activarDNI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
            </div>

        </div>
    </div>
</div>

</div>
<!-- Fin The Modal -->

<!-- The Modal Configuración-->
<div class="modal fade" id="myModalConf">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Habilitar PreInscripción</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <hr>
                <div class="form-group">
                    <h6 class="">Fecha Inicio:</h6>
                    <input id="fecha_inicio" name="fecha_inicio" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
                </div>
                <div class="form-group">
                    <h6 class="">Fecha Fin:</h6>
                    <input id="fecha_fin" name="fecha_fin" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button id="desactivarPI" type="button" class="btn btn-secondary" data-dismiss="modal">Desactivar</button>
                    <button id="activarPI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
                </div>

            </div>
        </div>
    </div>

</div>
<!-- Fin The Modal Configuración -->

<script>
    $(document).ready(function() {
        $("#myBtn").click(function() {
            $("#myModal3").modal();
        });
    });
</script>

<script type="text/javascript">
    function ConfirmDelete() {
        var respuesta = confirm("¿Esta seguro de eliminar el anuncio?")

        if (respuesta == true) {
            return true;
        } else {
            return false;
        }
    }
</script>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo URL_PROJECT ?>/js/usuario/enablePreInscription.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


    //  Feather Script
    feather.replace()
</script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


<!-- Inicio SweetAlert2 -->
<script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>

<script src="<?php echo URL_PROJECT ?>/js/dashboard/sweetAlertAll.js"></script>