<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';

//BANNER
include_once URL_APP . '/views/custom/banner.php';

// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<div class="container-fluid mt-4">



  <!--Section: Content-->
  <section class="dark-grey-text colorfondo p-3">

    <!-- Section heading -->
    <div class="container">
      <h2 class="display-5 mb-3 font-weight-bolder">Contáctenos</h2>
      <p class="mb-1 display-5 text-info ">Contáctenos completando el siguiente formulario o escríbanos al chat de la página en horarios operativos (Lun-Vie 18:30hs a 21:30hs).</p>
      <footer class="blockquote-footer">Cuerpo administrativo <cite title="Source Title">Instituto Superior de Formación Técnica 177</cite></footer>





    </div>

    <!-- Grid row -->
    <div class="row mt-4">

      <!-- Grid column -->
      <div class="col-lg-5 mb-lg-0 pb-lg-3 mb-4">

        <div class="container bordercustom bg-light">
          <h2 class="text-center mb-4 mt-4">Formulario de Contacto</h2>

          <form id="contactForm" name="contactform" class="popup-form" action="<?php echo URL_PROJECT ?>/Home/enviarContacto" method="POST" onsubmit="return alerta(event)">

            <div class="form-group col-sm-12 ">
              <input name="fname" id="fname" placeholder="Nombre*" class="form-control" type="text" required>
              <div class="input-group-icon"><img src="<?php echo URL_PROJECT ?>/public/img/contacto/nombre.png" alt="" width="25px"></div>
            </div><!-- end form-group -->

            <div class="form-group col-sm-12">
              <input name="email" id="email" placeholder="Email*" class="form-control" type="email" required>
              <div class="input-group-icon"><img src="<?php echo URL_PROJECT ?>/public/img/contacto/email.png" alt="" width="25px"></div>
            </div><!-- end form-group -->

            <div class="form-group col-sm-12">
              <input name="subject" id="subject" placeholder="Asunto*" class="form-control" type="text" required>
              <div class="input-group-icon"><img src="<?php echo URL_PROJECT ?>/public/img/contacto/asunto.png" alt="" width="25px"></div>
            </div><!-- end form-group -->

            <div class="form-group col-sm-12">
              <textarea rows="6" name="message" id="message" placeholder="Escribe tu mensaje aquí*" class="form-control" required></textarea>
              <div class="input-group-icon"><img src="<?php echo URL_PROJECT ?>/public/img/contacto/mensaje.png" alt="" width="25px"></div>

            </div><!-- end form-group -->

            <div class="form-group col-sm-12 d-flex">
              <button type="submit" id="submit" class="btn btn-custom">Enviar</button>
              <p class="ml-auto text-danger">* Campos requeridos</p>
            </div><!-- end form-group -->

          </form><!-- end form -->
        </div>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-lg-7">

        <!--Google map-->
        <div id="map-container-section " class="z-depth-1-half map-container-section mb-4">

          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13122.300393407293!2d-58.6882324!3d-34.6906719!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd02f9f0f990482a6!2sISFT%20177!5e0!3m2!1ses!2sar!4v1594419497159!5m2!1ses!2sar" frameborder="0" style="border:1;" allowfullscreen="" aria-hidden="false" tabindex="0" class=""></iframe>

        </div>
        <!-- Buttons-->
        <div class="row text-center">
          <div class="col-md-4">
            <img src="<?php echo URL_PROJECT ?>/img/contacto/mapa.png" alt="">
            <a href="https://goo.gl/maps/yo1VCtKgtQe6Yjxh6" class="text-info" target="_blank">
              <p class="mb-0 mt-1 ">Zapiola 1420 <br> Libertad-Merlo</p>
            </a>
          </div>
          <div class="col-md-4">
            <img src="<?php echo URL_PROJECT ?>/img/contacto/llamada.png" alt="">
            <a class="text-info" target="_blank">
              <p class="mb-0 mt-1 ">(0220)-4942076<br>Lun-Vie 18:30hs a 21:30hs </p>
            </a>
          </div>
          <div class="col-md-4">
            <img src="<?php echo URL_PROJECT ?>/img/contacto/email.png" alt="">
            <a class="text-info" target="_blank">
              <p class="mb-0 mt-1 ">isft177.libertad@gmail.com</p>
            </a>
          </div>
          <div class="col-md-4 mt-2">
            <img src="<?php echo URL_PROJECT ?>/img/contacto/facebook.png" alt="">
            <a href="https://www.facebook.com/ISFT177.Libertad/" class="text-info" target="_blank">
              <p class="mb-0 mt-1 ">@ISFT177.Libertad</p>
            </a>
          </div>
        </div>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->
  </section>
  <!--Section: Content-->
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->

<!-- Inicio SweetAlert2 -->
<script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>

<script>
function alerta(e) {
  Swal.fire({
      title: 'Recibimos tu email',
      imageUrl: '/img/home/inbox-gif.gif',
      imageHeight: 212,
      text: '¡Nos pondremos en contacto!',
      showConfirmButton: false,
      timer: 3000
    });
}
</script>



<!-- Fin SweetAlert2 -->

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>