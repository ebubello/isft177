<?php

$sesion = new UserSession();
//$xxx = new dashboardModel();

include_once URL_APP . '/views/custom/header_dashboard.php';

include_once URL_APP . '/views/custom/navbar_dashboard.php'; //Acá hay que ver de hacer que funcione esta navBar que es la del Dashboard.

?>


<main class="container-fluid p-4">
  <div class="row">

    <div class="col-md-12">

      <?php if (isset($_SESSION['message'])) { ?>
        <div class="alert alert-<?= $_SESSION['message_type'] ?> alert-dismissible fade show pb-3" role="alert">
          <?= $_SESSION['message'] ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <?php  } ?>

      <table class="table table-hover tableColor table-bordered">

        <div class="d-flex justify-content-between align-items-center">

          <h2 class="p-1 colorfondo2 rounded">ANUNCIOS</h2>

          <button type="button" class="btn btn-success" id="myBtn">+ Cargar Nuevo</button>

        </div>

        <thead class="thead-dark">
          <tr class="table animated fadeIn">
            <th>
              <div>ID</div>
            </th>
            <th>
              <div>Título</div>
            </th>
            <th>
              <div>Descripción</div>
            </th>
            <th>
              <div style="width: 40px;">Orden</div>
            </th>
            <th>
              <div style="width: 85px;">F_Inicio</div>
            </th>
            <th>
              <div style="width: 85px;">F_Fin</div>
            </th>
            <th>
              <div>Botón Link</div>
            </th>
            <!-- <th>
              <div>Nom_Link</div>
            </th> -->
            <th>
              <div>Imagen</div>
            </th>
            <th>
              <div style="width: 85px;">F_Alta</div>
            </th>
            <th>
              <div>Acción</div>
            </th>
          </tr>
        </thead>
        <tbody>


          <?php

          $resultado = $_SESSION['anuncios'];

          foreach ($resultado as $resultadoarray) { ?>
            <tr>
              <td><?php echo $resultadoarray->{"Id_Anuncio"}; ?></td>
              <td><?php if (strlen($resultadoarray->{"Titulo"}) > 10) {
                    echo substr($resultadoarray->{"Titulo"}, 0, 10) . " ...";
                  } else {
                    echo $resultadoarray->{"Titulo"};
                  }; ?></td>
              <td><?php if (strlen($resultadoarray->{"Descripcion"}) > 20) {
                    echo substr($resultadoarray->{"Descripcion"}, 0, 20) . " ...";
                  } else {
                    echo $resultadoarray->{"Descripcion"};
                  }; ?></td>
              <td class="text-center"><?php echo $resultadoarray->{"Prioridad"}; ?></td>
              <td><?php echo $resultadoarray->{"Fecha_Inicio"}; ?></td>
              <td><?php echo $resultadoarray->{"Fecha_Fin"}; ?></td>
              <td class="text-primary"><a href="<?php echo $resultadoarray->{"Link"} ?>" target="_blank">
              <?php if (strlen($resultadoarray->{"Link"}) > 30) {
                    echo substr($resultadoarray->{"Link"}, 0, 30) . " ...";
                  } else {
                    echo $resultadoarray->{"Link"};
                  }; ?></td>
              <!-- <td><?php if (strlen($resultadoarray->{"Nombre_Link"}) > 10) {
                    echo substr($resultadoarray->{"Nombre_Link"}, 0, 10) . " ...";
                  } else {
                    echo $resultadoarray->{"Nombre_Link"};
                  }; ?></td> -->
              <td><?php if (strlen($resultadoarray->{"Imagen"}) > 10) {
                    echo substr($resultadoarray->{"Imagen"}, 0, 10) . " ...";
                  } else {
                    echo $resultadoarray->{"Imagen"};
                  }; ?></td>
              <td><?php echo $resultadoarray->{"Fecha_Alta"}; ?></td>
              <td>
                <a href="<?php echo URL_PROJECT ?>/home/editar_anuncio?ID=<?php echo $resultadoarray->{"Id_Anuncio"} ?>" class="btn btn-primary" title="Editar">
                  <img src="<?php echo URL_PROJECT ?>/public/img/crudnovedades/editar.png">
                </a>
                <a href="<?php echo URL_PROJECT ?>/home/borrar_anuncio?ID=<?php echo $resultadoarray->{"Id_Anuncio"} ?>" class="btn btn-danger" title="Borrar" onclick="return ConfirmDelete()">
                  <img src="<?php echo URL_PROJECT ?>/public/img/crudnovedades/borrar.png">
                </a>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal3" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4><span class="glyphicon glyphicon-lock"></span>Nuevo Anuncio</h4>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body" style="padding:40px 50px;">
            <form action="<?php echo URL_PROJECT ?>/home/cargar_anuncio" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="titulo" class="h6">Título:</label>
                <input autocomplete="off" type="text" name="titulo" class="form-control" placeholder="Escribir título aquí" required >
              </div>
              <div class="form-group">
                <label for="descripcion" class="h6">Descripcion:</label>
                <textarea autocomplete="off" name="descripcion" rows="5" class="form-control" placeholder="Escribir descripción aquí" ></textarea>
              </div>
              <div class="form-group">
                <label for="prioridad" class="h6">Prioridad:</label>
                <input  type="number" name="prioridad" class="form-control" placeholder="Indicar prioridad" value="1" min="1" required >
              </div>
              <div class="form-group ">
                <label for="fecha_inicio" class="h6">Fecha de Inicio:</label>
                <input type="date" name="fecha_inicio" class="form-control" required >
              </div>
              <div class="form-group ">
                <label for="fecha_fin" class="h6">Fecha de Fin:</label>
                <input type="date" name="fecha_fin" class="form-control" required >
              </div>
              <div class="form-group">
                <label for="link" class="h6">Botón Link (Opcional):</label>
                <input autocomplete="off" type="text" name="link" class="form-control" placeholder="Escribir link aquí. Ej : https://www.ejemplo.com" >
                <input autocomplete="off" type="text" name="nombre_link" class="form-control" placeholder="Escribir nombre de botón aquí (Opcional)" >
              </div>
              <div class="form-group mt-1">
                <h6 class="">Imagen (Opcional):</h6>
                <input autocomplete="off" accept="image/png, image/jpeg, image/bmp" type="file" class="form-control-file rounded borderinput" name="imagen" aria-describedby="fileHelp" onchange='openFile(event)'>
                <small class="form-text text-muted">Seleccione una imagen <strong>(formato JPG o PNG)</strong></small>
              </div>
              <input type="submit" name="save_task" class="btn btn-success btn-block" value="Cargar">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Activar PreInscripción</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group">
          <label for="usr">DNI:</label>
          <input type="text" class="form-control" id="usr" value="0">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="activarDNI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
      </div>

    </div>
  </div>
</div>

</div>
<!-- Fin The Modal -->

<!-- The Modal Configuración-->
<div class="modal fade" id="myModalConf">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Habilitar PreInscripción</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

        <hr>
        <div class="form-group">
          <h6 class="">Fecha Inicio:</h6>
          <input id="fecha_inicio" name="fecha_inicio" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
        </div>
        <div class="form-group">
          <h6 class="">Fecha Fin:</h6>
          <input id="fecha_fin" name="fecha_fin" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button id="desactivarPI" type="button" class="btn btn-secondary" data-dismiss="modal">Desactivar</button>
          <button id="activarPI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
        </div>

      </div>
    </div>
  </div>

</div>
<!-- Fin The Modal Configuración -->

  <script>
    $(document).ready(function() {
      $("#myBtn").click(function() {
        $("#myModal3").modal();
      });
    });
  </script>

  <script type="text/javascript">
    function ConfirmDelete() {
      var respuesta = confirm("¿Esta seguro de eliminar el anuncio?")

      if (respuesta == true) {
        return true;
      } else {
        return false;
      }
    }
  </script>

  <!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo URL_PROJECT ?>/js/usuario/enablePreInscription.js"></script>

<!-- Menu Toggle Script -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });


  //  Feather Script
  feather.replace()
</script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


<!-- Inicio SweetAlert2 -->
<script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>

<script src="<?php echo URL_PROJECT ?>/js/dashboard/sweetAlertAll.js"></script>