<?php

$sesion = new UserSession();

// Header HTML
include_once URL_APP . '/views/custom/header_dashboard.php';

// Barra de Navegación
//include_once URL_APP . '/views/custom/navbar_preInsc.php';

// SideBar Dashboard
include_once URL_APP . '/views/custom/navbar_dashboard.php';

?>

<div class="container-fluid mt-0">
  <!-- <h2 class="mt-4 animated fadeIn">Documentos</h2> -->
  <div class="table-responsive">
    <table id="tablaDocumentos" class="table table-hover table-striped text-center">
    <h2 class="mt-4 animated fadeIn">Documentos</h2>
      <thead class="thead-dark mt-4">
        <tr class="table animated fadeIn">
          <th scope="col">#</th>
          <th scope="col">Documentos</th>
          <th scope="col">Tecnicatura</th>
          <th scope="col">Ver</th>
          <th scope="col">Status</th>
          <th scope="col">Acción</th>
        </tr>
      </thead>
      <tbody id="documentos" class="animated fadeIn">

        <!-- DataTable JavaScript -->
        <script src="<?php echo URL_PROJECT ?>/js/dashboard/dashPreInscDocsView.js"></script>

        </body>

        </html>