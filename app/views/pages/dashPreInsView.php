<?php

$sesion = new UserSession();

// Header HTML
include_once URL_APP . '/views/custom/header_dashboard.php';

// Barra de Navegación
//include_once URL_APP . '/views/custom/navbar_preInsc.php';

// SideBar Dashboard
include_once URL_APP . '/views/custom/navbar_dashboard.php';

?>



<!-- <body>-->

<div class="container-fluid">

  <h1 id="prueba" class="mt-4 ml-4 animated fadeIn">Preinscripción</h1>

<div class="table-responsive-sm mt-5">
  <table id="tableBTS4" class="table table-hover tableColor text-center mt-4">
    <thead class="thead-dark">
      <tr class="table animated fadeIn">
        <th scope="col">Apellido</th>
        <th scope="col">Nombre</th>
        <th scope="col">Dirección</th>
        <th scope="col">DNI</th>
        <th scope="col">E-Mail</th>
        <th scope="col">Telefono</th>
        <th scope="col">Status</th>
        <th scope="col">Fecha</th>
        <th scope="col">IdTec</th>
        <th scope="col">Tecnicatura</th>
        <th scope="col">Acciones</th>
      </tr>
    </thead>
    <tbody id="contenido" class=" prueba animated fadeIn">

    </tbody>
  </table>
</div>



</div>

  <!-- <a data-toggle="modal" href="#myModal">  Register</a>

  </button> -->

  <!-- Modal Activar PreInscripción-->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Activar PreInscripción</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label for="usr">DNI:</label>
            <input type="text" class="form-control" id="usr" value="0">
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button id="activarDNI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
        </div>

      </div>
    </div>
  </div>

</div>
<!-- Fin Modal Activar PreInscripción -->


<!-- Modal Delete -->
<div class="modal fade" id="deleteModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Eliminar PreInscripción</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          ¿Estas seguro de querer eliminar el registro?
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <a id="deletePre" href="#" class="btn btn-danger" role="button">Eliminar</a>
        </div>

      </div>
    </div>
  </div>

</div>
<!-- Modal Delete -->


<!-- The Modal Configuración-->
<div class="modal fade" id="myModalConf">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Habilitar PreInscripción</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

        <hr>
        <div class="form-group">
          <h6 class="">Fecha Inicio:</h6>
          <input id="fecha_inicio" name="fecha_inicio" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>">
        </div>
        <div class="form-group">
          <h6 class="">Fecha Fin:</h6>
          <input id="fecha_fin" name="fecha_fin" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>">
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button id="desactivarPI" type="button" class="btn btn-secondary" data-dismiss="modal">Desactivar</button>
          <button id="activarPI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
        </div>

      </div>
    </div>
  </div>

</div>
<!-- Fin The Modal Configuración -->


<!-- DataTable JavaScript -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<!-- DataTable JavaScript -->
<script src="<?php echo URL_PROJECT ?>/js/dashboard/dashPreInscView.js"></script>
<script src="<?php echo URL_PROJECT ?>/js/usuario/enablePreInscription.js"></script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->


<!-- Inicio SweetAlert2 -->
<script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>

<!-- Menu Toggle Script -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

  //  Feather Script
  feather.replace()
</script>



<script src="<?php echo URL_PROJECT ?>/js/dashboard/sweetAlertAll.js"></script>

</body>

</html>