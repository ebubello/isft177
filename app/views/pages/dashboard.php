<?php



$sesion = new UserSession();
$xxx = new dashboardModel();



// Header HTML
include_once URL_APP . '/views/custom/header_dashboard.php';

// Barra de Navegación
//include_once URL_APP . '/views/custom/navbar_preInsc.php';

// SideBar Dashboard
include_once URL_APP . '/views/custom/navbar_dashboard.php';

?>


<div class="container-fluid animated fadeIn">
  <h1 class="mt-4">Dashboard</h1>
  <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active"><strong>NOVEDADES</strong></li>
  </ol>

  <!-- Cards -->
  <div class="card-deck">
    <div class="card">
      <img src="../img/carreras/sistemas3.jpg" class="card-img-top" alt="" height="150">
      <div class="card-body">
        <h5 class="card-title">Tecnicatura en Sistemas.</h5>
        <p class="card-text mt-4">PreInscripciones
          <br><br><span class="badge badge-pill badge-success"><i data-feather="check-circle"></i>
            <?php
            echo ($xxx->getPreInsSuccess(1));
            ?>

          </span>&nbsp;<span class="badge badge-pill badge-warning"><i data-feather="alert-triangle"></i>
            <?php
            echo ($xxx->getPreInsWarning(1));
            ?>
          </span>&nbsp;
          <span class="badge badge-pill badge-danger"><i data-feather="x-circle"></i>
            <?php
            echo ($xxx->getPreInsDanger(1));
            ?>
          </span></p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      </div>
    </div>
    <div class="card">
      <img src="../img/carreras/gestamb1.jpg" class="card-img-top" alt="" height="150">
      <div class="card-body">
        <h5 class="card-title">Tecnicatura en Gestión Ambiental y Salud.</h5>
        <p class="card-text mt-4">PreInscripciones
          <br><br><span class="badge badge-pill badge-success"><i data-feather="check-circle"></i>
            <?php
            echo ($xxx->getPreInsSuccess(4));
            ?>

          </span>&nbsp;<span class="badge badge-pill badge-warning"><i data-feather="alert-triangle"></i>
            <?php
            echo ($xxx->getPreInsWarning(4));
            ?>
          </span>&nbsp;
          <span class="badge badge-pill badge-danger"><i data-feather="x-circle"></i>
            <?php
            echo ($xxx->getPreInsDanger(4));
            ?>
          </span></p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      </div>
    </div>
    <div class="card">
      <img src="../img/carreras/clinicos1.jpg" class="card-img-top" alt="" height="150">
      <div class="card-body">
        <h5 class="card-title ">Tecnicatura en Lab. de Análisis Clínicos.</h5>
        <p class="card-text mt-4">PreInscripciones
          <br><br><span class="badge badge-pill badge-success"><i data-feather="check-circle"></i>
            <?php
            echo ($xxx->getPreInsSuccess(2));
            ?>

          </span>&nbsp;<span class="badge badge-pill badge-warning"><i data-feather="alert-triangle"></i>
            <?php
            echo ($xxx->getPreInsWarning(2));
            ?>
          </span>&nbsp;
          <span class="badge badge-pill badge-danger"><i data-feather="x-circle"></i>
            <?php
            echo ($xxx->getPreInsDanger(2));
            ?>
          </span></p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      </div>
    </div>

    <div class="card">
      <img src="../img/carreras/contable1.jpeg" class="card-img-top" alt="" height="150">
      <div class="card-body">
        <h5 class="card-title ">Tecnicatura en Administración Contable.</h5>
        <p class="card-text mt-4">PreInscripciones
          <br><br><span class="badge badge-pill badge-success"><i data-feather="check-circle"></i>
            <?php
            echo ($xxx->getPreInsSuccess(3));
            ?>

          </span>&nbsp;<span class="badge badge-pill badge-warning"><i data-feather="alert-triangle"></i>
            <?php
            echo ($xxx->getPreInsWarning(3));
            ?>
          </span>&nbsp;
          <span class="badge badge-pill badge-danger"><i data-feather="x-circle"></i>
            <?php
            echo ($xxx->getPreInsDanger(3));
            ?>
          </span></p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      </div>
    </div>
  </div>


  <!-- Cards -->

</div>
</div>
<!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Activar PreInscripción</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group">
          <label for="usr">DNI:</label>
          <input type="text" class="form-control" id="usr" value="0">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="activarDNI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
      </div>

    </div>
  </div>
</div>

</div>
<!-- Fin The Modal -->

<!-- The Modal Configuración-->
<div class="modal fade" id="myModalConf">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Habilitar PreInscripción</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

        <hr>
        <div class="form-group">
          <h6 class="">Fecha Inicio:</h6>
          <input id="fecha_inicio" name="fecha_inicio" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
        </div>
        <div class="form-group">
          <h6 class="">Fecha Fin:</h6>
          <input id="fecha_fin" name="fecha_fin" type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button id="desactivarPI" type="button" class="btn btn-secondary" data-dismiss="modal">Desactivar</button>
          <button id="activarPI" type="button" class="btn btn-primary" data-dismiss="modal">Activar</button>
        </div>

      </div>
    </div>
  </div>

</div>
<!-- Fin The Modal Configuración -->


<!-- Optional JavaScript -->

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


<!-- Inicio SweetAlert2 -->
<script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>


<script src="<?php echo URL_PROJECT ?>/js/dashboard/dashPreInscView.js"></script>
<script src="<?php echo URL_PROJECT ?>/js/usuario/enablePreInscription.js"></script>

<!-- Menu Toggle Script -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });


  //  Feather Script
  feather.replace()
</script>

<script src="<?php echo URL_PROJECT ?>/js/dashboard/sweetAlertAll.js"></script>



</body>

</html>