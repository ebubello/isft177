<?php

$sesion = new UserSession();
//$xxx = new dashboardModel();

include_once URL_APP . '/views/custom/header_dashboard.php';

include_once URL_APP . '/views/custom/navbar_dashboard.php'; //Acá hay que ver de hacer que funcione esta navBar que es la del Dashboard.

?>


<?php

$anuncio = $_SESSION['anuncio'];

if (isset($anuncio)) {
    $id_anuncio = $anuncio[0]->{"Id_Anuncio"};
    $titulo = $anuncio[0]->{"Titulo"};
    $descripcion = $anuncio[0]->{"Descripcion"};
    $prioridad = $anuncio[0]->{"Prioridad"};
    $fecha_inicio = $anuncio[0]->{"Fecha_Inicio"};
    $fecha_fin = $anuncio[0]->{"Fecha_Fin"};
    $fecha_alta = $anuncio[0]->{"Fecha_Alta"};
    $rutaimagen = $anuncio[0]->{"Imagen"};
    $link = $anuncio[0]->{"Link"};
    $nombrelink = $anuncio[0]->{"Nombre_Link"};
}

?>

<div class="container p-4">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card card-body">
                <form action="<?php echo URL_PROJECT ?>/home/editar_anuncio" method="POST"  enctype="multipart/form-data">
                    <div class="form-group">
                        <h6 class="">ID:</h6>
                        <input  name="idanuncio" type="number" class="form-control" value="<?php echo $id_anuncio; ?>" readonly="readonly" >
                    </div>
                    <div class="form-group">
                        <h6 class="">Título:</h6>
                        <input autocomplete="off" name="titulo" type="text" class="form-control" value="<?php echo $titulo; ?>" placeholder="Escribir título aquí" >
                    </div>
                    <div class="form-group">
                        <h6 class="">Descripción:</h6>
                        <textarea autocomplete="off" name="descripcion" rows="5" class="form-control" placeholder="Escribir descripción aquí"><?php echo $descripcion; ?></textarea>
                    </div>
                    <div class="form-group">
                        <h6 class="">Orden:</h6>
                        <input  name="prioridad" type="number" class="form-control" value="<?php echo $prioridad; ?>"  min="1" >
                    </div>
                    <div class="form-group">
                        <h6 class="">Fecha Inicio:</h6>
                        <input name="fecha_inicio" type="date" class="form-control" value="<?php echo $fecha_inicio; ?>" >
                    </div>
                    <div class="form-group">
                        <h6 class="">Fecha Fin:</h6>
                        <input name="fecha_fin" type="date" class="form-control" value="<?php echo $fecha_fin; ?>" >
                    </div>
                    <div class="form-group">
                        <h6 class="">Botón Link (Opcional):</h6>
                        <input autocomplete="off" name="link" type="text" class="form-control" value="<?php echo $link; ?>" placeholder="Escribir link aquí">
                        <input autocomplete="off" type="text" name="nombre_link" class="form-control" value="<?php echo $nombrelink; ?>" placeholder="Escribir nombre de botón aquí (Opcional)" >
                    </div>
                    <div class="form-group mt-1">
                        <h6 class="">Imagen (Opcional):</h6>
                        <input autocomplete="off" name="imagentext" type="text" class="form-control" value="<?php echo $rutaimagen; ?>" placeholder="">
                        <input accept="image/png, image/jpeg, image/bmp" type="file" class="form-control-file rounded borderinput" name="imagen" id="imagen" aria-describedby="imagen" onchange='openFile(event)' >
                        <small id="imagen" class="form-text text-muted">Seleccione una imagen <strong>(formato JPG o PNG)</strong></small>
                    </div>
                    <div class="form-group">
                        <h6 class="">Fecha Alta:</h6>
                        <input name="fecha_alta" type="datetime" class="form-control" value="<?php echo $fecha_alta; ?>" readonly="readonly" >
                    </div>
                    <button class="btn btn-success" name="actualizar">
                        Actualizar
                    </button>
                    <button class="btn btn-danger" name="cancelar">
                        Cancelar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });


  //  Feather Script
  feather.replace()
  </script>