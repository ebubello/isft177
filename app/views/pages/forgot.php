

<?php


include_once URL_APP . '/views/custom/header.php'; 

// Barra de Navegación
include_once URL_APP . '/views/custom/navbar_preInsc.php'; 

?>
<body class="my-login-page">
	<section class="h-70">
		<div class="container h-100">
			<div class="row justify-content-md-center align-items-center h-100">
				<div class="card-wrapper">
					<div class="brand">
						<img src="<?php echo URL_PROJECT?>/img/login/logo.jpg" alt="bootstrap 4 login page">
					</div>
					<div class="card fat">
						<div class="card-body">
							<h4 class="card-title">Olvidó su contraseña</h4>
							<p id="respuesta"></p>
							<form id="formForgot" method="POST" class="my-login-validation" novalidate="" action="#">
								<div class="form-group">
									<label for="email">E-Mail</label>
									<input id="email" type="email" class="form-control" name="email" value="" required autofocus>
									<div class="invalid-feedback">
										Email no es valido
									</div>
									<div class="form-text text-muted">
									Al hacer clic en "Restablecer contraseña" le enviaremos un enlace para restablecer la contraseña
									</div>
								</div>

								<div class="form-group m-0">
									<button id="forgot" type="submit" class="btn btn-primary btn-block">
									Restablecer la contraseña
									</button>
								</div>
							</form>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</section>

	
	<script src="<?php echo URL_PROJECT?>js/login/my-login.js"></script>
	<script src="<?php echo URL_PROJECT ?>/js/usuario/usuarioForgot.js"></script>

	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


<!-- Inicio SweetAlert2 -->
<script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>

<script src="<?php echo URL_PROJECT ?>/js/usuario/sweetAlertAll.js"></script>


</body>
</html>