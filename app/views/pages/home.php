<?php


// Header HTML
include_once URL_APP . '/views/custom/header.php';

//BANNER
include_once URL_APP . '/views/custom/banner.php';

// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';


?>
<!-- Container principal -->
<div class="container colorfondo mt-4">
  <!-- Fila de novedades e imagenes-->
  <div class="row ">

    <!-- Columna de novedades -->

    <div class="col-lg-8 mb-2">

      <!--CRUD Novedades -->
      <h5 class="card-header colorfondo mt-1">NOVEDADES</h5>
      <div class="card mt-2">
        <?php
        $resultado = $_SESSION['Novedadeshome'];
        foreach ($resultado as $resultadoarray) { ?>

          <div class="card-body pb-0">
            <h4 class="card-title letragoogletitulo"><?php echo $resultadoarray->{"Titulo"}; ?></h4>

            <?php $img = $resultadoarray->{"Imagen"};
            if (is_null($img) || $img == "") {
            } else { ?>
              <img src="<?php echo URL_PROJECT . '/img/novedades/' . $resultadoarray->{"Imagen"} ?>" alt="" class="card-img-top"><br>
            <?php } ?>


            <h6 class="card-text font-weight-normal pt-2"><?php echo $resultadoarray->{"Descripcion"}; ?></h6>


            <?php $link = $resultadoarray->{"Link"};
            if (is_null($link) || trim($link) == "") {
            } else { ?>
              <a href="<?php echo $resultadoarray->{"Link"}; ?>" target="_blank" class="btn btn-warning colorfondo2 font-weight-bolder colorletra 
              
              <?php $img = $resultadoarray->{"Imagen"};
              if (is_null($img) || $img == "") {
              } else {
                echo "w-100";
              } ?>
              
              "><?php $mombrelink = $resultadoarray->{"Nombre_Link"};
                if (is_null($mombrelink) || trim($mombrelink) == "") {
                  echo "Ir ";
                } else {
                  echo trim($mombrelink) . " ";
                };
                ?><img src="<?php echo URL_PROJECT ?>/public/img/home/flecha.png"></a>
            <?php } ?>
          </div>
          <hr>
        <?php } ?>
      </div>
      <!--Fin CRUD Novedades -->

    </div>
    <!-- Fin Columna de novedades -->


    <!-- Columna de Anuncios -->

    <div class="col-lg-4 d-flex justify-content-center mb-2">

      <div class="col-lg-12">


        <!-- Card -->

        <h5 class="card-header mt-1 colorfondo">ANUNCIOS</h5>


        <?php
        $anuncios = $_SESSION['anuncioshome'];
        foreach ($anuncios as $resultadoarrayanuncios) { ?>
          <div class="card mt-2">
            <?php $img = $resultadoarrayanuncios->{"Imagen"};
            if (is_null($img) || $img == "") {
            } else { ?>
              <!-- Card image -->
              <img class="card-img-top" src="<?php echo URL_PROJECT . '/img/anuncios/' . $resultadoarrayanuncios->{"Imagen"} ?>" alt="Card image cap">
            <?php } ?>

            <!-- Card content -->
            <div class="card-body">

              <!-- Title -->
              <h4 class="card-title letragoogletitulo"><a><?php echo $resultadoarrayanuncios->{"Titulo"}; ?></a></h4>
              <!-- Text -->
              <p class="card-text"><?php echo $resultadoarrayanuncios->{"Descripcion"}; ?></p>

              <?php $link = $resultadoarrayanuncios->{"Link"};
              if (is_null($link) || trim($link) == "") {
              } else { ?>
                <!-- Button -->
                <a href="<?php echo $resultadoarrayanuncios->{"Link"}; ?>" target="_blank" class="btn btn-primary">
                <?php $mombrelink = $resultadoarrayanuncios->{"Nombre_Link"};
                if (is_null($mombrelink) || trim($mombrelink) == "") {
                  echo "Ir ";
                } else {
                  echo trim($mombrelink) . " ";
                };
                ?></a>
              <?php } ?>

            </div>

          </div>
        <?php } ?>
        <!-- Card -->



      </div>

    </div>

    <!--Fin Columna de imagenes -->
  </div>
  <!-- Fin Fila de novedades e imagenes-->
</div>
<!--Fin Container principal -->

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>
