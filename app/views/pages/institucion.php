<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<!-- <h1 class=" text-center navbar-dark colorfondo2 p-1">Instituto de Formación Técnica Superior N 177</h1> -->

<!-- Nav tabs INSTITUCIóN -->
<section class="container coloryellow">
  <div class="row">
    <div class="col-lg-3 text-white bg-dark">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <h4 class="text-dark text-center p-3 mt-3 colorfondo2 letragoogletitulo">INSTITUCIÓN</h4>
        <a class="nav-link text-white bg-dark active letragoogletitulo" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Historia</a>
        <!-- 
        <a class="nav-link text-white bg-dark disabled" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Cuerpo Directivo</a> -->
        <a class="nav-link text-white bg-dark disabled letragoogletitulo" id="v-pills-messages1-tab" data-toggle="pill" href="#v-pills-messages1" role="tab" aria-controls="v-pills-messages1" aria-selected="false">Cuerpo Docente</a>
        <!--  <a class="nav-link text-white bg-dark disabled" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Ideario Institucional</a> -->
        <a class="nav-link text-white bg-dark disabled letragoogletitulo" id="v-pills-message4s-tab" data-toggle="pill" href="#v-pills-messages4" role="tab" aria-controls="v-pills-messages4" aria-selected="false">Investigación y Desarrollo</a>
        <a class="nav-link text-white bg-dark disabled letragoogletitulo" id="v-pills-messages5-tab" data-toggle="pill" href="#v-pills-messages5" role="tab" aria-controls="v-pills-messages5" aria-selected="false">Calendario Institucional</a>
        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Relación con otras Instituciones</a>
        <!--   <a class="nav-link text-white bg-dark disabled" id="v-pills-messages2-tab" data-toggle="pill" href="#v-pills-messages2" role="tab" aria-controls="v-pills-messages2" aria-selected="false">Galería de Fotos</a> -->
      </div>
    </div>
    <div class="d-flex col-lg-9">
      <div class="tab-content" id="v-pills-tabContent">
        <div class="mt-1 tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
          <!--   
          <h1 class="text-center">
            <div class="bg-warning">Un poco de historia...</div>
            
          </h1> -->
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Nuestros Comienzos
          </h2>
          <hr class="my-3">
          <p class="text-justify">
            El Instituto Superior de Formación Técnica N° 177 se encuentra ubicado en la localidad de Libertad, Partido de Merlo, Provincia de Buenos Aires, y funciona ininterrumpidamente desde el año 1992, año en el que fue creado a partir de un convenio con el Ministerio de Cultura y Educación entre la Dirección de Educación de Adultos y el Obispado de Morón a través de la Parroquia San José iniciando sus actividades con la carrera de Técnico Superior en Administración de Empresas, carrera que, aún hoy, se mantiene.
          </p>
          <p class="text-justify">
            En el año 1993, se abren otras dos carreras, las de Técnico Superior en Análisis de Sistemas y de Técnico Superior en Análisis Clínicos, ésta última se mantiene actualmente como Tecnólogo en Salud con Orientación en Análisis Clínicos.
          </p>
          <p class="text-justify">
            A partir del año 1994, con el inicio del ciclo lectivo, el Instituto pasó a depender de la Dirección General de Cultura y Educación, manteniendo las tres carreras, así hasta el año 2001, año en el cual, atendiendo a las demandas de la sociedad, se abre la carrera de Técnico Superior en Recreación, la que se cierra en el año 2004.
          </p>
          <p class="text-justify">
            En el año 2005 se abre la carrera de Técnico Superior en Comunicación Social con orientación en Planificación Institucional y Comunitaria.
          </p>
          <p class="text-justify">
            En cuanto a la Planta Orgánico Funcional, el Instituto cuenta con un Director, un Secretario, una Bibliotecaria, dos bedeles y aproximadamente 50 docentes para atender una matrícula de más de 200 alumnos. En cuanto al personal, 17 son docentes y el resto son profesionales, contando con 8 especialistas en computación o Sistemas, 3 ingenieros, 2 abogados, 13 licenciados en distintas especialidades, un Magister en Metodología de la Investigación, 6 bioquímicos, 4 contadores públicos; muchos de los cuales han realizado el ciclo de Capacitación Docente, conjugando una serie de factores que prestigian al servicio educativo.
          </p>
          <p class="text-justify">
            En relación a la población estudiantil, el Instituto recibe alumnos, no sólo de Libertad y zonas aledañas, sino también de localidades y partidos vecinos, como Merlo, Ituzaingó, Castelar, Morón, Rafael Castillo, Moreno, Mariano Acosta, Marcos Paz entre otras. Esto se debe a que el Instituto es el único Técnico de gestión estatal en el Distrito y a que las carreras que poseemos les otorgan amplias perspectivas laborales con sólidas posibilidades de inserción en el sector socio-productivo.
          </p>
          <p class="text-justify">
            El Instituto, por ser conveniado, funciona en las instalaciones de la EP "Instituto José Manuel Estrada" perteneciente a la Parroquia San José y constituye parte de una amplia red de instituciones que atienden las necesidades educativas para adolescentes y adultos.
          </p>
          <p class="text-justify">
            Actualmente estamos trabajando en red con el I.S.F.D. N° 29 de Merlo, con el cual se están implementando distintos proyectos. En el marco de las prácticas de laboratorio y pasantías de la carrera de Análisis Clínicos, se han establecido convenios con distintos hospitales y clínicas de la zona.
          </p>
          <p class="text-justify">
            Además se establecieron convenios de colaboración recíproca entre escuelas e instituciones, donde cabe destacar los establecidos con la Escuela de Educación Media N° 11 (actualmente Escuela de Educación Técnica N°5) y el Centro de Formación Profesional N° 401.
          </p>
          <p class="text-justify">
            Consideramos, como Institución de formación Superior, que la educación es el único medio de brindar a nuestros alumnos las posibilidades de acceder a una mejor calidad de vida, de poder insertarse con éxito en el mercado laboral. Vemos fundamentado lo antes dicho por los egresados que hoy se encuentran trabajando en hospitales, clínicas, empresas, escuelas, pymes etc. de la zona, y son éstos egresados los que nos impulsan a seguir trabajando para ofrecer año a año una mejor y más amplia oferta educativa.
          </p>
        </div>

        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
          ...
        </div>

        <div class="mt-1 tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Relación con otras instituciones
          </h2>
          <hr class="my-3">
          <p class="font-weight-bold">Jornada en el INTA, con la participación de alumnos y docentes de la carrera de Gestión Ambiental y Salud</p>

          <p class="text-justify">El pasado Jueves 12 de Mayo de 2011, en las instalaciones del INTA (Instituto Nacional de Tecnología Agropecuaria, se realizó una Jornada/Taller sobre los temas:</p>
          <p>-Primeros Auxilios y RCP (Reanimación Cardio-Pulmonar) - Medicina Laboral</p>
          <p>-Procedimiento de Emergencia (Evacuación) Seguridad e Higiene</p>
          <p class="text-justify">Dicha Jornada estuvo a cargo del Lic. Gustavo Maurelis, con la participación de la Dra. Claudia Bolognesi como disertante, y la asistencia activa de alumnos y docentes de la Carrera de Técnico Superior en Gestión Ambiental y Salud de nuestro Instituto.</p>
          <p>Estas actividades permiten estrechar lazos con importantes instituciones como el INTA, además de elevar el nivel profesional de nuestros futuros egresados.</p>
          <p>Realiza clic en los siguientes links para ver las imágenes de la Jornada</p>

        </div>

        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
      </div>
    </div>
  </div>
</section>

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>