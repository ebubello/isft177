<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Estilos Bootstrap Local -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/bootstrap/bootstrap.min.css">

    <!-- JS Bootstrap Local -->
    <script src="<?php echo URL_PROJECT ?>/js/jquery/jquery-3.5.1.min.js"></script>
    <script src="<?php echo URL_PROJECT ?>/js/popper/popper1.16.0.min.js"></script>
    <script src="<?php echo URL_PROJECT ?>/js/bootstrap/bootstrap.min.js"></script>

    <!-- GoogleFonts -->
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">

    <!-- Css -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_home.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/style_inscripcion.css">
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/login/my-login.css">

     <!-- SweetAlert2 -->
     <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/sweetAlert2/sweetalert2.min.css">
        <!-- Animated -->
    <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/css/sweetAlert2/animate.min.css">

  
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5f483c51cc6a6a5947af721e/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
          </script>
        <!--End of Tawk.to Script-->

    <!-- Icono Pesataña -->
    <link rel="icon" href="<?php echo URL_PROJECT ?>/img/home/logoisft177.png">

    <!-- Titulo -->
    <title><?php echo NAME_PROJECT ?></title>

</head>

<body>



    <?php

    // Barra de Navegación
    include_once URL_APP . '/views/custom/navbar_preInsc.php';
    $p = new preInscripcionModel();

    ?>




    <div class="container mt-3 ">
        <div id="respuesta" class="mt-2">
        </div>

        <div class="row justify-content-md-center ">
            <div class="col-12">
                <h3 id="intro"></h3>
                <hr>

                <div class="card">
                    <h5 class="card-header h5">Documentación Solicitada</h5>
                    <div class="card-body">
                        <h5 class="card-title">Para todas las carreras en general:</h5>
                        <p>
                            Para llevar adelante el proceso de pre inscripción es necesario contar con una cuenta de correo electrónico y
                            con la siguiente documentación digitalizada:<br>

                            <ul>

                            </ul>

                            <strong> 1)</strong> PRE-INSCRIPCIÓN ONLINE en la web <a href="http://www.abc.gov.ar/">www.abc.gov.ar</a> .<br>
                            <strong> 2)</strong> Fotocopia y original de DNI.<br>
                            <strong> 3)</strong> Fotocopia y original de título del nivel medio o constancia de título en trámite, esto último en Original. <br>
                            <strong> 4)</strong> Cuatro fotos 4x4 color, iguales y recientes <strong>(Formato JPG o PNG)</strong>.<br>
                            <strong> 5)</strong> Fotocopia y original del Acta de Nacimiento.<br>
                            <strong> 6)</strong> Libreta sanitaria expedida por organismo público (excepto para la carrera de Laboratorio).<br>
                            <strong> 7)</strong> Ficha de inscripción completa y Reglamento firmado.<br><br>

                            <strong> Para la carrera de Análisis Clínicos</strong> en particular, además de lo anterior:<br><br>

                            <strong> 8)</strong> Certificado de Aptitud Psicofísica expedido por una institución de salud estatal (en reemplazo de la Libreta Sanitaria).<br>
                            <strong> 9)</strong> Constancia de aplicación de la Vacuna contra la Hepatitis B.

                        </p>

                        <!-- <p><strong>* Para mayor detalle, te invitamos a ver el video tutorial de pre inscripción online.</strong></p> -->
                        <p><strong>* CONSIDERACIONES: <i>Es necesario que toda la información digitalizada se cargue en formato PDF durante la pre-inscripción.</i></strong></p>

                        <p><strong>¿Tenes dudas de como digitalizar tus documentos?</strong>  Te invitamos a leer el siguiente articulo:<a href="https://www.xatakandroid.com/ofimatica/6-aplicaciones-para-escanear-documentos-con-tu-movil" target="_blank"> Leer</a>  </p>

                    </div>
                </div>



                <div class="card-deck mt-3 ">
                    <div class="card border-dark mb-3 ml-5" style="max-width: 20rem;">
                        <div class="card-header">Pre inscripción</div>
                        <div class="card-body text-dark">
                            <h5 class="card-title">Se compone de 4 pasos:</h5>
                            <p class="card-text">
                                1) Verificacion de Identidad. <br>
                                2) Datos de la tecnicatura.<br>
                                3) Datos Personales.<br>
                                4) Documentación Digitalizada.<br>
                            </p>
                        </div>
                    </div>

                    <div class="card border-dark mb-3" style="max-width: 20rem;">
                        <div class="card-header">Post Pre inscripción</div>
                        <div class="card-body text-dark">
                            <h5 class="card-title">Detalle:</h5>
                            <p class="card-text">
                                1) Recibiras un email indicando el status del tramite.<br>
                                2) Validadremos la documentación enviada.<br>
                                3) Nos pondremos en contacto<br>

                            </p>
                        </div>
                    </div>

                    <div class="card border-dark mb-3" style="max-width: 20rem;">
                        <div class="card-header">PRE INSCRIPCIÓN - PASO 1</div>
                        <div class="card-body text-dark">
                            <h5 class="card-title">Verificacion de Identidad</h5>
                            <p class="card-text">
                                <form class="form-inline mt-4 mb-5" id="formulario">
                                    <div class="form-group mb-2">

                                        <label for="">DNI</label>

                                    </div>
                                    <div class="form-group mx-sm-4 mb-2">
                                        <label for="inputDNI" class="sr-only">DNI</label>
                                        <input  type="number" min="11111111" max="99999999" oninvalid="this.setCustomValidity('El DNI ingresado debe tener 8 digitos!!!')" oninput="this.setCustomValidity('')"  class="form-control" id="dni" name="dni" placeholder="Ingrese DNI" style = "width:200px;" required >
                                    </div>
                                    <button type="submit" id='btnFetch' class="btn btn-primary btn-block mb-2 mt-3">Confirmar Identidad</button>
                                </form>
                            </p>
                        </div>
                    </div>


                </div>


            </div>

            <!-- <h5 class="mt-5 ml-5 ">VIDEO TUTORIAL DE PREINSCRIPCIÓN</h5>

            <div class="embed-responsive embed-responsive-16by9 mb-5">
                <iframe class="embed-responsive-item" width="60" height="15" src="https://www.youtube.com/embed/JaG58SlQirw" allowfullscreen></iframe>
            </div> -->


        </div>
    </div>



    <!-- Modal Preguntar -->
    <div class="modal show animated fadeIn" id="Modalpreguntar">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header" style="background-color:#5499C7;">
                    <h5 class="modal-title" style="color:white;"><strong>Aviso!! </strong>Usted ya cuenta con una preInscripción.</h5>


                </div>
                <div class="">
                    <p style="text-align: center;">
                        <h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;¿Desea inscribirse en otra tecnicatura?</h5>
                    </p>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button onclick="window.location.href='../preInscripcion/Intro';" type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button id="si" type="button" class="btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#ModalMasPreInsc">Si&nbsp;</button>
                </div>

            </div>
        </div>
    </div>

    </div>
    <!-- Fin Modal Preguntar -->

    <!-- Form Modal + Inscripciones -->


    <div class="modal show animated fadeIn" id="ModalMasPreInsc">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header" style="background-color:#5499C7;">
                    <h4 class="modal-title" style="color:white;">Nueva PreInscripción</h4>
                </div>

                <form id="formu2" name="form2" validate action="<?php echo URL_PROJECT ?>/preInscripcion/SavePreInscriptionShort" role="form" method="POST" enctype="multipart/form-data">
                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="sel1"><strong>Selecione Tecnicatura:</strong></label>
                            <select class="form-control" id="sel1" name="sel1">
                                <option>Seleccione...</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="direccion">DNI</label>
                            <input type="number" class="form-control " name="dni2" id="dni2" value="" maxlength="8" readonly>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="exampleInputFile"><strong>Constancia inscripción ABC </strong></label>
                            <input accept="application/pdf" required type="file" class="form-control-file" name="InputFileABC" id="InputFile-ABC" aria-describedby="fileHelp">
                            <small id="fileHelp" class="form-text text-muted">Seleccione la constancia obtenida en la pre inscripcion del sitio ABC.</small>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="exampleInputFile"><strong>Formulario de inscripción</strong></label>
                            <input accept="application/pdf" required type="file" class="form-control-file" name="InputFileinscripcion" id="InputFile-inscripcion" aria-describedby="fileHelp">
                            <small id="fileHelp" class="form-text text-muted">Seleccione archivo en formato pdf</small>
                        </div>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button onclick="window.location.href='../preInscripcion/Intro';" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button id="xx" type="submit" value="submit" class="btn btn-success">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    </div>
    <!-- Fin Modal + Inscripciones -->


    <!-- JavaScript -->
    <script src="<?php echo URL_PROJECT ?>/js/intro/ValidarDni.js"></script>
    <!-- JavaScript -->
    <script src="<?php echo URL_PROJECT ?>/js/intro/intro.js"></script>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->


    <!-- Inicio SweetAlert2 -->
    <script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>

    
<script src="<?php echo URL_PROJECT ?>/js/intro/sweetAlertIntro.js"></script>


</body>

</html>