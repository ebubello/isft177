<?php

$p = new preInscripcionModel();

include_once URL_APP . '/views/custom/header.php';

// Barra de Navegación
include_once URL_APP . '/views/custom/navbar_preInsc.php';

?>


<div class="container container-fluid bodypreinscripcion">
    <div class="container pt-3 mt-4 animated fast ">

        <!-- Alerta de Error -->
        <div class="alert alert-danger alert-dismissible fade show  mt-5" rol="alert" id='error'></div>
        <!-- Alerta de Error -->


        <div class="container my-5 contenido2 animated fast fadeIn" style="width: 90%;">
            <h3 id="titulo"></h3>
            <hr>
            <div class="progress mt-3">
                <div class="progress-bar progress-bar-striped  active " role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
            </div>



            <form style="background-color:white;" class=" mt-4  p-3 " id="regiration_form" name="regiration_form" validate action="<?php echo URL_PROJECT ?>/preInscripcion/SavePreInscription" method="POST" enctype="multipart/form-data">

                <fieldset class="animated fadeIn">

                    <div class="container ">

                        <img src="<?php echo URL_PROJECT ?>/img/preInscripcionIMG/silueta.jpg" id="imgSalida" width="12%" height="12%" src="" class="rounded float-left" />
                        <br>

                        <div class="form-group float-right mt-1">
                            <label for="exampleInputFile"><strong> Foto 4 x 4</strong></label>
                            <input accept="image/png,image/jpeg" type="file" class="form-control-file" id="InputFile-foto" name="InputFilefoto" aria-describedby="fileHelp" onchange='openFile(event)' required >
                            <small id="fileHelp" class="form-text text-muted">Seleccione una imagen <strong>(formato JPG o PNG)</strong> para su legajo</small>
                        </div>
                        <br> <br><br>
                    </div>



                    <h2 class="mt-5">Paso 2: Datos de la Tecnicatura</h2>

                    <div class="form-group mt-2">
                        <label for="tecnicatura">Tecnicatura</label>
                        <select class="form-control " id="tecnicatura" name="tecnicatura" required >
                            <option>Seleccione...</option>
                            <?php foreach ($p->technicalCareerList() as  $value) { 
                                echo '
                                <option value="'.$value['id'].'">'.$value['description'].'</option>
                                ';
                            }
                            ?>

                        </select>
                    </div>


                    <div class="form-group">
                       
                        <p><strong>Turno: </strong> Noche ( Lunes a Viernes de 18:00 a 22:00 Hs. )</p>

                    </div>

                    <input type="button" name="next1" id="next1" class="next btn btn-primary mb-2" value="Siguiente"/> 
                    <!-- onclick=" comprobarFormulario() -->


                </fieldset>

                <fieldset class="animated fadeIn">
                    <h2> Paso 3: Datos Personales</h2>
                    <div class="form-group">
                        <label for="apellido">Apellido(s)</label>
                        <input type="text" class="form-control " name="apellido" id="apellido" placeholder="Ingrese Apellido(s)" required minlength="3" maxlength="24" >
                    </div>

                    <div class="form-group">
                        <label for="nombre">Nombre(s)</label>
                        <input type="text" class="form-control " name="nombre" id="nombre" placeholder="Ingrese Nombre(s)" required >
                    </div>

                    <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input type="text" class="form-control " name="direccion" id="direccion" placeholder="Ingrese su dirección" required >
                    </div>

                    <div class="form-group">
                        <label for="direccion">DNI</label>
                        <input type="number" class="form-control " name="dni" id="dni" value ="" placeholder="Ingrese su DNI" maxlength="8" readonly>
                    </div>


                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control " name="email" id="email" placeholder="Ingrese su email" required >
                    </div>

                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="tel" pattern="[0-9]{11}" class="form-control " name="telefono" id="telefono" placeholder="Ingrese su numero de telefono" required >
                    </div>


                    <input type="button" name="previous" class="previous btn btn-outline-secondary" value="Anterior" />
                    <input id="next2" type="button" name="next" class="next btn btn-primary" value="Siguiente"  />
                </fieldset>

                <fieldset class="animated fadeIn">
                    <h2>Paso 4: Documentación Digitalizada</h2>
                    <div class="form-group  mt-1">
                        <label for="exampleInputFile"><strong>Constancia pre inscripcion ABC </strong></label>
                        <input accept="application/pdf" required type="file" class="form-control-file" name="InputFileABC" id="InputFile-ABC" aria-describedby="fileHelp" >
                        <small id="fileHelp" class="form-text text-muted">Seleccione la constancia obtenida en la pre inscripcion del sitio ABC.</small>
                    </div>

                    <div class="form-group  mt-1">
                        <label for="exampleInputFile"><strong> Copia del DNI</strong></label>
                        <input accept="application/pdf" required type="file" class="form-control-file" name="InputFileDNI" id="InputFile-DNI" aria-describedby="fileHelp" >
                        <small id="fileHelp" class="form-text text-muted">Seleccione archivo en formato pdf</small>
                    </div>

                    <div class="form-group  mt-1">
                        <label for="exampleInputFile"><strong> Copia libreta sanitaria</strong> <i style="color: red;">(*Opcional)</i></label>
                        <input accept="application/pdf"  type="file" class="form-control-file" name="InputFileLS" id="InputFile-LS" aria-describedby="fileHelp" >
                        <small id="fileHelp" class="form-text text-muted">Seleccione archivo en formato pdf</small>
                    </div>

                    <div class="form-group  mt-1">
                        <label for="exampleInputFile"><strong> Copia partida de nacimiento</strong></label>
                        <input accept="application/pdf" required type="file" class="form-control-file" name="InputFilePNac" id="InputFile-PNac" aria-describedby="fileHelp" >
                        <small id="fileHelp" class="form-text text-muted">Seleccione archivo en formato pdf</small>
                    </div>

                    <input type="button" name="previous" class="previous btn btn-outline-secondary" value="Anterior" />
                    <input id="next3" type="button" name="next3" class="next btn btn-primary" value="Siguiente" />
                </fieldset>
                <fieldset class="animated fadeIn">
                    <h2>Paso 4: Documentación Digitalizada</h2>
                    <div class="form-group  mt-1">
                        <label for="exampleInputFile"><strong>Constancia titulo secundario completo o en tramite</strong></label>
                        <input accept="application/pdf" required type="file" class="form-control-file" name="InputFileTitulo" id="InputFile-Titulo" aria-describedby="fileHelp" >
                        <small id="fileHelp" class="form-text text-muted">Seleccione archivo en formato pdf.</small>
                    </div>

                    <div class="form-group  mt-1">
                        <label for="exampleInputFile"><strong>Copia ficha de inscripcion</strong></label>
                        <input accept="application/pdf" required type="file" class="form-control-file" name="InputFileinscripcion" id="InputFile-inscripcion" aria-describedby="fileHelp" >
                        <small id="fileHelp" class="form-text text-muted">Seleccione archivo en formato pdf</small>
                    </div>

                    <div class="form-group  mt-1">
                        <label for="exampleInputFile"><strong>Copia del reglamento del establecimiento</strong></label>
                        <input accept="application/pdf" required type="file" class="form-control-file" name="InputFileReglamento" id="InputFile-Reglamento" aria-describedby="fileHelp" >
                        <small id="fileHelp" class="form-text text-muted">Seleccione archivo en formato pdf</small>
                    </div>

                    <input type="button" name="previous" class="previous btn btn-outline-secondary" value="Anterior" />
                    
                    <button type="submit" name="enviarForm" id='enviarForm' class="btn btn-success" >Enviar</button>
                </fieldset>

                <script src="<?php echo URL_PROJECT ?>/js/preInscripcionJS/enviar.js"></script>
                <script src="<?php echo URL_PROJECT ?>/js/preInscripcionJS/Validar.js"></script>
            </form>
        </div>
        <!-- <br><br><br> -->
    </div>


</div>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="vendor/jquery/jquery.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


    <!-- Inicio SweetAlert2 -->
    <script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>

    
<script src="<?php echo URL_PROJECT ?>/js/preInscripcionJS/sweetAlertInscripcion.js"></script>


</script>


</body>

</html>