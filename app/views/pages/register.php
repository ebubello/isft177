<?php


include_once URL_APP . '/views/custom/header.php';

// Barra de Navegación
include_once URL_APP . '/views/custom/navbar_preInsc.php';

?>

<body class="my-login-page">
	<section class="h-100">
		<div class="container h-100">
			<div class="row justify-content-md-center h-100">
				<div class="card-wrapper mt-4">
					<!-- <div class="brand">
						<img src="<?php echo URL_PROJECT ?>/img/login/logo.jpg" alt="bootstrap 4 login page">
					</div> -->

					   <!-- Alertas de Error -->
					   <div class="alert alert-danger alert-dismissible fade show  mt-1" rol="alert" id='error'></div>
					   <p id="respuesta"></p>
						<!-- Alertas de Error -->
						
					<div class="card fat">
						<div class="card-body">
							<h4 class="card-title">Registro</h4>
							<form id="formRegister" method="POST" class="my-login-validation" novalidate="" action="#">
								<div class="form-group">
									<label for="username">Nombre de usuario:</label>
									<input id="username" type="text" class="form-control" name="username" required minlength="3" maxlength="24" pattern="[A-Za-z]{3,}">
									<div class="invalid-feedback">
										¿Cual es su nombre?
									</div>
								</div>


								<div class="form-group">
									<label for="name">DNI</label>
									<input id="dni" type="text" class="form-control" name="dni" required minlength="8" maxlength="8">
									<div class="invalid-feedback">
										¿Cual es su DNI?
									</div>
								</div>

								<div class="form-group">
									<label for="email">E-Mail</label>
									<input id="email" type="email" class="form-control" name="email" required>
									<div class="invalid-feedback">
										Su E-Mail no es valido
									</div>
								</div>

								<div class="form-group">
									<label for="password">Contraseña</label>
									<input id="password" type="password" class="form-control" name="password" required data-eye>
									<div class="invalid-feedback">
										Password es requerido
									</div>
								</div>

								<div class="form-group">
									<label for="password2"> Repetir Contraseña</label>
									<input id="password2" type="password" class="form-control" name="password2" required data-eye>
									<div class="invalid-feedback">
										Password es requerido
									</div>
								</div>

								<div class="form-group">
									<label for="perfil">Perfil</label>
									<select class="form-control " id="perfil" name="perfil" required>
										<option>Seleccione...</option>
										<option value="1">Administrador</option>
										<option value="2">Preceptor</option>
										<option value="3">Alumno</option>
									</select>
								</div>

								

								<div class="form-group m-0">
									<button id="register" type="submit" class="btn btn-primary btn-block" id="enviar">
										Registrar
									</button>
								</div>
								<div class="mt-4 text-center">
									¿Ya tienes una cuenta? <a href="<?php echo URL_PROJECT ?>/Dashboard/dashboard">Volver a Dashboard</a>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

	 <!-- JavaScript -->
	<script src="<?php echo URL_PROJECT ?>js/login/my-login.js"></script>
	<script src="<?php echo URL_PROJECT ?>/js/usuario/registerForm.js"></script>
	<script src="<?php echo URL_PROJECT ?>/js/usuario/registerData.js"></script>

		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


<!-- Inicio SweetAlert2 -->
<script defer src="<?php echo URL_PROJECT ?>/js/sweetAlert2/sweetalert2.min.js"></script>




</body>

</html>