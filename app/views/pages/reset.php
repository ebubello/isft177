
<?php


include_once URL_APP . '/views/custom/header.php'; 



?>
<body class="my-login-page">
	<section class="h-100">
		<div class="container h-100">
			<div class="row justify-content-md-center h-100">
				<div class="card-wrapper">
					<div class="brand">
						<img src="<?php echo URL_PROJECT?>/img/login/logo.jpg" alt="bootstrap 4 login page">
					</div>
					<div class="card fat">
						<div class="card-body">
							<h4 class="card-title">Restablecer Contraseña</h4>
							<p id="respuesta"></p>
							<form id="formReset" method="POST" class="my-login-validation" novalidate="">
								<div class="form-group">
									<label for="new-password">Nueva Contraseña</label>
									<input id="newPass" type="password" class="form-control" name="newPass" required autofocus data-eye>
									<div class="invalid-feedback">
										Password is required
									</div>
									<label class="mt-2" for="new-password">Repita Contraseña</label>
									<input id="newPass2" type="password" class="form-control" name="newPass2" required autofocus data-eye>
									<div class="invalid-feedback">
										Password is required
									</div>
									<div class="form-text text-muted">
										Asegúrese de que su contraseña sea segura y fácil de recordar
									</div>
								</div>

								<div class="form-group m-0">
									<button type="submit" class="btn btn-primary btn-block">
									     Restablecer Contraseña
									</button>
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>

	
	<!-- <script src="<?php echo URL_PROJECT?>js/login/my-login.js""></script> -->
	<script src="<?php echo URL_PROJECT ?>/js/usuario/usuarioReset.js"></script>
</body>
</html>