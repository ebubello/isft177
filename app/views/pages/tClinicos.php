<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<!-- <h1 class=" text-center navbar-dark colorfondo2 p-1">Instituto de Formación Técnica Superior N 177</h1> -->

<!-- Nav tabs INSTITUCIóN -->
<div class="container imagenClinicos d-flex justify-content-end mt-0">
  <a href=""><img src="<?php echo URL_PROJECT ?>/img/home/logoisft177.png" width="100" height="100" class="d-inline-block align-top rounded" alt="Logo ISFT 177" title="Instituto de Formación Tecnica Superior N°177"></a>
</div>
<section class="container coloryellow">
  <div class="row">
    <div class="col-lg-3 text-white bg-dark">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <h4 class="text-dark text-center p-3 mt-3 colorfondo2 letragoogletitulo">CARRERAS</h4>

        <a class="nav-link text-white bg-dark active letragoogletitulo" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Perfil Profesional</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-salida-tab" data-toggle="pill" href="#v-pills-salida" role="tab" aria-controls="v-pills-salida" aria-selected="false">Salida Laboral</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-materias-tab" data-toggle="pill" href="#v-pills-materias" role="tab" aria-controls="v-pills-materias" aria-selected="false">Materias y Programas</a>

        <a class="nav-link text-white bg-dark disabled letragoogletitulo" id="v-pills-pasantias-tab" data-toggle="pill" href="#v-pills-pasantias" role="tab" aria-controls="v-pills-pasantias" aria-selected="false">Pasantías Laborales</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-horarios-tab" data-toggle="pill" href="#v-pills-horarios" role="tab" aria-controls="v-pills-horarios" aria-selected="false">Horarios</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-correlatividades-tab" data-toggle="pill" href="#v-pills-correlatividades" role="tab" aria-controls="v-pills-correlatividades" aria-selected="false">Correlatividades</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-matriculaBsAs-tab" data-toggle="pill" href="#v-pills-matriculaBsAs" role="tab" aria-controls="v-pills-matriculaBsAs" aria-selected="false">Matrícula Bs.As</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-matriculaNac-tab" data-toggle="pill" href="#v-pills-matriculaNac" role="tab" aria-controls="v-pills-matriculaNac" aria-selected="false">Matrícula Nación</a>

      </div>
    </div>
    <div class="d-flex col-lg-9">
      <div class="tab-content" id="v-pills-tabContent">
        <div class="mt-1 tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Laboratorio de Análisis Clínicos
          </h2>
          <!-- Perfil profesional -->
          <hr class="my-2">
          <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Perfil Profesional</h5>

          <p class="text-justify">
            Profesional no universitario de la Salud que, desde una formación centrada en el proceso tecnológico, participa en los procesos de gestión y atención de la salud, realizando los procesos técnicos específicos de su especialidad contribuyendo, de este modo, a la promoción de la salud, a la prevención
            de enfermedades, al diagnóstico, tratamiento, recuperación y rehabilitación de la persona, familia y comunidad.
            A través del trabajo interdisciplinario en el equipo de salud, realiza actividades intra, extra e interinstitucionales
            relacionadas con su práctica específica y con el desarrollo de su profesión y participa en investigación y educación permanente en salud.
            Como Tecnólogo en Salud su práctica profesional está caracterizada por una actitud reflexiva, crítica, ética y humanística basada
            en una concepción integral del hombre propendiendo a mejorar la calidad de vida de la población.
            Este perfil de formación, de carácter general, se complementa con cada uno de los perfiles enunciados en cada una de
            las carreras de Tecnología en Salud, según las distintas especialidades.
          </p>
        </div>
        <!-- fin perfil profesional -->

        <!-- Salida laboral -->

        <div class="mt-1 tab-pane fade" id="v-pills-salida" role="tabpanel" aria-labelledby="v-pills-salida-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Laboratorio de Análisis Clínicos
          </h2>
          <hr class="my-2">
          <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Salida Laboral</h5>
          <p class="ml-3">

            <p class="text-justify">El Tecnólogo en Salud con especialidad en Laboratorio de Análisis Clínicos desarrollará su ejercicio como profesional
              no universitario en los siguientes ámbitos según el marco legal vigente.</p>
            <ul class="ml-3">
              <li> Sistema de Salud–Subsector Público, Privado, y Obras Sociales en todos los niveles de atención y Programas Sanitarios.</li>

              <li> Educativo de Gestión Pública y Privada.</li>

              <li> Organizaciones No Gubernamentales (O.N.G.) y Gubernamentales.</li>

              <li> Establecimientos Industriales.</li>

              <li> Empresas.</li>

              <li> Otras Organizaciones.</li>
            </ul>

          </p>
        </div>
        <!-- fin salida laboral -->

        <!-- Materias -->
        <div class="mt-1 tab-pane fade" id="v-pills-materias" role="tabpanel" aria-labelledby="v-pills-materias-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Laboratorio de Análisis Clínicos
          </h2>
          <hr class="my-2">
          <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Materias y Programas</h5>
          <p>
            Para ver listado de materias por año con su respectiva carga horaria haga <a href="<?php echo URL_PROJECT ?>/documents/Materias/clinicosMaterias.pdf" target="_blank">Click aquí </a>
          </p>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5">Programas de Estudio por Materia</h5><br>

          <div class="ml-3">
            <h6 class="font-weight-bold">Primer Año</h6>
            <ul>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-1año/BioHumana-1AC.pdf" target="_blank">Biología Humana</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-1año/FisicoQui-1AC.pdf" target="_blank">Fisico Química</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-1año/Ingles-1AC.pdf" target="_blank">Inglés</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-1año/Mate-1AC.pdf" target="_blank">Matemática</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-1año/PracticasProf1-1AC.pdf" target="_blank">Prácticas Profesionalizantes I</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-1año/ProcesosTecEnSalud-1AC.pdf" target="_blank">Procesos Tecnológicos en Salúd</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-1año/SaludPubli-1AC.pdf" target="_blank">Salúd Pública</a></li>


              <hr class="my-4">
              <h6 class="font-weight-bold">Segundo Año</h6>

              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-2año/Comu-2AC.pdf" target="_blank">Comunicación</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-2año/MetoInv-2AC.pdf" target="_blank">Metodología de la Investigación</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-2año/OrgYgestion-2AC.pdf" target="_blank">Organización y Gestión de los Servicios de Salud</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-2año/PracticasProf2-2AC.pdf" target="_blank">Prácticas profesionalizantes 2</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-2año/ProcesosDeLab-2AC.pdf" target="_blank">Procesos de Laboratorio 1</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-2año/QuimicaBio-2AC.pdf" target="_blank">Química Biológica</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-2año/SeguHigi-2AC.pdf" target="_blank">Seguridad e Higiene</a></li>
              <hr class="my-4">
              <h6 class="font-weight-bold">Tercer Año</h6>

              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-3año/Bioeti-3AC.pdf" target="_blank">Bioetica</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-3año/ControlCali-3AC.pdf" target="_blank">Control de Calidad</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-3año/GestionLab-3AC.pdf" target="_blank">Gestión de Laboratorio</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-3año/InvServSalud-3AC.pdf" target="_blank">Investigación en Servicios de Salud</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-3año/PracticasProf3-3AC.pdf" target="_blank">Prácticas Profesionalizantes 3</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Clinicos-3año/ProcesosDeLab2-3AC.pdf" target="_blank">Procesos de Laboratorio 2</a></li>

            </ul>
          </div>

        </div>
        <!-- fin materias -->

        <!-- Pasantias -->
        <div class="mt-1 tab-pane fade" id="v-pills-pasantias" role="tabpanel" aria-labelledby="v-pills-pasantias-tab">

        </div>
        <!-- fin Pasantias -->

        <!-- Horarios -->
        <div class="mt-1 tab-pane fade" id="v-pills-horarios" role="tabpanel" aria-labelledby="v-pills-horarios-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Laboratorio de Análisis Clínicos
          </h2>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Horarios</h5>
          <p>Horarios de cursada ciclo electivo año 2020 -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/horariosAC2020.pdf" target="_blank">Click aquí</a></p>
        </div>


        <!-- Fin horarios -->

        <div class="tab-pane fade" id="v-pills-correlatividades" role="tabpanel" aria-labelledby="v-pills-correlatividades-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Laboratorio de Análisis Clínicos
          </h2>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Correlatividades</h5>

          <p>Correlatividades de materias -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/correlatividadesClinicos.pdf" target="_blank">Click aquí</a></p>

        </div>

        <!-- Mastricula Bs.AS-->
        <div class="mt-1 tab-pane fade" id="v-pills-matriculaBsAs" role="tabpanel" aria-labelledby="v-pills-matriculaBsAs-tab">
        <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Laboratorio de Análisis Clínicos
          </h2>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Matrícula Provincial</h5>
          
                                <p>Link para gestionar Matrícula Provincial (Provincia de Buenos Aires) -> <a href="https://sistemas.ms.gba.gov.ar/profesionales/pre_web/login.php" target="_blank">Click aquí</a></p>
        </div>
        <!-- fin mastricula Bs.AS -->

         <!-- Mastricula Nacional-->
         <div class="mt-1 tab-pane fade" id="v-pills-matriculaNac" role="tabpanel" aria-labelledby="v-pills-matriculaNac-tab">
        <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Laboratorio de Análisis Clínicos
          </h2>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5 letragoogletitulo"> Matrícula Nacional</h5>
          
          <p>Link para gestionar Matrícula Nacional -> <a href="http://rups.msal.gov.ar/" target="_blank">Click aquí </a></p>
        </div>
        <!-- fin mastricula nacional -->


      </div>
    </div>
  </div>
</section>

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>