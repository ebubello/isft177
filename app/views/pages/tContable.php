<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<!-- <h1 class=" text-center navbar-dark colorfondo2 p-1">Instituto de Formación Técnica Superior N 177</h1> -->

<!-- Nav tabs INSTITUCIóN -->
 
<section class="container coloryellow">
<div class="container imagenContable d-flex justify-content-end mt-0">


        <a href=""><img src="<?php echo URL_PROJECT ?>/img/home/logoisft177.png" width="100" height="100" class="d-inline-block align-top rounded" alt="Logo ISFT 177" title="Instituto de Formación Tecnica Superior N°177"></a>
        </div>
  <div class="row">
    <div class="col-lg-3 text-white bg-dark">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <h4 class="text-dark text-center p-3 mt-3 colorfondo2 letragoogletitulo">CARRERAS</h4>

        <a class="nav-link text-white bg-dark active letragoogletitulo" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Perfil Profesional</a>
      
        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-salida-tab" data-toggle="pill" href="#v-pills-salida" role="tab" aria-controls="v-pills-salida" aria-selected="false">Salida Laboral</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-materias-tab" data-toggle="pill" href="#v-pills-materias" role="tab" aria-controls="v-pills-materias" aria-selected="false">Materias y Programas</a>

        <a class="nav-link text-white bg-dark disabled letragoogletitulo" id="v-pills-pasantias-tab" data-toggle="pill" href="#v-pills-pasantias" role="tab" aria-controls="v-pills-pasantias" aria-selected="false">Pasantías Laborales</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-horarios-tab" data-toggle="pill" href="#v-pills-horarios" role="tab" aria-controls="v-pills-horarios" aria-selected="false">Horarios</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-correlatividades-tab" data-toggle="pill" href="#v-pills-correlatividades" role="tab" aria-controls="v-pills-correlatividades" aria-selected="false">Correlatividades</a>
      
      </div>
    </div>
    <div class="d-flex col-lg-9">
      <div class="tab-content" id="v-pills-tabContent">
        <div class="mt-1 tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
            <hr class="my-3">
              <h2 class="letragoogletitulo text-left">
              Tecnicatura Superior en Administración Contable
              </h2>
                  <!-- Perfil profesional -->
              <hr class="my-2">
              <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Perfil Profesional</h5> 

                   <p class="text-justify">
                   El Técnico Superior en Administración es un profesional que estará capacitado para desarrollar las competencias para: organizar, programar, ejecutar y controlar las operaciones comerciales, financieras y administrativas de la organización; elaborar, controlar y registrar el flujo de información; organizar y planificar los recursos referidos para desarrollar sus actividades interactuando con el entorno y participando en la toma de decisiones relacionadas con sus actividades. Coordinando equipos de trabajo relacionado con su especialidad. Estas competencias serán desarrolladas según las incumbencias y las normas técnicas y legales que rigen su campo profesional.

                    </p>
        </div>
            <!-- fin perfil profesional -->

             <!-- Salida laboral -->

        <div class="mt-1 tab-pane fade" id="v-pills-salida" role="tabpanel" aria-labelledby="v-pills-salida-tab">
            <hr class="my-3">
               <h2 class="letragoogletitulo text-left">
               Tecnicatura Superior en Administración Contable
              </h2>
              <hr class="my-2">
              <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Salida Laboral</h5>  
              <p class="text-justify">
                                El Técnico Superior en Administración podrá desarrollar sus actividades en grandes empresas, pequeñas y medianas empresas y micro emprendimientos como también en el sector terciario.
                                Los roles del Técnico Superior podrán ser, desde fuertemente específicos, hasta marcadamente globales y de gestión; variando con el tamaño, contenido tecnológico y tipo de tamaño y gestión de la empresa en la que se desempeñe.
                                En empresas de mayor tamaño, participa, desde sus tareas específicas, dentro del equipo de gestión (trabajo en grupos, en células, etc.), incrementándose la participación en los aspectos más estratégicos y de la toma de decisiones a medida que el tamaño de la empresa disminuye.
                                El trabajo coordinado, en equipo y de interrelación con otros sectores ocupa un lugar clave en las actividades de proyecto, diseño y gestión.
                                Las funciones propias de este perfil puede diferenciarse de los distintos funcionarios según los grados de decisión, autonomía, responsabilidad, especificidad y la rutina de los roles que detenten en la organización.
                                Podrá cumplir distintas funciones dentro de la organización como ser:
                            </p>
                            <ul class="ml-3">
                                <li>Dirección y planeamiento</li>
                                <li>Producción</li>
                                <li>Recursos Humanos</li>
                                <li>Compras</li>
                                <li>Comercialización y Comercio Exterior</li>
                                <li>Financiación</li>
                                <li>Contabilización</li>
                                <li>Gestión integral</li>
                            </ul>
        </div>
                <!-- fin salida laboral -->

                <!-- Materias -->
        <div class="mt-1 tab-pane fade" id="v-pills-materias" role="tabpanel" aria-labelledby="v-pills-materias-tab">
           <hr class="my-3">
              <h2 class="letragoogletitulo text-left">
              Tecnicatura Superior en Administración Contable
              </h2>
               <hr class="my-2">
                 <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Materias y Programas</h5>
                 <p class="text-justify">
                                Para ver listado de materias por año con su respectiva carga horaria haga <a href="<?php echo URL_PROJECT ?>/documents/Materias/contableMaterias.pdf" target="_blank">Click aquí </a>
                            </p>

                    <hr class="my-3">
                    <h5 class="mt-0 ml-0 display-5">Programas de Estudio por Materia</h5><br>

                    <div class="ml-3">
                     <h6 class="font-weight-bold">Primer Año</h6>
                    <ul>
                    <li><a>Matemática I</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-1año/COMPUTACION_I_1RO_CONTABLE.pdf" target="_blank">Computación I</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-1año/DERECHO_CIVIL_1ro_CONTABLE.pdf" target="_blank">Derecho</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-1año/Economia_1ro_contable.pdf" target="_blank">Economía</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-1año/Contabilidad_I.pdf" target="_blank">Contabilidad</a></li>
                                <li><a>Sociología de la Organización</a></li>
                                <li><a>Principios de la Administración</a></li>
                                <li><a>Metodología de la Investigación</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-1año/Gestion_adm_cont.pdf" target="_blank">Gestión Administrativo Contable</a></li>


                      <hr class="my-4">
                      <h6 class="font-weight-bold">Segundo Año</h6>

                      <li><a>Matemática II</a></li>
                                <li><a>Estadística</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-2año/Ingles_2do_contable.pdf" target="_blank">Inglés I</a></li>
                                <li><a>Computación II</a></li>
                                <li><a>Contabilidad II</a></li>
                                <li><a>Matemática Financiera</a></li>
                                <li><a>Derecho Laboral</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-2año/Practica_prof_I.pdf" target="_blank">Práctica Profesional</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-2año/DERECHO_COMERCIAL_2do_contable.pdf" target="_blank">Derecho Comercial</a></li>
                      <hr class="my-4">
                      <h6 class="font-weight-bold">Tercer Año</h6>

                      <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-3año/Ingles_3ro_contable.pdf" target="_blank">Inglés II</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-3año/Tecnica_impos.pdf" target="_blank">Técnica Impositiva y Laboral</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-3año/Regimen_trib.pdf" target="_blank">Régimen Tributario</a></li>
                                <li><a>Contabilidad III</a></li>
                                <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Contable-3año/Costos.pdf" target="_blank">Costos y Presupuestos</a></li>
                                <li><a>Administración Financiera</a></li>
                                <li><a>Administración Estratégica</a></li>
                                <li><a>Práctica Profesional II</a></li>
                  
                  </ul>
                 </div>

        </div>
           <!-- fin materias -->

            <!-- Pasantias -->
              <div class="mt-1 tab-pane fade" id="v-pills-pasantias" role="tabpanel" aria-labelledby="v-pills-pasantias-tab">
         
              </div>
              <!-- fin Pasantias -->

               <!-- Horarios -->
                <div class="mt-1 tab-pane fade" id="v-pills-horarios" role="tabpanel" aria-labelledby="v-pills-horarios-tab">
                   <hr class="my-3">
              <h2 class="letragoogletitulo text-left">
              Tecnicatura Superior en Administración Contable
              </h2>
               <hr class="my-3">
                 <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Horarios de cursada</h5>
                  
                 <p> Horarios de cursada ciclo electivo año 2020 -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/horariosAContable.pdf" target="_blank">Click aquí</a></p>
                </div>


                <!-- Fin horarios -->

                <div class="tab-pane fade" id="v-pills-correlatividades" role="tabpanel" aria-labelledby="v-pills-correlatividades-tab">
                    <hr class="my-3">
                  <h2 class="letragoogletitulo text-left">
                  Tecnicatura Superior en Administración Contable
                 </h2>
                  <hr class="my-3">
                   <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Correlatividades</h5>
                     
                   <p>Correlatividades de materias -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/correlatividadesContable-273_03.pdf" target="_blank">Click aquí</a></p>

                </div>


       </div>
    </div>
  </div>
</section>

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>