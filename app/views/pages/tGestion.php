<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<style>
.imagenGestion {
  background-image: url(../img/carreras/gestamb1.jpg);
  height: 30vh;
  background-size: cover;
}
</style>

<!-- <h1 class=" text-center navbar-dark colorfondo2 p-1">Instituto de Formación Técnica Superior N 177</h1> -->

<!-- Nav tabs INSTITUCIóN -->
<div class="container imagenGestion d-flex justify-content-end mt-0">
    <a href=""><img src="<?php echo URL_PROJECT ?>/img/home/logoisft177.png" width="100" height="100" class="d-inline-block align-top rounded" alt="Logo ISFT 177" title="Instituto de Formación Tecnica Superior N°177"></a>
</div>
<section class="container coloryellow">
    <div class="row">
        <div class="col-lg-3 text-white bg-dark">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <h4 class="text-dark text-center p-3 mt-3 colorfondo2 letragoogletitulo">CARRERAS</h4>

                <a class="nav-link text-white bg-dark active letragoogletitulo" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Perfil Profesional</a>

                <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-salida-tab" data-toggle="pill" href="#v-pills-salida" role="tab" aria-controls="v-pills-salida" aria-selected="false">Salida Laboral</a>

                <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-materias-tab" data-toggle="pill" href="#v-pills-materias" role="tab" aria-controls="v-pills-materias" aria-selected="false">Materias y Programas</a>

                <a class="nav-link text-white bg-dark disabled letragoogletitulo" id="v-pills-pasantias-tab" data-toggle="pill" href="#v-pills-pasantias" role="tab" aria-controls="v-pills-pasantias" aria-selected="false">Pasantías Laborales</a>

                <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-horarios-tab" data-toggle="pill" href="#v-pills-horarios" role="tab" aria-controls="v-pills-horarios" aria-selected="false">Horarios</a>

                <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-correlatividades-tab" data-toggle="pill" href="#v-pills-correlatividades" role="tab" aria-controls="v-pills-correlatividades" aria-selected="false">Correlatividades</a>

            </div>
        </div>
        <div class="d-flex col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="mt-1 tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <hr class="my-3">
                    <h2 class="letragoogletitulo text-left">
                        Tecnicatura Superior en Gestión Ambiental y Salud
                    </h2>
                    <!-- Perfil profesional -->
                    <hr class="my-2">
                    <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Perfil Profesional</h5>

                    <p class="text-justify">
                        Es el profesional con formación científica, tecnológica y ética, competente para la intervención en los procesos técnicos específicos del campo de la gestión ambiental. Diseñará y ejecutará planes y programas tendientes a la vigilancia ambiental y sanitaria, en ámbitos urbanos y rurales. Asimismo, coordinará actividades de protección y promoción de la salud ambiental e implementará estrategias de atención primaria ambiental.
                    </p>
                </div>
                <!-- fin perfil profesional -->

                <!-- Salida laboral -->

                <div class="mt-1 tab-pane fade" id="v-pills-salida" role="tabpanel" aria-labelledby="v-pills-salida-tab">
                    <hr class="my-3">
                    <h2 class="letragoogletitulo text-left">
                        Tecnicatura Superior en Gestión Ambiental y Salud
                    </h2>
                    <hr class="my-2">
                    <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Salida Laboral</h5>
                    <p class="text-justify">
                        El Técnico Superior en Gestión Ambiental y Salud puede ejercer sus actividades, tanto en forma autónoma, como en relación de dependencia. Asimismo, tendrá la posibilidad de desempeñarse en:

                    </p>

                    <ul class="ml-3">
                        <li>Instituciones públicas y/o privadas de jurisdicción Nacional, Provincial y Municipal, del área de la salud y del ambiente.</li>
                        <li>Sistemas de gestión ambiental del subsector público y/o privado e interjurisdiccional.</li>
                        <li>Establecimientos industriales, parques industriales.</li>
                        <li>Aseguradoras de Riesgos del Trabajo (ART) Y todos aquellos ámbitos en donde se requiera su desempeño, acorde al marco legal vigente.</li>
                    </ul>
                </div>
                <!-- fin salida laboral -->

                <!-- Materias -->
                <div class="mt-1 tab-pane fade" id="v-pills-materias" role="tabpanel" aria-labelledby="v-pills-materias-tab">
                    <hr class="my-3">
                    <h2 class="letragoogletitulo text-left">
                        Tecnicatura Superior en Gestión Ambiental y Salud
                    </h2>
                    <hr class="my-2">
                    <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Materias y Programas</h5>
                    <p>
                        Para ver listado de materias por año con su respectiva carga horaria haga <a href="<?php echo URL_PROJECT ?>/documents/Materias/gestionMaterias.pdf" target="_blank">Click aquí</a>
                    </p>
                    <hr class="my-3">
                    <h5 class="mt-0 ml-0 display-5">Programas de Estudio por Materia</h5><br>

                    <div class="ml-3">
                        <h6 class="font-weight-bold">Primer Año</h6>
                        <ul>
                            <li><a>Matemática I</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Biologia_1ro_Gestion.pdf" target="_blank">Biología</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Quimica_1ro_gestion.pdf" target="_blank">Química</a></li>
                            <li><a>Física</a></li>
                            <li><a>Matemática y estadisticas</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Metod_investig_1ro_gestion.pdf" target="_blank">Metodología de la investigación</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Derecho_amb_1ro_gestion.pdf" target="_blank">Derecho Ambiental</a></li>
                            <li><a>Salud y problemática ambiental I</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/EDI_1ro_gestion.pdf" target="_blank">Espacio de definición institucional</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Prac_profes_I_1ro_Gestion.pdf" target="_blank">Práctica Profesional I</a></li>

                            <hr class="my-4">
                            <h6 class="font-weight-bold">Segundo Año</h6>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Ecologia_2do_Gestion.pdf" target="_blank">Ecología</a></li>
                            <li><a>Inglés Técnico</a></li>
                            <li><a>Geología e Hidrología</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Quimica_Amb_2do_gestion.pdf" target="_blank">Química del Ambiente</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Epidemiologia_2do_gestion.pdf" target="_blank">Epidemiología</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/Salud_y_prob_amb_II_2do_gestion.pdf" target="_blank">Salud y problemática Ambiental II</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-1año/EDI_II.pdf" target="_blank">Espacio de definición institucional</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-2año/Prac_prof_II_2do_gestion.pdf" target="_blank">Práctica Profesional II</a></li>
                            <hr class="my-4">
                            <h6 class="font-weight-bold">Tercer Año</h6>

                            <li><a>Sociología de las Organizaciones</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-3año/Control_amb_3ro_gestion.pdf" target="_blank">Control Ambiental</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-3año/Seg_higiene_3ro_gestion.pdf" target="_blank">Seguridad e higiene en el trabajo</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-3año/Gestion_Medio_Ambiental_3ro_gestion.pdf" target="_blank">Gestión Medio Ambiental</a></li>
                            <li><a>Salud y problemática Ambiental III</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-3año/EDI_3ro_Gestion.pdf" target="_blank">Espacio de definición institucional</a></li>
                            <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Gestion-3año/Prac_profes_III_3ro_Gestion.pdf" target="_blank">Práctica Profesional II</a></li>
                        </ul>
                    </div>

                </div>
                <!-- fin materias -->

                <!-- Pasantias -->
                <div class="mt-1 tab-pane fade" id="v-pills-pasantias" role="tabpanel" aria-labelledby="v-pills-pasantias-tab">

                </div>
                <!-- fin Pasantias -->

                <!-- Horarios -->
                <div class="mt-1 tab-pane fade" id="v-pills-horarios" role="tabpanel" aria-labelledby="v-pills-horarios-tab">
                    <hr class="my-3">
                    <h2 class="letragoogletitulo text-left">
                        Tecnicatura Superior en Gestión Ambiental y Salud
                    </h2>
                    <hr class="my-3">
                    <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Horarios de cursada</h5>

                    <p>Horarios de cursada ciclo electivo año 2020 -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/horariosGA2020.pdf" target="_blank">Click aquí</a></p>
                </div>


                <!-- Fin horarios -->

                <div class="tab-pane fade" id="v-pills-correlatividades" role="tabpanel" aria-labelledby="v-pills-correlatividades-tab">
                    <hr class="my-3">
                    <h2 class="letragoogletitulo text-left">
                        Tecnicatura Superior en Gestión Ambiental y Salud
                    </h2>
                    <hr class="my-3">
                    <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Correlatividades</h5>
                    <p>Correlatividades de materias -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/GesAmb-442-08.pdf" target="_blank">Click aquí</a></p>

                </div>


            </div>
        </div>
    </div>
</section>

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>