<?php

// Header HTML
include_once URL_APP . '/views/custom/header.php';
// Banner
include_once URL_APP . '/views/custom/banner.php';
// Barra de Navegación
include_once URL_APP . '/views/custom/navbar.php';

?>

<!-- <h1 class=" text-center navbar-dark colorfondo2 p-1">Instituto de Formación Técnica Superior N 177</h1> -->

<!-- Nav tabs INSTITUCIóN -->
<div class="container imagenSistemas d-flex justify-content-end mt-0">
  <a href=""><img src="<?php echo URL_PROJECT ?>/img/home/logoisft177.png" width="100" height="100" class="d-inline-block align-top rounded" alt="Logo ISFT 177" title="Instituto de Formación Tecnica Superior N°177"></a>
</div>
<section class="container coloryellow">
  <div class="row">
    <div class="col-lg-3 text-white bg-dark">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <h4 class="text-dark text-center p-3 mt-3 colorfondo2 letragoogletitulo">CARRERAS</h4>

        <a class="nav-link text-white bg-dark active letragoogletitulo" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Perfil Profesional</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-salida-tab" data-toggle="pill" href="#v-pills-salida" role="tab" aria-controls="v-pills-salida" aria-selected="false">Salida Laboral</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-materias-tab" data-toggle="pill" href="#v-pills-materias" role="tab" aria-controls="v-pills-materias" aria-selected="false">Materias y Programas</a>

        <a class="nav-link text-white bg-dark disabled letragoogletitulo" id="v-pills-pasantias-tab" data-toggle="pill" href="#v-pills-pasantias" role="tab" aria-controls="v-pills-pasantias" aria-selected="false">Pasantías Laborales</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-horarios-tab" data-toggle="pill" href="#v-pills-horarios" role="tab" aria-controls="v-pills-horarios" aria-selected="false">Horarios</a>

        <a class="nav-link text-white bg-dark letragoogletitulo" id="v-pills-correlatividades-tab" data-toggle="pill" href="#v-pills-correlatividades" role="tab" aria-controls="v-pills-correlatividades" aria-selected="false">Correlatividades</a>

      </div>
    </div>
    <div class="d-flex col-lg-9">
      <div class="tab-content" id="v-pills-tabContent">
        <div class="mt-1 tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Análisis de Sistemas
          </h2>
          <!-- Perfil profesional -->
          <hr class="my-2">
          <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Perfil Profesional</h5>

          <p class="text-justify">
            El/la Técnico/a Superior en Análisis de Sistemas estará capacitado para diagnosticar necesidades,
            diseñar, desarrollar, poner en servicio y mantener productos, servicios o soluciones informáticas acorde a las organizaciones que lo requieran.
            Estas competencias serán desarrolladas según las incumbencias y las normas técnicas y legales que rigen su campo profesional.

          </p>
        </div>
        <!-- fin perfil profesional -->

        <!-- Salida laboral -->

        <div class="mt-1 tab-pane fade" id="v-pills-salida" role="tabpanel" aria-labelledby="v-pills-salida-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Análisis de Sistemas
          </h2>
          <hr class="my-2">
          <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Salida Laboral</h5>
          <p class="text-justify">
            El Instituto Superior de Formación Técnica N° 177 se encuentra ubicado en la localidad de Libertad, Partido de Merlo, Provincia de Buenos Aires, y funciona ininterrumpidamente desde el año 1992, año en el que fue creado a partir de un convenio con el Ministerio de Cultura y Educación entre la Dirección de Educación de Adultos y el Obispado de Morón a través de la Parroquia San José iniciando sus actividades con la carrera de Técnico Superior en Administración de Empresas, carrera que, aún hoy, se mantiene.
          </p>
          <p class="text-justify">
            En el año 1993, se abren otras dos carreras, las de Técnico Superior en Análisis de Sistemas y de Técnico Superior en Análisis Clínicos, ésta última se mantiene actualmente como Tecnólogo en Salud con Orientación en Análisis Clínicos.
          </p>
          <p class="text-justify">
            A partir del año 1994, con el inicio del ciclo lectivo, el Instituto pasó a depender de la Dirección General de Cultura y Educación, manteniendo las tres carreras, así hasta el año 2001, año en el cual, atendiendo a las demandas de la sociedad, se abre la carrera de Técnico Superior en Recreación, la que se cierra en el año 2004.
          </p>
          <p class="text-justify">
            En el año 2005 se abre la carrera de Técnico Superior en Comunicación Social con orientación en Planificación Institucional y Comunitaria.
          </p>
          <p class="text-justify">
            En cuanto a la Planta Orgánico Funcional, el Instituto cuenta con un Director, un Secretario, una Bibliotecaria, dos bedeles y aproximadamente 50 docentes para atender una matrícula de más de 200 alumnos. En cuanto al personal, 17 son docentes y el resto son profesionales, contando con 8 especialistas en computación o Sistemas, 3 ingenieros, 2 abogados, 13 licenciados en distintas especialidades, un Magister en Metodología de la Investigación, 6 bioquímicos, 4 contadores públicos; muchos de los cuales han realizado el ciclo de Capacitación Docente, conjugando una serie de factores que prestigian al servicio educativo.
          </p>
          <p class="text-justify">
            En relación a la población estudiantil, el Instituto recibe alumnos, no sólo de Libertad y zonas aledañas, sino también de localidades y partidos vecinos, como Merlo, Ituzaingó, Castelar, Morón, Rafael Castillo, Moreno, Mariano Acosta, Marcos Paz entre otras. Esto se debe a que el Instituto es el único Técnico de gestión estatal en el Distrito y a que las carreras que poseemos les otorgan amplias perspectivas laborales con sólidas posibilidades de inserción en el sector socio-productivo.
          </p>
          <p class="text-justify">
            El Instituto, por ser conveniado, funciona en las instalaciones de la EP "Instituto José Manuel Estrada" perteneciente a la Parroquia San José y constituye parte de una amplia red de instituciones que atienden las necesidades educativas para adolescentes y adultos.
          </p>
          <p class="text-justify">
            Actualmente estamos trabajando en red con el I.S.F.D. N° 29 de Merlo, con el cual se están implementando distintos proyectos. En el marco de las prácticas de laboratorio y pasantías de la carrera de Análisis Clínicos, se han establecido convenios con distintos hospitales y clínicas de la zona.
          </p>
          <p class="text-justify">
            Además se establecieron convenios de colaboración recíproca entre escuelas e instituciones, donde cabe destacar los establecidos con la Escuela de Educación Media N° 11 (actualmente Escuela de Educación Técnica N°5) y el Centro de Formación Profesional N° 401.
          </p>
          <p class="text-justify">
            Consideramos, como Institución de formación Superior, que la educación es el único medio de brindar a nuestros alumnos las posibilidades de acceder a una mejor calidad de vida, de poder insertarse con éxito en el mercado laboral. Vemos fundamentado lo antes dicho por los egresados que hoy se encuentran trabajando en hospitales, clínicas, empresas, escuelas, pymes etc. de la zona, y son éstos egresados los que nos impulsan a seguir trabajando para ofrecer año a año una mejor y más amplia oferta educativa.
          </p>
        </div>
        <!-- fin salida laboral -->

        <!-- Materias -->
        <div class="mt-1 tab-pane fade" id="v-pills-materias" role="tabpanel" aria-labelledby="v-pills-materias-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Análisis de Sistemas
          </h2>
          <hr class="my-2">
          <h5 class="mt-4 ml-0 display-5 letragoogletitulo">Materias y Programas</h5>
          <p>
            Para ver listado de materias por año con su respectiva carga horaria haga <a href="<?php echo URL_PROJECT ?>/documents/Materias/sistemasMaterias.pdf" target="_blank">Click aquí </a>
          </p>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5">Programas de Estudio por Materia</h5><br>

          <div class="ml-3">
            <h6 class="font-weight-bold">Primer Año</h6>
            <ul>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/Algebra-1AS.pdf" target="_blank">Álgebra</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/AnMat1-1AS.pdf" target="_blank">Análisis Matemático I</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/Ingles1-1AS.pdf" target="_blank">Inglés I</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/CienTecySoc-1AS.pdf" target="_blank">Ciencia, Tecnología y Sociedad</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/AlgyEstrucDat1-1AS.pdf" target="_blank">Algoritmos y estructuras de datos I</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/SistyOrg-1AS.pdf" target="_blank">Sistemas y Organizaciones</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/ArqComp-1AS.pdf" target="_blank">Arquitectura de Computadores</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-1año/PracProf1-1AS.pdf" target="_blank">Prácticas Profesionalizantes I</a></li>

              <hr class="my-4">
              <h6 class="font-weight-bold">Segundo Año</h6>

              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/Estadistica-2AS.pdf" target="_blank">Estadística</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/AnMat2-2AS.pdf" target="_blank">Análisis Matemático II</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/Ingles2-2AS.pdf" target="_blank"> Inglés II</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/IngSoft1-2AS.pdf" target="_blank">Ingeniería de Software I</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/AlgyEstrucDat2-2AS.pdf" target="_blank">Algoritmos y estructuras de datos II</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/SistOp-2AS.pdf" target="_blank"> Sistemas Operativos</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/PracProf2-2AS.pdf" target="_blank">Prácticas Profesionalizantes II</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-2año/BasDat-2AS.pdf" target="_blank">Bases de Datos</a></li>
              <hr class="my-4">
              <h6 class="font-weight-bold">Tercer Año</h6>

              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-3año/Ingles3-3AS.pdf" target="_blank">Inglés III</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-3año/AspLegProfe-3AS.pdf">Aspectos legales de la Profesión</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-3año/SemiActu-3AS.pdf">Seminario de actualización</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-3año/RedesyComu-3AS.pdf">Redes y Comunicaciones</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-3año/IngSoft2-3AS.pdf">Ingeniería de Software II</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-3año/AlgyEstrucDat3-3AS.pdf">Algoritmos y estructuras de datos III</a></li>
              <li><a href="<?php echo URL_PROJECT ?>/documents/Materias/Sistemas-3año/PractProf3-3AS.pdf">Prácticas Profesionalizantes III</a></li>

            </ul>
          </div>

        </div>
        <!-- fin materias -->

        <!-- Pasantias -->
        <div class="mt-1 tab-pane fade" id="v-pills-pasantias" role="tabpanel" aria-labelledby="v-pills-pasantias-tab">

        </div>
        <!-- fin Pasantias -->

        <!-- Horarios -->
        <div class="mt-1 tab-pane fade" id="v-pills-horarios" role="tabpanel" aria-labelledby="v-pills-horarios-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Análisis de Sistemas
          </h2>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Horarios</h5>

          <p>Horarios de cursada ciclo electivo año 2020 -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/horariosAS2020.pdf" target="_blank">Click aquí</a></p>
        </div>


        <!-- Fin horarios -->

        <div class="tab-pane fade" id="v-pills-correlatividades" role="tabpanel" aria-labelledby="v-pills-correlatividades-tab">
          <hr class="my-3">
          <h2 class="letragoogletitulo text-left">
            Tecnicatura Superior en Análisis de Sistemas
          </h2>
          <hr class="my-3">
          <h5 class="mt-0 ml-0 display-5 letragoogletitulo">Correlatividades</h5>

          <p>Correlatividades de materias -> <a href="<?php echo URL_PROJECT ?>/documents/Materias/correlatividadesSistemas.pdf" target="_blank">Click aquí</a></p>

        </div>


      </div>
    </div>
  </div>
</section>

<?php

// Footer
include_once URL_APP . '/views/custom/footer.php';

?>