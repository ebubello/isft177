<?php


include_once URL_APP . '/views/custom/header.php';

// Barra de Navegación
include_once URL_APP . '/views/custom/navbar_preInsc.php';

?>
<!-- <div class="container mt-4">
<div id="respuesta" ></div>
</div> -->

<body class="my-login-page">

	<section class="h-100">

		<div class="container h-100">

			<div class="row justify-content-md-center h-100">
				<div class="card-wrapper">
					<div class="brand">
						<img src="<?php echo URL_PROJECT ?>/img/login/logo.jpg" alt="logo">
					</div>

					<div class="card fat">
						<div class="card-body">

							<h4 class="card-title">Iniciar Sesión</h4>
							<p id="respuesta"></p>
							<form id="formLogin" method="POST" class="my-login-validation" novalidate="" action="#">
								<div class="form-group">
									<label for="username">DNI</label>
									<input id="dni" type="number" class="form-control" name="dni" value="" required autofocus>
									<div class="invalid-feedback">
										Usuario no valido
									</div>
								</div>

								<div class="form-group">
									<label for="password">Contraseña
										<a href="<?php echo URL_PROJECT ?>/Usuario/forgot" class="float-right">
											¿Olvidó su contraseña?
										</a>
									</label>
									<input id="password" type="password" class="form-control" name="password" required data-eye>
									<div class="invalid-feedback">
										La contraseña es requerida
									</div>
								</div>

								<div class="form-group">
									<div class="custom-checkbox custom-control">
										<input type="checkbox" name="remember" id="remember" class="custom-control-input">
										<label for="remember" class="custom-control-label">Recuerdame</label>
									</div>
								</div>

								<div class="form-group m-0">
									<button type="submit" class="btn btn-primary btn-block">
										Ingresar
									</button>
								</div>

							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

	<!-- JavaScript -->
	<!-- <script src="<?php echo URL_PROJECT ?>js/login/my-login.js"></script> -->
	<script src="<?php echo URL_PROJECT ?>/js/usuario/usuario.js"></script>
</body>

</html>