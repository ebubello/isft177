-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-10-2020 a las 19:24:37
-- Versión del servidor: 10.3.22-MariaDB-1ubuntu1
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_alumno`
--

DELIMITER $$
--
-- Procedimientos
--
$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anuncios`
--

CREATE TABLE `anuncios` (
  `Id_Anuncio` int(11) NOT NULL,
  `Titulo` varchar(150) DEFAULT NULL,
  `Descripcion` varchar(500) DEFAULT NULL,
  `Fecha_Inicio` date DEFAULT NULL,
  `Fecha_Fin` date DEFAULT NULL,
  `Prioridad` int(11) DEFAULT NULL,
  `Link` varchar(250) DEFAULT NULL,
  `Nombre_Link` varchar(100) DEFAULT NULL,
  `Imagen` varchar(200) DEFAULT NULL,
  `Fecha_Alta` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `anuncios`
--

INSERT INTO `anuncios` (`Id_Anuncio`, `Titulo`, `Descripcion`, `Fecha_Inicio`, `Fecha_Fin`, `Prioridad`, `Link`, `Nombre_Link`, `Imagen`, `Fecha_Alta`) VALUES
(1, 'Aquí debería ir el título', 'Aquí debería ir la descripción', '2020-09-27', '2020-10-10', 2, 'https://www.google.com/', '', 'utiles.jpg', '2020-09-27 23:11:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document_type`
--

CREATE TABLE `document_type` (
  `type` varchar(1) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `document_type`
--

INSERT INTO `document_type` (`type`, `description`) VALUES
('C', 'Común para la tecnicatura'),
('U', 'Único para la tecnicatura');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `module`
--

INSERT INTO `module` (`id`, `description`) VALUES
(1, 'Pre-Inscripcion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE `novedades` (
  `Id_Novedad` int(11) NOT NULL,
  `Titulo` varchar(150) DEFAULT NULL,
  `Descripcion` text DEFAULT NULL,
  `Fecha_Inicio` date DEFAULT NULL,
  `Fecha_Fin` date DEFAULT NULL,
  `Prioridad` int(11) DEFAULT NULL,
  `Link` varchar(250) DEFAULT NULL,
  `Nombre_Link` varchar(100) DEFAULT NULL,
  `Imagen` varchar(200) DEFAULT NULL,
  `Fecha_Alta` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`Id_Novedad`, `Titulo`, `Descripcion`, `Fecha_Inicio`, `Fecha_Fin`, `Prioridad`, `Link`, `Nombre_Link`, `Imagen`, `Fecha_Alta`) VALUES
(1, 'Suspensión de actividades por COVID-19.', 'Se han suspendido las actividades presenciales. Si eres un alumno inscrito ingresa al campus virtual.', '2020-08-24', '2020-10-10', 1, 'https://campus.isft177.edu.ar/', 'Campus', 'covid_19.png', '2020-08-17 22:04:50'),
(3, 'Informes e inscripción.', 'Lunes a Viernes de 18:30 hs a 21:30 hs. Se han suspendido las actividades presenciales. Si eres un alumno inscrito ingresa al campus virtual.', '2020-09-28', '2020-12-12', 1, 'https://new.isft177.edu.ar/home/contacto', 'Contáctenos', '', '2020-09-28 19:42:19'),
(4, 'Novedad', 'Nueva novedad', '2020-10-05', '2020-10-06', 3, 'https://new.isft177.edu.ar', '', 'covid_19.png', '2020-10-05 14:11:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operation`
--

CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `idmodule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `operation`
--

INSERT INTO `operation` (`id`, `description`, `idmodule`) VALUES
(1, 'Create', 1),
(2, 'Read', 1),
(3, 'Update', 1),
(4, 'Delete', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preinscrition_config`
--

CREATE TABLE `preinscrition_config` (
  `id` int(11) NOT NULL,
  `begin_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preinscrition_config`
--

INSERT INTO `preinscrition_config` (`id`, `begin_date`, `end_date`, `status`) VALUES
(1, '2020-10-03', '2020-10-03', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_inscription`
--

CREATE TABLE `pre_inscription` (
  `last_name` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `dni` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `status` varchar(7) DEFAULT NULL,
  `download` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `technical_career` int(11) NOT NULL,
  `path` varchar(100) DEFAULT NULL,
  `action` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pre_inscription`
--

INSERT INTO `pre_inscription` (`last_name`, `name`, `address`, `dni`, `email`, `phone_number`, `status`, `download`, `technical_career`, `path`, `action`) VALUES
('Paez', 'Fito', 'Tucuman 713', 11111111, 'fpaez@gmail.com', '123444554535435', 'NOT OK', '2020-10-03 19:48:55', 1, '../public/uploadFiles/11111111/', 'A'),
('martinez', 'fabricio', 'deseado 3245', 33186495, 'fabricio-martinez@hotmail.com.ar', '1141963213', '--', '2020-09-04 23:03:28', 1, '../public/uploadFiles/33186495/', 'A'),
('Galeano', 'Joaquin Ezequiel', 'Whatever 123', 39389129, 'joa.cavs@gmail.com', '1157653392', '--', '2020-09-19 23:13:34', 1, '../public/uploadFiles/39389129/', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_inscription_document`
--

CREATE TABLE `pre_inscription_document` (
  `id` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `doc_description` varchar(100) NOT NULL,
  `download` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_status` int(11) DEFAULT NULL,
  `idTecCareer` int(11) NOT NULL,
  `docType` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pre_inscription_document`
--

INSERT INTO `pre_inscription_document` (`id`, `dni`, `doc_description`, `download`, `id_status`, `idTecCareer`, `docType`) VALUES
(601, 33186495, 'Inscripcion-DNI-33186495-act 11 1°ayc.pdf', '2020-09-04 23:03:28', 3, 1, 'C'),
(602, 33186495, 'DNI-33186495-act 11 1°ayc.pdf', '2020-09-04 23:03:28', 3, 1, 'C'),
(603, 33186495, 'LS-DNI-33186495-act 11 1°ayc.pdf', '2020-09-04 23:03:28', 3, 1, 'C'),
(604, 33186495, 'Reglamento-DNI-33186495-act 11 1°ayc.pdf', '2020-09-04 23:03:28', 3, 1, 'C'),
(685, 39389129, 'DNI-39389129-DNI-497653047-Archivo1.pdf', '2020-09-19 23:13:34', 3, 1, 'C'),
(686, 39389129, 'Foto-DNI-39389129-Foto-DNI-497653047-logo_progresar.png', '2020-09-19 23:13:34', 3, 1, 'C'),
(687, 39389129, 'Titulo-DNI-39389129-Reglamento-DNI-497653047-Archivo3.pdf', '2020-09-19 23:13:34', 3, 1, 'C'),
(688, 39389129, 'Inscripcion-DNI-39389129-PNac-DNI-497653047-Archivo4.pdf', '2020-09-19 23:13:34', 3, 1, 'U'),
(689, 39389129, 'ABC-DNI-39389129-ABC-DNI-497653047-Archivo 2.pdf', '2020-09-19 23:13:34', 3, 1, 'U'),
(690, 39389129, 'Reglamento-DNI-39389129-Titulo-DNI-497653047-Archivo 2.pdf', '2020-09-19 23:13:34', 3, 1, 'C'),
(691, 39389129, 'PNac-DNI-39389129-Titulo-DNI-497653047-Archivo 2.pdf', '2020-09-19 23:13:34', 3, 1, 'C'),
(692, 39389129, 'LS-DNI-39389129-Inscripcion-DNI-497653047-Archivo1.pdf', '2020-09-19 23:13:34', 3, 1, 'U'),
(693, 11111111, 'DNI-11111111-2 - copia.pdf', '2020-10-03 19:49:02', 1, 1, 'C'),
(694, 11111111, 'PNac-DNI-11111111-4 - copia (3).pdf', '2020-10-03 19:48:37', 3, 1, 'C'),
(695, 11111111, 'LS-DNI-11111111-3 - copia (2).pdf', '2020-10-03 19:48:37', 3, 1, 'C'),
(696, 11111111, 'ABC-DNI-11111111-1.pdf', '2020-10-03 19:48:37', 3, 1, 'U'),
(697, 11111111, 'Foto-DNI-11111111-images.jpg', '2020-10-03 19:48:37', 3, 1, 'C'),
(698, 11111111, 'Reglamento-DNI-11111111-7 - copia (6).pdf', '2020-10-03 19:48:37', 3, 1, 'C'),
(699, 11111111, 'Titulo-DNI-11111111-5- copia (4).pdf', '2020-10-03 19:48:37', 3, 1, 'C'),
(700, 11111111, 'Inscripcion-DNI-11111111-6 - copia (5).pdf', '2020-10-03 19:48:37', 3, 1, 'U');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_ins_doc_definition`
--

CREATE TABLE `pre_ins_doc_definition` (
  `id` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pre_ins_doc_definition`
--

INSERT INTO `pre_ins_doc_definition` (`id`, `description`) VALUES
(1, 'OK'),
(2, 'NOT_OK'),
(3, '--');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `description`) VALUES
(1, 'Administrator'),
(2, 'Preceptor'),
(3, 'Alumno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_operation`
--

CREATE TABLE `rol_operation` (
  `id` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idoperation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol_operation`
--

INSERT INTO `rol_operation` (`id`, `idrol`, `idoperation`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 2, 1),
(6, 2, 2),
(7, 2, 3),
(8, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `technical_career`
--

CREATE TABLE `technical_career` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `technical_career`
--

INSERT INTO `technical_career` (`id`, `description`) VALUES
(1, 'Tecnicatura Superior en Análisis de Sistemas'),
(2, 'Tecnicatura Superior en Laboratorio de Análisis Clínicos'),
(3, 'Tecnicatura Superior en Administración Contable'),
(4, 'Tecnicatura Superior en Gestión Ambiental y Salud');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `dni` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` char(60) NOT NULL,
  `creationdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `idrol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `dni`, `email`, `password`, `creationdate`, `idrol`) VALUES
(6, 'admin', 29863127, 'ebubello@gmail.com', '$2y$10$xLFSZgtZCb1He./9VAImzuaEaskdDK85IIEEa128VXEg/HvtE4u9G', '2020-09-15 00:25:01', 1),
(36, 'Prueba', 11111111, 'prueba@gmail.com', '$2y$10$Vs1bu.bFYj.65LcBB62dpOZE39sm3z7VVL15HnJVzAMh7faZ4gRky', '2020-07-28 21:27:49', 3),
(37, 'Walter', 21728439, 'wdcarnero@gmail.com', '$2y$10$O2dEwkCgrLE3bj0evBHDauyoafdetPpPzwLG53qM.OggfYw7jOYkO', '2020-08-31 22:36:46', 1),
(38, 'Guest', 22222222, 'jajajaja@gmail.com', '$2y$10$5dkT56dMCMCV.7LibZrpmeB4BCWp.pIuqiyeLBHmu2GzRC4s0mp82', '2020-09-19 21:27:15', 2),
(39, 'admin', 99999999, 'isft.177.prueba@gmail.com', '$2y$10$I5XeOz6/cSwdGT5jGWn.Pu5K1hIOhHYE4zyiXpgmTMmQuhdA.YqOG', '2020-09-28 23:49:20', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anuncios`
--
ALTER TABLE `anuncios`
  ADD PRIMARY KEY (`Id_Anuncio`);

--
-- Indices de la tabla `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`type`);

--
-- Indices de la tabla `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `novedades`
--
ALTER TABLE `novedades`
  ADD PRIMARY KEY (`Id_Novedad`);

--
-- Indices de la tabla `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_operation_module` (`idmodule`);

--
-- Indices de la tabla `preinscrition_config`
--
ALTER TABLE `preinscrition_config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pre_inscription`
--
ALTER TABLE `pre_inscription`
  ADD PRIMARY KEY (`dni`,`email`,`technical_career`) USING BTREE;

--
-- Indices de la tabla `pre_inscription_document`
--
ALTER TABLE `pre_inscription_document`
  ADD PRIMARY KEY (`id`,`dni`,`idTecCareer`),
  ADD KEY `FK_pre_inscription_document_pre_ins_doc_definition` (`id_status`),
  ADD KEY `FK_pre_inscription_document_technical_career` (`idTecCareer`),
  ADD KEY `FK_pre_inscription_document_document_type` (`docType`);

--
-- Indices de la tabla `pre_ins_doc_definition`
--
ALTER TABLE `pre_ins_doc_definition`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol_operation`
--
ALTER TABLE `rol_operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_rol_operation_operation` (`idoperation`),
  ADD KEY `FK_rol_operation_rol` (`idrol`);

--
-- Indices de la tabla `technical_career`
--
ALTER TABLE `technical_career`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`dni`,`email`),
  ADD KEY `FK_user_rol` (`idrol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anuncios`
--
ALTER TABLE `anuncios`
  MODIFY `Id_Anuncio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `novedades`
--
ALTER TABLE `novedades`
  MODIFY `Id_Novedad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pre_inscription_document`
--
ALTER TABLE `pre_inscription_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=701;

--
-- AUTO_INCREMENT de la tabla `pre_ins_doc_definition`
--
ALTER TABLE `pre_ins_doc_definition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rol_operation`
--
ALTER TABLE `rol_operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `technical_career`
--
ALTER TABLE `technical_career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `operation`
--
ALTER TABLE `operation`
  ADD CONSTRAINT `FK_operation_module` FOREIGN KEY (`idmodule`) REFERENCES `module` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pre_inscription_document`
--
ALTER TABLE `pre_inscription_document`
  ADD CONSTRAINT `FK_pre_inscription_document_document_type` FOREIGN KEY (`docType`) REFERENCES `document_type` (`type`),
  ADD CONSTRAINT `FK_pre_inscription_document_pre_ins_doc_definition` FOREIGN KEY (`id_status`) REFERENCES `pre_ins_doc_definition` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_pre_inscription_document_technical_career` FOREIGN KEY (`idTecCareer`) REFERENCES `technical_career` (`id`);

--
-- Filtros para la tabla `rol_operation`
--
ALTER TABLE `rol_operation`
  ADD CONSTRAINT `FK_rol_operation_operation` FOREIGN KEY (`idoperation`) REFERENCES `operation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rol_operation_rol` FOREIGN KEY (`idrol`) REFERENCES `rol` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_rol` FOREIGN KEY (`idrol`) REFERENCES `rol` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
