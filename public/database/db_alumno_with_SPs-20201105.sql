-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-11-2020 a las 04:42:08
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_alumno`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `PreIns_UpdateStatus` (IN `_dni` INT, IN `_idTecCareer` VARCHAR(1))  begin
	
	
set @value := ( select if (count(dni) = 2, "true","false") from pre_inscription_document where dni = _dni and idTecCareer = _idTecCareer);

if(@value)then
	
	update pre_inscription set status = 'NOT OK' where Technical_Career = _idTecCareer and  dni in (select  distinct dni from pre_inscription_document where id_status in (2,3) and dni = _dni and idTecCareer = _idTecCareer group by dni having count(id_status) >= 1 );

	update pre_inscription set status = 'OK' where  Technical_Career = _idTecCareer and dni in (select  distinct dni from pre_inscription_document where id_status in (1) and dni = _dni  and idTecCareer = _idTecCareer group by dni having count(id_status) = 2 );

	else

	update pre_inscription set status = 'OK' where  Technical_Career = _idTecCareer and dni in (select  distinct dni from pre_inscription_document where id_status in (1) and dni = _dni group by dni having (count(id_status)>7) );

	update pre_inscription set status = 'NOT OK' where Technical_Career = _idTecCareer and  dni in (select  distinct dni from pre_inscription_document where id_status in (1) and dni = _dni group by dni having (count(id_status)<7) );

	update pre_inscription set status = 'NOT OK' where Technical_Career = _idTecCareer and  dni in (select  distinct dni from pre_inscription_document where id_status in (2)  and dni = _dni group by dni having (count(id_status)>0) );

	update pre_inscription set status = '--' where Technical_Career = _idTecCareer and  dni in (select  distinct dni from pre_inscription_document where id_status in (3) and dni = _dni group by dni having (count(id_status)>7) );

end if;

end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_active_pre_inscription` (IN `_dni` INT, IN `_dniUser` INT)  SQL SECURITY INVOKER
    COMMENT 'SP activar pre inscripciones'
BEGIN	

	set @value := (select if(idrol = 1, "true","false") from user  where dni= _dniUser);		
    
if(@value = "true")then
	update pre_inscription set action= 'A' where dni= _dni;
	end if;
END$$

CREATE DEFINER=`db_alumno`@`localhost` PROCEDURE `sp_delete_pre_inscription` (IN `_dni` INT, IN `_dniUser` INT, IN `_idTec` VARCHAR(1))  SQL SECURITY INVOKER
    COMMENT 'SP de borrado de pre inscripciones'
BEGIN	

	set @value := (select if(idrol = 1, "true","false") from user  where dni= _dniUser);	
    set @value2 := (select if(count(dni)>1, "true","false")from pre_inscription where dni = _dni );

 /* Si el idRol = 1 y tengo mas de una tecnicatura para un mismo DNI, borro solamente los documentos Unicos "U" */ 
   
if( @value = "true" and @value2 = "true" )then   
	delete from pre_inscription_document where dni = _dni and idTecCareer = _idTec and docType = 'U';
	delete from pre_inscription where dni = _dni and technical_career = _idTec;

elseif( @value = "true" and @value2 = "false" )then  /* Si el idRol = 1 y tengo una sola tecnicatura para un DNI, borro todo */ 

	delete from pre_inscription_document where dni = _dni;
	delete from pre_inscription where dni = _dni and technical_career = _idTec;

else	 /* Si el idRol <> 1 pongo el Action = D */ 
	update pre_inscription set `action` = 'D' where dni = _dni and technical_career = _idTec;	end if;
END$$

CREATE DEFINER=`db_alumno`@`localhost` PROCEDURE `sp_get_pre_inscription_document` (IN `_dni` INT, IN `_idTec` VARCHAR(1))  SQL SECURITY INVOKER
    COMMENT 'SP listar los documentos asociados a las PreInscripciones'
BEGIN	

					  SELECT PID.id, 
                             PID.doc_description, 
                             PID.dni, 
                             PDD.description, 
                             PID.idTecCareer, 
                             TC.description as Description
                        FROM 
                            pre_inscription_document PID 
                        inner join 
                            pre_ins_doc_definition PDD 
                        on 
                            PID.id_status = PDD.id 
                        inner join 
                            technical_career TC 
                        on PID.idTecCareer = TC.id 
                        WHERE 
                              PID.dni = _dni 
                          and PID.idtecCareer = _idTec
                        UNION
                        SELECT PID.id, 
                             PID.doc_description, 
                             PID.dni, 
                             PDD.description, 
                             PID.idTecCareer, 
                             TC.description as Description
                        FROM 
                            pre_inscription_document PID 
                        inner join 
                            pre_ins_doc_definition PDD 
                        on 
                            PID.id_status = PDD.id 
                        inner join 
                            technical_career TC 
                        on PID.idTecCareer = TC.id 
                        WHERE 
                              PID.dni = _dni 
                           and doctype in ("C");
	
END$$

CREATE DEFINER=`db_alumno`@`localhost` PROCEDURE `sp_pre_inscription` (IN `_User` INT)  SQL SECURITY INVOKER
    COMMENT 'SP select pre inscripciones'
BEGIN	

	set @value := (select if(idrol = 1, "true","false") from user  where dni= _User);		
    
if(@value = "true")then
	 select PI.last_name, PI.name , PI.address , PI.dni , PI.email , PI.phone_number , PI.status , PI.download , PI.technical_career , TC.description from pre_inscription PI inner join db_alumno.technical_career TC on PI.technical_career = TC.id ;
else	
	 select PI.last_name, PI.name , PI.address , PI.dni , PI.email , PI.phone_number , PI.status , PI.download , PI.technical_career , TC.description from pre_inscription PI inner join db_alumno.technical_career TC on PI.technical_career = TC.id  where PI.action = 'A';	end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_save_preinscription` (IN `_last_name` VARCHAR(50), IN `_name` VARCHAR(50), IN `_address` VARCHAR(50), IN `_dni` INT, IN `_email` VARCHAR(100), IN `_phone_number` VARCHAR(50), IN `_status` VARCHAR(6), IN `_technical_career` VARCHAR(100), IN `_path` VARCHAR(100), IN `_action` VARCHAR(1))  BEGIN
	INSERT INTO pre_inscription (last_name , name , address , dni , email , Phone_number, status, technical_career, path, `action` ) 
                         VALUES ( _last_name , _name , _address , _dni , _email , _phone_number, _status, _technical_career, _path, _action );
end$$

CREATE DEFINER=`db_alumno`@`localhost` PROCEDURE `sp_save_pre_inscription_2` (IN `_dni` INT, IN `_idTec` VARCHAR(1))  SQL SECURITY INVOKER
BEGIN	

	
    INSERT INTO pre_inscription ( last_name, name, address, dni, email, phone_number, status, technical_career, path, action )
                        SELECT distinct last_name, name ,address ,dni , email, phone_number, '--', _idTec, path, 'A' FROM pre_inscription WHERE dni = _dni;
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_save_pre_inscription_document` (IN `_dni` INT, IN `_doc_description` VARCHAR(100), IN `_id_status` INT, IN `_idTecCareer` VARCHAR(1), IN `_doctype` VARCHAR(1))  BEGIN
	INSERT INTO pre_inscription_document  (dni, doc_description , id_status, idTecCareer, doctype ) 
                         VALUES ( _dni, _doc_description, _id_status, _idTecCareer, _doctype  );   
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_save_pre_inscription_document2` (IN `_dni` INT, IN `_doc_description` VARCHAR(100), IN `_doc_description2` VARCHAR(100), IN `_id_status` INT, IN `_idTecCareer` VARCHAR(1), IN `_doctype` VARCHAR(1))  BEGIN
	INSERT INTO pre_inscription_document  (dni, doc_description , id_status, idTecCareer, doctype ) 
                         VALUES ( _dni, _doc_description, _id_status, _idTecCareer, _doctype  );   
                        
    INSERT INTO pre_inscription_document  (dni, doc_description , id_status, idTecCareer, doctype ) 
                         VALUES ( _dni, _doc_description2, _id_status, _idTecCareer, _doctype  );                          
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_pre_inscription_document` (IN `_dni` INT, IN `_idTecCareer` VARCHAR(1))  BEGIN
	    update  pre_inscription_document set docType = 'U' where dni  = _dni and doc_description like '%ABC-DNI%'  and idTecCareer = _idTecCareer and docType ='C';
     	update  pre_inscription_document set docType = 'U' where dni  = _dni and doc_description like '%Inscripcion-DNI%'  and idTecCareer = _idTecCareer and docType ='C';
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_validate_dni` (IN `_dni` INT)  BEGIN
	select * from db_alumno.pre_inscription where dni = _dni;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anuncios`
--

CREATE TABLE `anuncios` (
  `Id_Anuncio` int(11) NOT NULL,
  `Titulo` varchar(150) DEFAULT NULL,
  `Descripcion` varchar(500) DEFAULT NULL,
  `Fecha_Inicio` date DEFAULT NULL,
  `Fecha_Fin` date DEFAULT NULL,
  `Prioridad` int(11) DEFAULT NULL,
  `Link` varchar(250) DEFAULT NULL,
  `Nombre_Link` varchar(100) DEFAULT NULL,
  `Imagen` varchar(200) DEFAULT NULL,
  `Fecha_Alta` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `anuncios`
--

INSERT INTO `anuncios` (`Id_Anuncio`, `Titulo`, `Descripcion`, `Fecha_Inicio`, `Fecha_Fin`, `Prioridad`, `Link`, `Nombre_Link`, `Imagen`, `Fecha_Alta`) VALUES
(17, 'CFP 401 - San José', 'Novedades e información general sobre el C.F.P. Nº 401 \"San José\" de Libertad', '2020-10-11', '2025-01-01', 1, 'https://www.facebook.com/CFP401Libertad', 'Visitar', '106505012_3660975883919049_2512804267133708731_n.png', '2020-10-11 14:55:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document_type`
--

CREATE TABLE `document_type` (
  `type` varchar(1) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `document_type`
--

INSERT INTO `document_type` (`type`, `description`) VALUES
('C', 'Común para la tecnicatura'),
('U', 'Único para la tecnicatura');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `module`
--

INSERT INTO `module` (`id`, `description`) VALUES
(1, 'Pre-Inscripcion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE `novedades` (
  `Id_Novedad` int(11) NOT NULL,
  `Titulo` varchar(150) DEFAULT NULL,
  `Descripcion` text DEFAULT NULL,
  `Fecha_Inicio` date DEFAULT NULL,
  `Fecha_Fin` date DEFAULT NULL,
  `Prioridad` int(11) DEFAULT NULL,
  `Link` varchar(250) DEFAULT NULL,
  `Nombre_Link` varchar(100) DEFAULT NULL,
  `Imagen` varchar(200) DEFAULT NULL,
  `Fecha_Alta` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`Id_Novedad`, `Titulo`, `Descripcion`, `Fecha_Inicio`, `Fecha_Fin`, `Prioridad`, `Link`, `Nombre_Link`, `Imagen`, `Fecha_Alta`) VALUES
(1, 'Suspensión de actividades por COVID-19.', 'Se han suspendido las actividades presenciales. Si eres un alumno inscrito ingresa al campus virtual.', '2020-08-24', '2021-05-01', 1, 'https://campus.isft177.edu.ar/', 'Campus', 'covid_recomendaciones.jpg', '2020-08-17 22:04:50'),
(3, 'Informes e inscripción.', 'Lunes a Viernes de 18:30 hs a 22:00 hs. Se han suspendido las actividades presenciales. Si eres un alumno inscrito ingresa al campus virtual.', '2020-09-28', '2025-01-01', 1, '', '', '', '2020-09-28 19:42:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operation`
--

CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `idmodule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `operation`
--

INSERT INTO `operation` (`id`, `description`, `idmodule`) VALUES
(1, 'Create', 1),
(2, 'Read', 1),
(3, 'Update', 1),
(4, 'Delete', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preinscrition_config`
--

CREATE TABLE `preinscrition_config` (
  `id` int(11) NOT NULL,
  `begin_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preinscrition_config`
--

INSERT INTO `preinscrition_config` (`id`, `begin_date`, `end_date`, `status`) VALUES
(1, '2020-11-05', '2020-11-05', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_inscription`
--

CREATE TABLE `pre_inscription` (
  `last_name` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `dni` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `status` varchar(7) DEFAULT NULL,
  `download` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `technical_career` int(11) NOT NULL,
  `path` varchar(100) DEFAULT NULL,
  `action` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pre_inscription`
--

INSERT INTO `pre_inscription` (`last_name`, `name`, `address`, `dni`, `email`, `phone_number`, `status`, `download`, `technical_career`, `path`, `action`) VALUES
('Martinez', 'Fabricio', 'deseado 3245', 33186495, 'fabricio-martinez@hotmail.com.ar', '1141963213', 'NOT OK', '2020-10-11 15:54:35', 1, '../public/uploadFiles/33186495/', 'A'),
('Galeano', 'Joaquin Ezequiel', 'Whatever 123', 39389129, 'joa.cavs@gmail.com', '1157653392', 'OK', '2020-10-11 15:43:58', 1, '../public/uploadFiles/39389129/', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_inscription_document`
--

CREATE TABLE `pre_inscription_document` (
  `id` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `doc_description` varchar(100) NOT NULL,
  `download` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_status` int(11) DEFAULT NULL,
  `idTecCareer` int(11) NOT NULL,
  `docType` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pre_inscription_document`
--

INSERT INTO `pre_inscription_document` (`id`, `dni`, `doc_description`, `download`, `id_status`, `idTecCareer`, `docType`) VALUES
(685, 39389129, 'DNI-39389129-DNI-497653047-Archivo1.pdf', '2020-10-11 15:43:48', 1, 1, 'C'),
(686, 39389129, 'Foto-DNI-39389129-Foto-DNI-497653047-logo_progresar.png', '2020-10-11 15:43:50', 1, 1, 'C'),
(687, 39389129, 'Titulo-DNI-39389129-Reglamento-DNI-497653047-Archivo3.pdf', '2020-10-11 15:43:51', 1, 1, 'C'),
(688, 39389129, 'Inscripcion-DNI-39389129-PNac-DNI-497653047-Archivo4.pdf', '2020-10-11 15:43:53', 1, 1, 'U'),
(689, 39389129, 'ABC-DNI-39389129-ABC-DNI-497653047-Archivo 2.pdf', '2020-10-11 15:43:54', 1, 1, 'U'),
(690, 39389129, 'Reglamento-DNI-39389129-Titulo-DNI-497653047-Archivo 2.pdf', '2020-10-11 15:43:56', 1, 1, 'C'),
(691, 39389129, 'PNac-DNI-39389129-Titulo-DNI-497653047-Archivo 2.pdf', '2020-10-11 15:43:56', 1, 1, 'C'),
(692, 39389129, 'LS-DNI-39389129-Inscripcion-DNI-497653047-Archivo1.pdf', '2020-10-11 15:43:58', 1, 1, 'U'),
(735, 33186495, 'PNac-DNI-33186495-4 - copia (3).pdf', '2020-10-11 15:54:35', 1, 1, 'C'),
(736, 33186495, 'Reglamento-DNI-33186495-7 - copia (6).pdf', '2020-10-12 23:36:13', 2, 1, 'C'),
(737, 33186495, 'ABC-DNI-33186495-1.pdf', '2020-10-11 15:47:30', 3, 1, 'U'),
(738, 33186495, 'LS-DNI-33186495-3 - copia (2).pdf', '2020-10-11 15:47:30', 3, 1, 'C'),
(739, 33186495, 'DNI-33186495-2 - copia.pdf', '2020-10-11 15:47:30', 3, 1, 'C'),
(740, 33186495, 'Foto-DNI-33186495-images.jpg', '2020-10-11 15:47:30', 3, 1, 'C'),
(741, 33186495, 'Inscripcion-DNI-33186495-6 - copia (5).pdf', '2020-10-11 15:47:30', 3, 1, 'U'),
(742, 33186495, 'Titulo-DNI-33186495-5- copia (4).pdf', '2020-10-11 15:47:30', 3, 1, 'C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_ins_doc_definition`
--

CREATE TABLE `pre_ins_doc_definition` (
  `id` int(11) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pre_ins_doc_definition`
--

INSERT INTO `pre_ins_doc_definition` (`id`, `description`) VALUES
(1, 'OK'),
(2, 'NOT_OK'),
(3, '--');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `description`) VALUES
(1, 'Administrator'),
(2, 'Preceptor'),
(3, 'Alumno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_operation`
--

CREATE TABLE `rol_operation` (
  `id` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idoperation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol_operation`
--

INSERT INTO `rol_operation` (`id`, `idrol`, `idoperation`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 2, 1),
(6, 2, 2),
(7, 2, 3),
(8, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `technical_career`
--

CREATE TABLE `technical_career` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `technical_career`
--

INSERT INTO `technical_career` (`id`, `description`) VALUES
(1, 'Tecnicatura Superior en Análisis de Sistemas'),
(2, 'Tecnicatura Superior en Laboratorio de Análisis Clínicos'),
(3, 'Tecnicatura Superior en Administración Contable'),
(4, 'Tecnicatura Superior en Gestión Ambiental y Salud');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `dni` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` char(60) NOT NULL,
  `creationdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `idrol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `dni`, `email`, `password`, `creationdate`, `idrol`) VALUES
(6, 'admin', 29863127, 'ebubello@gmail.com', '$2y$10$xLFSZgtZCb1He./9VAImzuaEaskdDK85IIEEa128VXEg/HvtE4u9G', '2020-09-15 00:25:01', 1),
(38, 'Guest', 22222222, 'jajajaja@gmail.com', '$2y$10$5dkT56dMCMCV.7LibZrpmeB4BCWp.pIuqiyeLBHmu2GzRC4s0mp82', '2020-09-19 21:27:15', 2),
(39, 'admin', 99999999, 'isft.177.prueba@gmail.com', '$2y$10$I5XeOz6/cSwdGT5jGWn.Pu5K1hIOhHYE4zyiXpgmTMmQuhdA.YqOG', '2020-09-28 23:49:20', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anuncios`
--
ALTER TABLE `anuncios`
  ADD PRIMARY KEY (`Id_Anuncio`);

--
-- Indices de la tabla `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`type`);

--
-- Indices de la tabla `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `novedades`
--
ALTER TABLE `novedades`
  ADD PRIMARY KEY (`Id_Novedad`);

--
-- Indices de la tabla `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_operation_module` (`idmodule`);

--
-- Indices de la tabla `preinscrition_config`
--
ALTER TABLE `preinscrition_config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pre_inscription`
--
ALTER TABLE `pre_inscription`
  ADD PRIMARY KEY (`dni`,`email`,`technical_career`) USING BTREE;

--
-- Indices de la tabla `pre_inscription_document`
--
ALTER TABLE `pre_inscription_document`
  ADD PRIMARY KEY (`id`,`dni`,`idTecCareer`),
  ADD KEY `FK_pre_inscription_document_pre_ins_doc_definition` (`id_status`),
  ADD KEY `FK_pre_inscription_document_technical_career` (`idTecCareer`),
  ADD KEY `FK_pre_inscription_document_document_type` (`docType`);

--
-- Indices de la tabla `pre_ins_doc_definition`
--
ALTER TABLE `pre_ins_doc_definition`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol_operation`
--
ALTER TABLE `rol_operation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_rol_operation_operation` (`idoperation`),
  ADD KEY `FK_rol_operation_rol` (`idrol`);

--
-- Indices de la tabla `technical_career`
--
ALTER TABLE `technical_career`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`dni`,`email`),
  ADD KEY `FK_user_rol` (`idrol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anuncios`
--
ALTER TABLE `anuncios`
  MODIFY `Id_Anuncio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `novedades`
--
ALTER TABLE `novedades`
  MODIFY `Id_Novedad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pre_inscription_document`
--
ALTER TABLE `pre_inscription_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=784;

--
-- AUTO_INCREMENT de la tabla `pre_ins_doc_definition`
--
ALTER TABLE `pre_ins_doc_definition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rol_operation`
--
ALTER TABLE `rol_operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `technical_career`
--
ALTER TABLE `technical_career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `operation`
--
ALTER TABLE `operation`
  ADD CONSTRAINT `FK_operation_module` FOREIGN KEY (`idmodule`) REFERENCES `module` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pre_inscription_document`
--
ALTER TABLE `pre_inscription_document`
  ADD CONSTRAINT `FK_pre_inscription_document_document_type` FOREIGN KEY (`docType`) REFERENCES `document_type` (`type`),
  ADD CONSTRAINT `FK_pre_inscription_document_pre_ins_doc_definition` FOREIGN KEY (`id_status`) REFERENCES `pre_ins_doc_definition` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_pre_inscription_document_technical_career` FOREIGN KEY (`idTecCareer`) REFERENCES `technical_career` (`id`);

--
-- Filtros para la tabla `rol_operation`
--
ALTER TABLE `rol_operation`
  ADD CONSTRAINT `FK_rol_operation_operation` FOREIGN KEY (`idoperation`) REFERENCES `operation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rol_operation_rol` FOREIGN KEY (`idrol`) REFERENCES `rol` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_rol` FOREIGN KEY (`idrol`) REFERENCES `rol` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
