﻿                    Comando básicos para trabajar con repositorios GIT

******PASOS PARA TRASLADAR UN REPOSITORIO LOCAL A GITLAB******

1: Crear repositirio en GITLAB

2: Crear carpeta local (en nuestra computadora) la cual pretendemos que se conecte con GITLAB

3: Click derecho dentro de la carpeta y elegir la opcion "Git Bash Here"

***COMANDOS EN CONSOLA GIT***

##SI ES LA PRIMERA VEZ QUE SE INGRESA A LA CONSOLA:##

1-> Para establecer nombre de usuario de manera global

    git config --global user.name "nombre_de_usuario_gitlab"

2-> Para establecer correo de manera global
    
    git config --global user.email "cuenta_de_correo_gitlab"

3-> Para verificar los valores definidos
    
    git config --global --list

######################################################

4-> Generar un repositorio de manera local 
    
    git init

5-> Para definir el repositorio remoto generado en GitLab 
    
    git remote add origin https://gitlab.com/lcsgimenez017/gitlab.git

6->Para descargar/actualizar el repositorio remoto al local 
    
    git pull origin master

7->Para subir cambios
 7.1-> agregar los archivos que se cargaran (se utiliza "." para agragar a todos los archivos)    
    git add .
 7.2-> Generar un nombre para el commit que esta por realizarse
    git commit -m "first commit"
 7.2-> Ejecutar push para finalizar la carga de los archivos locales
    git push -u origin master


******COMANDOS QUE PUEDEN SER UTILES UTILES*****************
->Para verificar archivos pendientes de agregar o cargar al repositorio remoto

    git status

->En caso de no poder cargar los cambios por porblemas con SSL

    git config --global http.sslVerify false

->En caso de querer eliminar un repositirio local probar con alguna de estas opciones
    rm -rf .git
    find . -type f | grep -i "\.git" | xargs rm

******BUENAS PRACTICAS PARA TRABAJAR CON VSCODE Y GIT*******

Detalle: 
Al momento de ejecutar la instruccion git init en VSCODE nos detallara las diferencias/cambios 
en nuestros archivos utilizando como comparación la ultima versión guardada del repositio 
LOCAL (por lo que es importante tener la version actualizada del repositirio).

Se recomienda respetar los siguientes pasos : 

1.Comprimir una copia de nuestro proyecto local antes de comenzar.

2.Generar una copia del proyecto local (carpeta) y darle al nombre una referencia por ejemplo : mismonommbre_GIT ,
pues sera en esta ultima en la que crearemos un repositorio local para poder descargarnos el repositorio remoto y mantenerlo actualizado.

3. Trabajar y realizar los cambios que correspondan sobre los archivos de nuestro proyecto ORIGINAL.

4. Actualizar repositorio local (carpeta del paso 2) para asegurarnos que trabajamos sobre la ultima version.

5. Remplazar los archivos del repositorio local con los archivos que modifiquemos en nuestro proyecto ORIGINAL .

6. Abrir repositorio local en VSCode donde verificaremos todos los cambios/diferencias en nuestros archivos los cuales estan pendiente a ser 
agregados al repositorio (add .) . Revisar con precaucion los archivos modificados y tener en cuenta que el codigo que no este en nuestra versión
significa que es codigo desarrollado por otro participante y podriamos afectar su trabajo teniendo en cuenta que cargariamos el archivo sin ese codigo,
lo correcto sería contemplarlo y asegurarnos de que quede en el archivo.