/******************JQuery DataTable****************************** */

$(document).ready(function() {
    $('#tableBTS4').DataTable({
        "destroy": true,
        "ajax": {
            "url": "../Dashboard/viewPreInsc",
            "dataSrc": ""
        },
        "columns": [
            { "data": "last_name" },
            { "data": "name" },
            { "data": "address" },
            { "data": "dni" },
            { "data": "email" },
            { "data": "phone_number" },
            { "data": "status" },
            { "data": "download" },
            { "data": "technical_career" },
            { "data": "description" },
            { "defaultContent": "<a id='Ver' class='btn btn-info btn-sm' title='Ver Documentos' href='../Dashboard/dashPreInsDoc/' role='button'><i class='far fa-eye'></i></a><a id='eliminar' data-toggle='modal' href='#deleteModal' class='btn btn-danger btn-sm' title='Eliminar' href='#' role='button'><i class='far fa-trash-alt'></i></a>" },

        ],
        "autoWidth": false,
        "language": idioma_es,
        "columnDefs": [{
                "targets": [8],
                "visible": false,
                "searchable": false
            },
            {
                "targets": 'hide_column',
                "visible": false
            }
        ]


    });

    // tabla = $('#tableBTS4').DataTable();
    // tabla.columns([8]).visible(false);

    $("#tableBTS4").on('click', '.btn', function() {
        // get the current row
        var currentRow = $(this).closest("tr");

        var col1 = currentRow.find("td:eq(3)").text(); // get current row 1st table cell TD value
        var col2 = currentRow.find("td:eq(8)").text();

        switch (col2) {
            case 'Tecnicatura Superior en Análisis de Sistemas':
                col2 = 1;
                break;
            case 'Tecnicatura Superior en Laboratorio de Análisis Clínicos':
                col2 = 2;
                break;
            case 'Tecnicatura Superior en Administración Contable':
                col2 = 3;
                break;
            case 'Tecnicatura Superior en Gestión Ambiental y Salud':
                col2 = 4;
                break;
        }

        $("a#Ver").attr("href", "../Dashboard/dashPreInsDoc/" + col1 + "/" + col2);
        $("a#deletePre").attr("href", "../Dashboard/deletePreIns/" + col1 + "/" + col2);
    });




});




/****************** FIN JQuery DataTable****************************** */





let idioma_es = {
    "emptyTable": "No hay datos disponibles en la tabla.",
    "info": "Del _START_ al _END_ de _TOTAL_ ",
    "infoEmpty": "Mostrando 0 registros de un total de 0.",
    "infoFiltered": "(filtrados de un total de _MAX_ registros)",
    "infoPostFix": "(actualizados)",
    "lengthMenu": "Mostrar _MENU_ registros",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "Buscar:",
    "searchPlaceholder": "Dato para buscar",
    "zeroRecords": "No se han encontrado coincidencias.",
    "paginate": {
        "first": "Primera",
        "last": "Última",
        "next": "Siguiente",
        "previous": "Anterior"
    },
    "aria": {
        "sortAscending": "Ordenación ascendente",
        "sortDescending": "Ordenación descendente"
    }
}




$(document).ready(function() {
    $("#activarDNI").click(function() {
        let value = $('#usr').val();
        location.href = '../Dashboard/activePreIns/' + value;
    });

});