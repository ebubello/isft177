// Inicio Alertas Dashboard

$("#activarDNI").click(function() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'PreInscripción Activada',
        text: 'Ya se encuentra disponible para los preceptores.',
        showConfirmButton: false,
        timer: 4000
    })
});



$("#activarPI").click(function() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'PreInscripción Habilitada',
        text: 'Los futuros alumnos ya puede realizar la PreInscripción',
        showConfirmButton: false,
        timer: 4000
    })
});


$("#deletePre").click(function() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'PreInscripción Eliminada',
        showConfirmButton: false,
        timer: 4000
    })
});


$("#desactivarPI").click(function() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'PreInscripción Desactivada!!!',
        showConfirmButton: false,
        timer: 4000
    })
});

//Fin Alertas Dashboard