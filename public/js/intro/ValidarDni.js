let formulario = document.getElementById('formulario');
let respuesta = document.getElementById('respuesta');
let dni = document.getElementById('dni');
let validar = document.getElementById('btnFetch');


formulario.addEventListener('submit', function(e) {
    e.preventDefault();

    validar.innerHTML = `<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Validando...`;

    var datos = new FormData(formulario);


    fetch("../preInscripcion/dni", {
            method: 'POST',
            body: datos
        })
        .then(res => res.json())
        .then(data => {

            if (data['result'] === 3) {

                window.scrollTo(0, 0);

                dni.value = '';

                validar.innerHTML = `Confirmar Identidad `;

                respuesta.innerHTML = `
                                         <div class="alert alert-danger" role="alert">
                        Ya cuentas con tres preinscripciónes en este instituto.
                        Ante cualquier duda, puedes utilizar el chat del sitio.
                                        </div>
                                        `

            } else if (data['result'] == 0) {

                let dato = document.getElementById("dni").value;
                localStorage.setItem("dato", dato);
                window.location.href = "../preInscripcion/preInscripcion";

            } else {

                $('#Modalpreguntar').modal('show');

                let dato = document.getElementById("dni").value;
                document.getElementById("dni2").value = dato;


                let row = data['result2'];
                for (let i = 0, len = row.length; i < len; i++) {

                    document.getElementById("sel1").innerHTML += `<option value="${row[i].id}">${row[i].description}</option>`;
                }


                $('#si').onclick(function() {
                    $('#ModalMasPreInsc').modal('show');
                });
            }
        })
        .catch((error) => {
            console.log(error)
        });
});