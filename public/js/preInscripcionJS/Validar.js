window.addEventListener('load', validarForm, false);

function validarForm() {
    /***Step 2 */
    document.getElementById('InputFile-foto').addEventListener('change', validarFoto, false);
    document.getElementById('tecnicatura').addEventListener('change', validarTecnicatura, false);
    /***Step 3 */
    document.getElementById('apellido').addEventListener('change', validarApellido, false);
    document.getElementById('nombre').addEventListener('change', validarNombre, false);
    document.getElementById('direccion').addEventListener('change', validarDireccion, false);
    // document.getElementById('dni').addEventListener('change', validarDNI, false);
    document.getElementById('email').addEventListener('change', validarEmail, false);
    document.getElementById('telefono').addEventListener('keypress', validarTelefono, false);
    /***Step 4 */
    document.getElementById('InputFile-ABC').addEventListener('change', validarABC, false);
    document.getElementById('InputFile-DNI').addEventListener('change', validarConstDNI, false);
    // document.getElementById('InputFile-LS').addEventListener('change', validarLS, false);
    document.getElementById('InputFile-PNac').addEventListener('change', validarPNac, false);
    /***Step 4 */
    document.getElementById('InputFile-Titulo').addEventListener('change', validarTitulo, false);
    document.getElementById('InputFile-inscripcion').addEventListener('change', validarInscripcion, false);
    document.getElementById('InputFile-Reglamento').addEventListener('change', validarReglamento, false);

    //document.getElementById('enviarForm').addEventListener('click', validar, false);
}


function validarFoto() {

    document.getElementById('error').style.display = "none";

    let valInputFile_foto = document.getElementById('InputFile-foto').value;
    let valTecnicatura = document.getElementById('tecnicatura').value;

    let validar = [];

    let valNext1 = document.getElementById('next1');


    if (valInputFile_foto == 0 || valInputFile_foto.length == 0) {
        document.getElementById('error').focus();
        validar.push("<strong>Paso 2 - Foto 4 x 4: </strong> Seleccione una imagen para su perfil");
    }

    document.getElementById('tecnicatura').focus();

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valTecnicatura != 'Seleccione...') {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }
}

function validarTecnicatura() {


    document.getElementById('error').style.display = "none";

    let valInputFile_foto = document.getElementById('InputFile-foto').value;
    let valTecnicatura = document.getElementById('tecnicatura').value;

    let validar = [];

    let valNext1 = document.getElementById('next1');


    if (valTecnicatura == 'Seleccione...') {
        validar.push("<strong>Paso 2 - Foto 4 x 4: </strong> Debe seleccionar una tecnicatura.");
        document.getElementById('tecnicatura').focus();
    } else if (valInputFile_foto.length == 0) {
        validar.push("<strong>Paso 2 - Tecnicatura: </strong> Seleccione una imagen para su perfil.");
    }

    document.getElementById('tecnicatura').focus();

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valTecnicatura != 'Seleccione...') {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

function validarApellido() {
    const patternApellido = /^[a-zA-Z\s]/;

    let valApellido = document.getElementById('apellido').value;
    let valNombre = document.getElementById('nombre').value;
    let validarDireccion = document.getElementById('direccion').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarTelefono = document.getElementById('telefono').value;

    let validar = [];

    let valNext1 = document.getElementById('next2');

    /**************************Validar Apellido*******************************************/

    if (valApellido == null || valApellido.length == 0 || valApellido == '') {
        document.getElementById('apellido').focus();
        validar.push("<strong>Paso 3 - Importante:</strong> El campo <strong>Apellido(s)</strong> no debe ir vacío o con espacios en blanco.<br>");
        window.scrollTo(0, 0);

    } else if (patternApellido.test(valApellido) == false) {
        document.getElementById('apellido').focus();
        validar.push("<strong>Paso 3 - Apellido(s):</strong> Solo se permiten caracteres A a Z Mínimo (3) Máximo (24).<br>");
        window.scrollTo(0, 0);

    }


    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valNombre.length != 0 && valApellido.length != 0 && validarDireccion.length != 0 && validarDNI.length != 0 && validarEmail.length != 0 && validarTelefono.length != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

function validarNombre() {
    const patternNombre = /^[A-Za-z]{3,}/;

    let valApellido = document.getElementById('apellido').value;
    let valNombre = document.getElementById('nombre').value;
    let validarDireccion = document.getElementById('direccion').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarTelefono = document.getElementById('telefono').value;

    let validar = [];

    let valNext1 = document.getElementById('next2');

    /**************************Validar Nombre*******************************************/

    if (valNombre == null || valNombre.length == 0 || valNombre == '') {
        document.getElementById('nombre').focus();
        validar.push("<strong>Paso 3 - Importante:</strong> El campo <strong>nombre</strong> no debe ir vacío o con espacios en blanco.<br>");
        window.scrollTo(0, 0);
    } else if (patternNombre.test(valNombre) == false) {
        document.getElementById('nombre').focus();
        validar.push("<strong>Paso 3 - Nombre(s):</strong> Solo se permiten caracteres A a Z Mínimo (3) Máximo (24).<br>");
        window.scrollTo(0, 0);
    }


    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valNombre.length != 0 && valApellido.length != 0 && validarDireccion.length != 0 && validarDNI.length != 0 && validarEmail.length != 0 && validarTelefono.length != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

function validarDireccion() {
    const patternDireccion = /^[A-Za-z0-9]{3,}/;
    let valApellido = document.getElementById('apellido').value;
    let valNombre = document.getElementById('nombre').value;
    let validarDireccion = document.getElementById('direccion').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarTelefono = document.getElementById('telefono').value;

    let validar = [];

    let valNext1 = document.getElementById('next2');

    /**************************Validar Dirección*******************************************/

    if (validarDireccion == null || validarDireccion.length == 0 || validarDireccion == '') {
        document.getElementById('direccion').focus();
        validar.push("<strong>Paso 3 - Importante:</strong> El campo <strong>Dirección</strong> no debe ir vacío.<br>");
        window.scrollTo(0, 0);

    } else if (patternDireccion.test(validarDireccion) == false) {
        document.getElementById('direccion').focus();
        validar.push("<strong>Paso 3 - Dirección:</strong> Solo se permiten caracteres A a Z  y Números Mínimo (3) Máximo (24).<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valNombre.length != 0 && valApellido.length != 0 && validarDireccion.length != 0 && validarDNI.length != 0 && validarEmail.length != 0 && validarTelefono.length != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }
}

function validarDNI() {
    const patternDNI = /^[0-9]{8,8}/;
    let valApellido = document.getElementById('apellido').value;
    let valNombre = document.getElementById('nombre').value;
    let validarDireccion = document.getElementById('direccion').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarTelefono = document.getElementById('telefono').value;

    let validar = [];

    let valNext1 = document.getElementById('next2');
    /**************************Validar DNI**************************************************/

    if (validarDNI == null || validarDNI.length == 0 || validarDNI == '') {
        document.getElementById('dni').focus();
        validar.push("<strong>Paso 3 - Importante:</strong> El campo <strong>DNI</strong> no debe ir vacío.<br>");
        window.scrollTo(0, 0);
    } else if (patternDNI.test(validarDNI) == false) {
        document.getElementById('dni').focus();
        validar.push("<strong>Paso 3 - DNI:</strong> Solo se permiten caracteres numericos.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valNombre.length != 0 && valApellido.length != 0 && validarDireccion.length != 0 && validarDNI.length != 0 && validarEmail.length != 0 && validarTelefono.length != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

function validarEmail() {
    const patternEmail = /[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}/;

    let valApellido = document.getElementById('apellido').value;
    let valNombre = document.getElementById('nombre').value;
    let validarDireccion = document.getElementById('direccion').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarTelefono = document.getElementById('telefono').value;
    let validar = [];
    let valNext1 = document.getElementById('next2');

    /**************************Validar Email**************************************************/

    if (validarEmail == null || validarEmail.length == 0 || validarEmail == '') {
        document.getElementById('email').focus();
        validar.push("<strong>Paso 3 - Importante:</strong> El campo <strong>Email</strong> no debe ir vacío.<br>");
        window.scrollTo(0, 0);

    } else if (patternEmail.test(validarEmail) == false) {
        document.getElementById('email').focus();
        validar.push("<strong>Paso 3 - Email:</strong> Ingrese un Email valido (Los caracteres especiales no estan permitidos).<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valNombre.length != 0 && valApellido.length != 0 && validarDireccion.length != 0 && validarDNI.length != 0 && validarEmail.length != 0 && validarTelefono.length != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }
}

function validarTelefono() {
    const patternTelefono = /[0-9]{8,12}/;

    let valApellido = document.getElementById('apellido').value;
    let valNombre = document.getElementById('nombre').value;
    let validarDireccion = document.getElementById('direccion').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarTelefono = document.getElementById('telefono').value;

    let validar = [];

    let valNext1 = document.getElementById('next2');


    /**************************Validar Telefono**************************************************/
    //validarTelefono == null || validarTelefono.length == 0 ||
    if (validarTelefono == null || validarTelefono.length == 0 || validarTelefono == '') {
        document.getElementById('telefono').focus();
        validar.push("<strong>Paso 3 - Importante:</strong> El campo <strong>Telefono</strong> no debe ir vacío.<br>");
        window.scrollTo(0, 0);

    } else if (patternTelefono.test(validarTelefono) == false) {
        document.getElementById('telefono').focus();
        validar.push("<strong>Paso 3 - Telefono:</strong> Ingrese un numero Telefono valido (Los caracteres especiales y letras no estan permitidos.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;
    } else if (valNombre.length != 0 && valApellido.length != 0 && validarDireccion.length != 0 && validarDNI.length != 0 && validarEmail.length != 0 && validarTelefono.length != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

function validarABC() {
    let InputFileABC = document.getElementById('InputFile-ABC').value;
    let InputFileDNI = document.getElementById('InputFile-DNI').value;
    // let InputFileLS = document.getElementById('InputFile-LS').value;
    let InputFilePNac = document.getElementById('InputFile-PNac').value;

    let validar = [];

    let valNext1 = document.getElementById('next3');

    /**************************Validar Constancia incripcion ABC*******************************************/
    if (InputFileABC == '') {
        validar.push("<strong>Paso 4 - Importante:</strong> Seleccione constancia de inscripcion ABC.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;

    } else if (InputFileABC != 0 && InputFileDNI != 0 && InputFilePNac != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

function validarConstDNI() {
    let InputFileABC = document.getElementById('InputFile-ABC').value;
    let InputFileDNI = document.getElementById('InputFile-DNI').value;
    //let InputFileLS = document.getElementById('InputFile-LS').value;
    let InputFilePNac = document.getElementById('InputFile-PNac').value;

    let validar = [];

    let valNext1 = document.getElementById('next3');

    /**************************Validar Constancia DNI*******************************************/
    if (InputFileDNI == '') {
        validar.push("<strong>Paso 4 - Importante:</strong> Seleccione constancia de DNI.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;

    } else if (InputFileABC != 0 && InputFileDNI != 0 && InputFilePNac != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

// function validarLS() {
//     let InputFileABC = document.getElementById('InputFile-ABC').value;
//     let InputFileDNI = document.getElementById('InputFile-DNI').value;
//     let InputFileLS = document.getElementById('InputFile-LS').value;
//     let InputFilePNac = document.getElementById('InputFile-PNac').value;

//     let validar = [];

//     let valNext1 = document.getElementById('next3');

//     /**************************Validar Libreta Sanitaria*******************************************/
//     if (InputFileLS == '') {
//         validar.push("<strong>Paso 4 - Importante:</strong> Seleccione constancia de libreta sanitaria.<br>");
//         window.scrollTo(0, 0);
//     }

//     if (validar.length > 0) {
//         document.getElementById('error').innerHTML = validar;
//         document.getElementById('error').style.display = "block";
//         validar = [];
//         valNext1.disabled = true;

//     } else if (InputFileABC != 0 && InputFileDNI != 0 && InputFileLS != 0 && InputFilePNac != 0) {
//         document.getElementById('error').style.display = "none";

//         valNext1.disabled = false;
//     }

// }

function validarPNac() {
    let InputFileABC = document.getElementById('InputFile-ABC').value;
    let InputFileDNI = document.getElementById('InputFile-DNI').value;
    //let InputFileLS = document.getElementById('InputFile-LS').value;
    let InputFilePNac = document.getElementById('InputFile-PNac').value;

    let validar = [];

    let valNext1 = document.getElementById('next3');

    /**************************Validar Constancia Partida de Nacimiento*******************************************/
    if (InputFilePNac == '') {
        validar.push("<strong>Paso 4  - Importante:</strong> Seleccione constancia de partida de nacimiento.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        valNext1.disabled = true;

    } else if (InputFileABC != 0 && InputFileDNI != 0 && InputFilePNac != 0) { // && InputFileLS != 0 Comentado para hacer opcional el input.
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}

function validarTitulo() {
    let InputFileTitulo = document.getElementById('InputFile-Titulo').value;
    let InputFileReglamento = document.getElementById('InputFile-Reglamento').value;
    let InputFileInscripcion = document.getElementById('InputFile-inscripcion').value;

    validar = [];

    let valNext1 = document.getElementById('enviarForm');

    /**************************Validar Constancia de Titulo*******************************************/
    if (InputFileTitulo == 0 || InputFileTitulo.length == 0) {
        validar.push("<strong>Paso 4 - Importante:</strong>Adjunte titulo secundario o constancia en tramite.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').style.display = "block";
        document.getElementById('error').innerHTML = validar;
        validar = [];
        valNext1.disabled = true;

    } else if (InputFileTitulo != 0 && InputFileReglamento != 0 && InputFileInscripcion != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }
}

function validarInscripcion() {
    let InputFileTitulo = document.getElementById('InputFile-Titulo').value;
    let InputFileReglamento = document.getElementById('InputFile-Reglamento').value;
    let InputFileInscripcion = document.getElementById('InputFile-inscripcion').value;

    validar = [];

    let valNext1 = document.getElementById('enviarForm');

    /**************************Validar ficha de inscripción*******************************************/
    if (InputFileInscripcion == 0 || InputFileInscripcion.length == 0) {
        validar.push("<strong>Paso 4 - Importante:</strong>Adjunte ficha de inscripcion firmado por usted.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').style.display = "block";
        document.getElementById('error').innerHTML = validar;
        validar = [];
        valNext1.disabled = true;

    } else if (InputFileTitulo != 0 && InputFileReglamento != 0 && InputFileInscripcion != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}


function validarReglamento() {
    let InputFileTitulo = document.getElementById('InputFile-Titulo').value;
    let InputFileReglamento = document.getElementById('InputFile-Reglamento').value;
    let InputFileInscripcion = document.getElementById('InputFile-inscripcion').value;

    validar = [];

    let valNext1 = document.getElementById('enviarForm');

    /**************************Validar Constancia reglamento*******************************************/
    if (InputFileReglamento == '') {
        validar.push("<strong>Paso 4 - Importante:</strong>Adjunte reglamento del Instituto firmado por usted.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').style.display = "block";
        document.getElementById('error').innerHTML = validar;
        validar = [];
        valNext1.disabled = true;

    } else if (InputFileTitulo != 0 && InputFileReglamento != 0 && InputFileInscripcion != 0) {
        document.getElementById('error').style.display = "none";

        valNext1.disabled = false;
    }

}