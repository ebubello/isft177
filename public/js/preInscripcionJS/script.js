$(document).ready(function() {
    var current = 1,
        current_step, next_step, steps;
    steps = $("fieldset").length;
    $(".next").click(function() {
        current_step = $(this).parent();
        next_step = $(this).parent().next();
        next_step.show();
        current_step.hide();
        setProgressBar(++current);
    });
    $(".previous").click(function() {
        current_step = $(this).parent();
        next_step = $(this).parent().prev();
        next_step.show();
        current_step.hide();
        setProgressBar(--current);
    });
    setProgressBar(current);
    // Change progress bar action
    function setProgressBar(curStep) {
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width", percent + "%")
            .html(percent + "%");
    }
});


//Pre Visualizar Imagen 4x4

var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function() {
        var dataURL = reader.result;
        var output = document.getElementById('imgSalida');
        output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
};

//Año
window.onload = function() {
    window.scrollTo(0, 0);
    var d = new Date();
    y = d.getFullYear() + 1;
    document.getElementById('titulo').innerHTML = 'SOLICITUD PARA ASPIRANTES AL CURSO INICIAL - AÑO: ' + y;


    document.getElementById('error').style.display = "none";

    valNext1 = document.getElementById('next1');
    valNext1.disabled = true;


    valNext2 = document.getElementById('next2');
    valNext2.disabled = true;

    valNext3 = document.getElementById('next3');
    valNext3.disabled = true;

    valEnviar = document.getElementById('enviarForm');
    valEnviar.disabled = true;

    document.getElementById("dni").value = localStorage.getItem("dato");
    localStorage.removeItem("dato");



};