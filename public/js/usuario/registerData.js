let formulario2 = document.getElementById('formRegister');
let dni2 = document.getElementById('dni');
let email2 = document.getElementById('email');
let respuesta2 = document.getElementById('respuesta');
let register = document.getElementById('cRegister');

function UserCreate() {
    $(document).ready(function() {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Usuario Creado!!!',
            showConfirmButton: false,
            timer: 3500
        })
    });
}


formulario2.addEventListener('submit', function(e) {
    e.preventDefault();

    let datos = new FormData(formulario2);


    fetch("../Usuario/register", {
            method: 'POST',
            body: datos
        })
        .then(res => res.json())
        .then(data => {

            if (data[0] === 'true') {

                window.scrollTo(0, 0);
                dni2.value = '';
                respuesta2.innerHTML = `<div class="alert alert-danger" role="alert">Ya existe un usuario con ese DNI.</div>`;

            } else if (data[1] === 'true') {

                window.scrollTo(0, 0);
                email2.value = '';
                respuesta2.innerHTML = `<div class="alert alert-danger" role="alert">Ya existe un usuario con ese EMAIL.</div>`;

            }

            if (data[0] === 'false' && data[1] === 'false') {
                UserCreate();
                window.location.href = "../Dashboard/dashboard";
            }



        })
        .catch((error) => {
            console.log(error)
        });
});