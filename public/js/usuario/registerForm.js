window.onload = function() {
    window.scrollTo(0, 0);
    document.getElementById('error').style.display = "none";
    document.getElementById('enviar').disabled = true;

}



window.addEventListener('load', validarForm, false);


function validarForm() {
    /***Step 2 */

    document.getElementById('username').addEventListener('change', userName, false);
    document.getElementById('dni').addEventListener('change', dni, false);
    document.getElementById('email').addEventListener('change', email, false);
    document.getElementById('password').addEventListener('change', password, false);
    document.getElementById('password2').addEventListener('change', password2, false);
    document.getElementById('perfil').addEventListener('change', perfil, false);



}

function userName() {
    const patternNombre = /^[A-Za-z]{3,}/;


    let validarNombre = document.getElementById('username').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarPassword = document.getElementById('password').value;
    let validarPassword2 = document.getElementById('password2').value;
    let validarPerfil = document.getElementById('perfil').value;
    let validarEnviar = document.getElementById('enviar');

    let validar = [];

    /**************************Validar Nombre*******************************************/

    if (validarNombre.length == 0) {
        document.getElementById('username').focus();
        validar.push("<strong>Nombre de Usuario:</strong> No debe estar vacio<br>");
        window.scrollTo(0, 0);
    } else if (patternNombre.test(validarNombre) == false) {
        document.getElementById('username').focus();
        validar.push("<strong>Nombre de Usuario:</strong> Solo se permiten caracteres A a Z Mínimo (3) Máximo (24).<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        validarEnviar.disabled = true;
    } else if (validarNombre.length != 0 && validarDNI.length != 0 && validarPassword.length != 0 &&
        validarPassword2.length != 0 && validarPerfil.length != 0 && validarEmail.length != 0 &&
        validarPassword == validarPassword2) {
        document.getElementById('error').style.display = "none";

        validarEnviar.disabled = false;
    }
}


function dni() {
    const patternDNI = /[0-9]{8,8}/;

    let validarNombre = document.getElementById('username').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarPassword = document.getElementById('password').value;
    let validarPassword2 = document.getElementById('password2').value;
    let validarPerfil = document.getElementById('perfil').value;
    let validarEnviar = document.getElementById('enviar');

    let validar = [];

    /**************************Validar DNI*******************************************/

    if (validarDNI.length == 0) {
        document.getElementById('dni').focus();
        validar.push("<strong>DNI: </strong> No debe estar vacio<br>");
        window.scrollTo(0, 0);
    } else if (patternDNI.test(validarDNI) == false) {
        document.getElementById('dni').focus();
        validar.push("<strong>DNI: </strong>Solo se permiten caracteres numericos.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        validarEnviar.disabled = true;
    } else if (validarNombre.length != 0 && validarDNI.length != 0 && validarPassword.length != 0 &&
        validarPassword2.length != 0 && validarPerfil.length != 0 && validarEmail.length != 0 &&
        validarPassword == validarPassword2) {
        document.getElementById('error').style.display = "none";

        validarEnviar.disabled = false;
    }

}

function email() {
    const patternEmail = /[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}/;

    let validarNombre = document.getElementById('username').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarPassword = document.getElementById('password').value;
    let validarPassword2 = document.getElementById('password2').value;
    let validarPerfil = document.getElementById('perfil').value;
    let validarEnviar = document.getElementById('enviar');

    let validar = [];

    /**************************Validar Email*******************************************/

    if (validarEmail.length == 0) {
        document.getElementById('email').focus();
        validar.push("<strong>E-mail: </strong> No debe estar vacio<br>");
        window.scrollTo(0, 0);
    } else if (patternEmail.test(validarEmail) == false) {
        document.getElementById('email').focus();
        validar.push("<strong>E-mail: </strong>Ingrese un Email valido (Los caracteres especiales no estan permitidos).<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        validarEnviar.disabled = true;
    } else if (validarNombre.length != 0 && validarDNI.length != 0 && validarPassword.length != 0 &&
        validarPassword2.length != 0 && validarPerfil.length != 0 && validarEmail.length != 0 &&
        validarPassword == validarPassword2) {
        document.getElementById('error').style.display = "none";

        validarEnviar.disabled = false;
    }
}


function password() {
    const patternPassword = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

    let validarNombre = document.getElementById('username').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarPassword = document.getElementById('password').value;
    let validarPassword2 = document.getElementById('password2').value;
    let validarPerfil = document.getElementById('perfil').value;
    let validarEnviar = document.getElementById('enviar');



    let validar = [];

    /**************************Validar Password*******************************************/

    if (validarPassword.length == 0) {
        document.getElementById('password').focus();
        validar.push("<strong>Password: </strong> No debe estar vacio<br>");
        window.scrollTo(0, 0);
    } else if (patternPassword.test(validarPassword) == false) {
        document.getElementById('password').focus();
        validar.push("<strong>Password: </strong>Debe utiliza mayusculas, minusculas, numeros/ caracteres especiales y un min de 8 caracteres.<br>");
        window.scrollTo(0, 0);
    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        validarEnviar.disabled = true;
    } else if (validarNombre.length != 0 && validarDNI.length != 0 && validarPassword.length != 0 &&
        validarPassword2.length != 0 && validarPerfil.length != 0 && validarEmail.length != 0 &&
        validarPassword == validarPassword2) {
        document.getElementById('error').style.display = "none";

        validarEnviar.disabled = false;
    }

}

function password2() {

    let validarNombre = document.getElementById('username').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarPassword = document.getElementById('password').value;
    let validarPassword2 = document.getElementById('password2').value;
    let validarPerfil = document.getElementById('perfil').value;
    let validarEnviar = document.getElementById('enviar');

    let validar = [];

    /**************************Validar Password2*******************************************/

    if (validarPassword2.length == 0) {
        document.getElementById('password2').focus();
        validar.push("<strong>Password: </strong> No debe estar vacio<br>");
        window.scrollTo(0, 0);
    } else if (validarPassword2 != validarPassword) {
        document.getElementById('password2').focus();
        validarPassword.value = '';
        validarPassword2.value = '';
        validar.push("<strong>Repetir Password: </strong>Las contraseñas no son iguales.<br>");
        window.scrollTo(0, 0);

    }

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        validarEnviar.disabled = true;
    }


}

function perfil() {

    let validarNombre = document.getElementById('username').value;
    let validarDNI = document.getElementById('dni').value;
    let validarEmail = document.getElementById('email').value;
    let validarPassword = document.getElementById('password').value;
    let validarPassword2 = document.getElementById('password2').value;
    let validarPerfil = document.getElementById('perfil').value;
    let validarEnviar = document.getElementById('enviar');

    let validar = [];

    /**************************Validar Perfil*******************************************/

    if (validarPerfil == 'Seleccione...') {
        validar.push("<strong>Perfil: </strong> Debe seleccionar un perfil.");
        document.getElementById('perfil').focus();
    }

    document.getElementById('perfil').focus();

    if (validar.length > 0) {
        document.getElementById('error').innerHTML = validar;
        document.getElementById('error').style.display = "block";
        validar = [];
        validarEnviar.disabled = true;
    } else if (validarNombre.length != 0 && validarDNI.length != 0 && validarEmail.length != 0 &&
        validarPassword.length != 0 && validarPassword2.length != 0 && validarPerfil != 'Seleccione...' &&
        validarPassword == validarPassword2) {
        document.getElementById('error').style.display = "none";

        validarEnviar.disabled = false;
    }

}