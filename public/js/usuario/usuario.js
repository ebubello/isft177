let formulario = document.getElementById('formLogin');
let dni = document.getElementById('dni');
let pass = document.getElementById('password');
// let validar = document.getElementById('btnFetch');
let respuesta = document.getElementById('respuesta');



formulario.addEventListener('submit', function(e) {
    e.preventDefault();

    var datos = new FormData(formulario);


    fetch("../Usuario/login", {
            method: 'POST',
            body: datos
        })
        .then(res => res.json())
        .then(data => {

            if (data === 'false') {

                window.scrollTo(0, 0);

                dni.value = '';
                pass.value = '';

                respuesta.innerHTML = `
                                         <div class="alert alert-danger" role="alert">
                       DNI o Contraseña incorrectos.
                                        </div>
                                        `

            } else {
                window.location.href = "../Dashboard/dashboard";
            }
        })
        .catch((error) => {
            console.log(error)
        });
});