let formulario = document.getElementById('formForgot');
let email = document.getElementById('email');


let respuesta = document.getElementById('respuesta');



formulario.addEventListener('submit', function(e) {
    e.preventDefault();

    var datos = new FormData(formulario);


    fetch("../Usuario/forgot", {
            method: 'POST',
            body: datos
        })
        .then(res => res.json())
        .then(data => {

            if (data === 'false') {

                window.scrollTo(0, 0);

                email.value = '';

                respuesta.innerHTML = `
                                         <div class="alert alert-danger" role="alert">
                       Email no pertenece a un usuario registrado.
                                        </div>
                                        `

            } else {
                window.location.href = "../Home";
            }
        })
        .catch((error) => {
            console.log(error)
        });
});