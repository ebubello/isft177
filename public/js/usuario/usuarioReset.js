let formulario = document.getElementById('formReset');
let newPass = document.getElementById('newPass');
let newPass2 = document.getElementById('newPass2');

formulario.addEventListener('submit', function(e) {
    e.preventDefault();

    var datos = new FormData(formulario);


    fetch("../../Usuario/resetPassword", {
            method: 'POST',
            body: datos
        })
        .then(res => res.json())
        .then(data => {

            if (data === 'false') {

                window.scrollTo(0, 0);

                newPass.value = '';
                newPass2.value = '';

                respuesta.innerHTML = `
                                         <div class="alert alert-danger" role="alert">
                       Las contraseñas no son iguales.
                                        </div>
                                        `

            } else {
                window.location.href = "../Home";
            }
        })
        .catch((error) => {
            console.log(error)
        });
});